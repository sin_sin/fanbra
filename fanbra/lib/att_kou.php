<?php
#□■□■□■□■□■□■□■□■□■□■□
#■ 	-    BR ATTACK PROGRAM    - 	 ■
#□ 									 □
#■ 		  サブルーチン一覧  		 ■
#□ 									 □
#■ ATTACK2			-		後攻攻撃処理 ■
#□■□■□■□■□■□■□■□■□■□■□
#==================#
# ■ 後攻攻撃処理  #
#==================#
function ATTACK2(){
global $in,$pref;

global $w_sts,$w_club,$wep,$w_wep,$watt,$att,$atp,$def,$bou,$bdef,$bdef_h,$bdef_a,$bdef_a,$bdef_f,$item,$dfp,$w_wtai,$w_watt,$w_att,$atn,$w_def,$w_bou,$w_btai,$w_bdef,$w_bdef_h,$w_bdef_a,$w_bdef_f,$w_item,$dfn;
global $w_hit,$log,$w_f_name,$w_l_name,$w_cl,$w_sex,$w_no,$l_name;
global $mei2,$hit,$w_teamID;
global $wk,$w_limit,$w_level,$pnt,$inf,$w_inf;
global $jyuku2,$limit,$btai,$w_wep_2,$w_watt_2,$w_wtai_2,$inf_2,$level,$w_exp,$weps,$weps2,$wtai,$mei,$w_log,$hour,$min,$sec,$f_name,$l_name,$cl,$sexx,$no;
global $jyuku1,$wep_2,$watt_2,$wtai_2,$w_inf_2,$exp;
global $hissatsu1,$hissatsu2,$w_ousen,$kega2,$kega3,$hakaiinf2,$hakaiinf3,$w_kind,$w_kind2;
if($w_hit <= 0){ERROR('非正常接入','no hit','ATT_KOU',__FUNCTION__,__LINE__);}

$kega2 = $kega3 = $hakaiinf2 = $hakaiinf3 = '';

$result = 0;
$result2 = 0;
$i = 0;
$dice1 = rand(0,100);
if(preg_match('/军乐队部|围棋部|基翁公国|足球部|曲棍球部/',$pref['clb'][$w_club])){$dice2 = rand(0,110);}
else{$dice2 = rand(0,100);}
if($pref['clb'][$club] == '柔道部'){$dice3 = rand(0,90);}
else{$dice3 = rand(0,100);}
list($w_name,$w_kind) = explode("<>",$wep);
list($w_name2,$w_kind2) = explode("<>",$w_wep);

TACTGET();TACTGET2();	#基本行動

#プレイヤー
if((preg_match("/G|A/",$wep)) && ($wtai == 0)){$att_p = (($watt/10) + $att) * $atp;}
else{$att_p = ($watt + $att) * $atp;}
$ball = $def + $bdef + $bdef_h + $bdef_a + $bdef_f;
if(preg_match("/AD/",$item[5])){$ball += $eff[5];}#装飾が防具？
$def_p = $ball * $dfp;

#敵
if((preg_match('/G|A/',$w_wep)) && ($w_wtai == 0)){$att_n = (($w_watt/10) + $w_att) * $atn;}
else{$att_n = ($w_watt + $w_att) * $atn;}
$ball2 = $w_def + $w_bdef + $w_bdef_h + $w_bdef_a + $w_bdef_f;
if(preg_match('/AD/',$w_item[5])){$ball += $w_eff[5];}#装飾が防具？
$def_n = $ball2 * $dfn;

BLOG_CK();
EN_KAIFUKU();

$in['Command']='BATTLE';

$log .= $w_f_name.' '.$w_l_name.'（'.$w_cl.' '.$w_sex.$w_no.'号）<BR>　　　　　　　　　　　　　　　突然袭击过来了！<br>';

WEPTREAT($w_name2,$w_kind2,$w_wtai,$w_l_name,$l_name,'攻击','NPC');
if($dice2 < $mei2){	#攻撃成功
	#攻撃(攻撃力*熟練度)
	$result = $att_n*$wk;
	#必殺技
	$hissatsu1 = rand(0,100);
	if($w_limit >= 100){
		$result *= 1.5;
		$log .= '　<font color="red"><b>限界解除发动！</b></font><br>';
		$hissatsu1 = '(奥义)';
		$w_limit -= 100;
	}elseif(($level >= 10)&&($jyuku1 >= 200)&&($hissatsu1 < 1)){
		$result *= 1.4;
		$log .= '　<font color="red"><b>秘技发动！</b></font><br>';
		$hissatsu1 = "(秘技)";
	}elseif(($level >= 3)&&($jyuku1 >= 20)&&($hissatsu1 < 4)){
		$result *= 1.1;
		$log .= '　<font color="red"><b>爆击！</b></font><br>';
		$hissatsu1 = '(爆击)';
	}else{$hissatsu1 = '';}

	#攻撃(-防御/2+=rand)
	$result -= $def_p;
	$result /= 2;
	$result += rand(0,$result);
	$result = (int)($result);

	DEFTREAT($w_kind2,'PC');
	$result = (int)($result * $pnt);

	if($result <= 0){$result = 1;}
	if($pref['clb'][$club]=='攀岩部' && $result > $hit && 1 == rand(1,10)){$result = $hit - 1;}
	$log .= '　　<font color="red"><b>'.$result.'伤害 '.$kega2.'</b>！</font><br>';

	$hit -= $result;$btai--;

	if($btai <= 0){$bou = '内衣<>DN';$bdef=0;$btai='∞';}
	$w_wep = $w_wep_2;$w_watt = $w_watt_2;$w_wtai = $w_wtai_2;$inf = $inf_2;
	list($w_name2,$w_kind2) = explode("<>",$w_wep);
#経験値-敵
	$expup = (int)(($level - $w_level)/3);if($expup <1){$expup = 1;}$w_exp += $expup;
#リミット-自分
	if($pref['clb'][$club]=='攀岩部'){$div=2.5;}else{$div=3;}
	$limitup = (int)(($w_level - $level)/$div);if($limitup < 1){$limitup = 1;}$limit += $limitup;
}else{
	$log .= '　　但是,千钧一发之际避开了！<br>';
}

if($hit <= 0){DEATH();}#死亡？
elseif($dice3 < 50){#反撃

	if($weps == $weps2 || $weps == 'M'){#距離一緒？

		WEPTREAT($w_name, $w_kind,  $wtai, $l_name, $w_l_name, '反撃','PC');

		if($dice1 < $mei){	#攻撃成功
			#攻撃(攻撃力*熟練度)
			$result2 = $att_p*$wk;
			#必殺技
			$hissatsu2 = rand(0,100);
			if($limit >= 100){
				$result2 *= 1.7;
				$log .= '　<font color="red"><b>限界解除发动！</b></font><br>';
				$hissatsu2 = "(奥义)";
				$limit -= 100;
			}elseif(($w_level >= 10)&&($jyuku2 >= 200)&&($hissatsu2 < 1)){
				$result2 *= 1.6;
				$log .= '　<font color="red"><b>秘技发动！</b></font><br>';
				$hissatsu2 = '(秘技)';
			}elseif(($w_level >= 3)&&($jyuku2 >= 20)&&($hissatsu2 < 5)){
				$result2 *= 1.3;
				$log .= '　<font color="red"><b>爆击！</b></font><br>';
				$hissatsu2 = "(爆击)";
			}else{$hissatsu2 = "";}
			#攻撃(-防御/2+=rand)
			$result2 -= $def_n;
			$result2 /= 2;
			$result2 += rand(0,$result2);
			$result2 = (int)($result2);

			DEFTREAT($w_kind,'NPC');
			$result2 = (int)($result2 * $pnt);

			if($result2 <= 0){$result2 = 1;}
			if($pref['clb'][$w_club]=='攀岩部' && $result2 > $w_hit && 1 == rand(1,10)){$result2 = $w_hit - 1;}
			$log .= '　　<font color="red"><b>'.$result2.'伤害 '.$hakaiinf3.' '.$kega3.'</b>！</font><br>';

			$w_hit -= $result2;$w_btai--;

			if($w_btai <= 0){$w_bou = '内衣<>DN';$w_bdef=0;$w_btai='∞';}

			if($w_hit <=0){#死亡？
				DEATH2();
			}else{#逃亡
				$log .= '　'.$l_name.' 逃了…。<br>';
			}

			$w_log .= '<font color="yellow"><b>'.$hour.':'.$min.':'.$sec.' 战斗：'.$f_name.' '.$l_name.'（'.$cl.' '.$sexx.$no.'号） 攻'.$hissatsu2.':'.$result.' 受'.$hissatsu1.':'.$result2.' '.$hakaiinf2.' '.$kega3.'</b></font><br>';
			$wep = $wep_2;$watt = $watt_2;$wtai = $wtai_2;$w_inf = $w_inf_2;
			list($w_name,$w_kind) = explode('<>',$wep);
		#経験値-自分
			$expup = (int)(($w_level - $level)/3);if($expup <1){$expup = 1;}$exp += $expup;
		#リミット-相手
			if($pref['clb'][$w_club]=='攀岩部'){$div=2.5;}else{$div=3;}
			$w_limitup = (int)(($level -$w_level)/$div);if($w_limitup < 1){$w_limitup = 1;}$w_limit += $w_limitup;
		}else{
			$w_log .= '<font color="yellow"><b>'.$hour.':'.$min.':'.$sec.' 战斗：'.$f_name.' '.$l_name.'（'.$cl.' '.$sexx.$no.'号） 攻'.$hissatsu2.':'.$result.' '.$hakaiinf2.'</b></font><br>';
			$log .= '　但是,避开了！<br>';
		}
		#武器消耗
		if((preg_match('/G|A/',$w_kind)) && ($wtai > 0)){$wtai--;if($wtai <= 0){$wtai = 0;}}#銃・射？
		elseif(preg_match('/C|D/',$w_kind)){$wtai--;if($wtai <= 0){$wep ='空手<>WP';$watt=0;$wtai='∞';}}
		elseif((preg_match('/K/',$w_kind)) && (rand(0,5) == 0)){$watt -= rand(1,3);if($watt <= 0){$wep ='空手<>WP';$watt=0;$wtai='∞';}}
	}else{
		$log .= $l_name.' 不能反击！<br>　'.$l_name.' 逃了…。<br>';
		$w_log .= '<font color="yellow"><b>'.$hour.':'.$min.':'.$sec.' 战斗：'.$f_name.' '.$l_name.'（'.$cl.' '.$sexx.$no.'号） 攻'.$hissatsu2.':'.$result.' '.$hakaiinf2.' '.$kega3.'</b></font><br>';
	}
}else{	#逃亡
	$log .= $w_l_name.'逃了…。<br>';
	$w_log .= '<font color="yellow"><b>'.$hour.':'.$min.':'.$sec.' 战斗：'.$f_name.' '.$l_name.'（'.$cl.' '.$sexx.$no.'号） 受'.$hissatsu1.':'.$result.' '.$hakaiinf2.' '.$kega3.'</b></font><br>';
	if($w_ousen == '逃跑姿态'){#逃跑姿态自動回避
		global $w_pls;
		$w_pls = RUN_AWAY($w_pls);
		$w_log .= '<font color="lime"><b>'.$pref['place'][$w_pls].'移动了。</b></font><br>';
	}
}
if(($w_sts == 'NPC')&&(preg_match('/大叔/',$w_cl))){#逃跑姿态自動回避
	global $w_pls;
	$w_pls = RUN_AWAY($w_pls);
	$w_log .= '<font color="lime"><b>'.$pref['place'][$w_pls].'移动</b></font><br>';
}
#武器消耗
if((preg_match('/G|A/',$w_kind2)) && ($w_wtai > 0)){$w_wtai--;if($w_wtai <= 0){$w_wtai = 0;}}#銃・射？
elseif(preg_match('/C|D/',$w_kind2)){$w_wtai--;if($w_wtai <= 0){$w_wep = '空手<>WP';$w_watt=0;$w_wtai='∞';}}
elseif((preg_match('/K/',$w_kind2)) && (rand(0,5) == 0)){$w_watt -= rand(1,3);if($w_watt <= 0){$w_wep ='空手<>WP';$w_watt=0;$w_wtai='∞';}}
LVUPCHK();

SAVE();
SAVE2();

}
?>