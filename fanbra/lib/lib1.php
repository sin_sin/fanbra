<?php
/*
#□■□■□■□■□■□■□■□■□■□■□
#■ 	-    BR LIBRARY PROGRAM    - 	 ■
#□ 									 □
#■ 		サブルーチン一覧			 ■
#□ 									 □
#■ 	-			-	初期化			 ■
#■ ADDKINSHI2		-	禁止エリア追加	 ■
#■ ADDKINSHI2		-禁止エリア追加(修正)■
#■ KINSHIDEATH		-禁止エリア死亡処理  ■
#■ DECODE			-	デコード処理	 ■
#□ SAVE			-	ユーザ情報保存	 □
#■ SAVE2			-	敵情報保存		 ■
#□ CREAD			-	クッキー読込	 □
#■ CSAVE			-	クッキー保存	 ■
#□ CDELETE			-	クッキー削除	 □
#■ LOGSAVE			-	ログ保存		 ■
#□ GetHostName		-	ホスト名取得	 □
#■ HEADER			-	ヘッダー部		 ■
#□ FOOTER			-	フッター部		 □
#■ ERROR			-	エラー処理		 ■
#□ AS				-	自動選択		 □
#□ LOCK			-	ロック			 □
#■ UNLOCK			-	アンロック		 ■
#□■□■□■□■□■□■□■□■□■□■□*/
//初期化ここから//
global $ADMINLOCK;

LOCK();#ファイルロック

# ホスト判別
global $host,$ref_url;
$host1 = $_SERVER['REMOTE_ADDR'];
$host2 = gethostbyaddr($host1);
if($pref['host_save'] == 1){
	$host = $host1;
}elseif($pref['host_save']){
	if($host1 == $host2 || $host1 == ''){$host = $host2;}
	else{$host = $host1.'-'.$host2;}
}else{
	$host = '';
}

# モバイル対応Script
$user_agent = explode('/',$_SERVER{'HTTP_USER_AGENT'});
$pref['MOBILE'] = $pic = false;
if (preg_match("/\.(ezweb|ido)\.ne\.jp$/",$host2)) {# EZweb 用の処理
	$pref['MOBILE'] = 'Z';
	$pic = '.gif';
}elseif(preg_match("/\.google\.com$/",$host2) && $user_agent[0] == 'Mozilla' && strpos($user_agent[1],'Google Wireless Transcoder')!==false){# EZweb PCサイト 用の処理
	$pref['MOBILE'] = 'Z';
	$pic = '.gif';
#	if ($ENV{'HTTP_USER_AGENT'} =~ /^KDDI/) {# EZweb WAP2.0 端末用の処理
#	} else {# EZweb 旧端末用の処理
#	}
#} elsif (($hostname eq 'pdxcgw.pdx.ne.jp')&&($user_agent[0] eq 'PDXGW')) {# H" 用の処理
#}elseif((preg_match("/\.docomo\.ne\.jp$/",$host2))&&($user_agent[0] == 'DoCoMo')) {# i-mode 用の処理
}elseif(preg_match("/\.docomo\.ne\.jp$/",$host2)) {# i-mode 用の処理
	$pref['MOBILE'] = 'D';
	$pic = '.gif';
#}elseif(($user_agent[0] == 'J-PHONE')&&(preg_match("/(jp-[ckqt]|vodafone|softbank)\.ne\.jp$/",$host2))) {# J-SKY 用の処理
#	$pref['MOBILE'] = 'V';
#	$pic = '.png';
#} elsif (($user_agent[0] eq 'L-mode')&&($hostname =~ /\.pipopa\.ne\.jp$/)){# L-mode 用の処理
#}elseif(($user_agent[0] == 'Vodafone' || $user_agent[0] == 'SoftBank')&&(preg_match("/(jp-[ckqt]|vodafone|softbank)\.ne\.jp$/",$host2))) {# SoftBank 用の処理
}elseif(preg_match("/(jp-[ckqt]|vodafone|softbank)\.ne\.jp$/",$host2) || strpos($host2,'mobile.ogk.yahoo.co.jp')){
	$pref['MOBILE'] = 'V';
	$pic = '.gif';
} elseif ($user_agent[0] == 'Mozilla' && preg_match("/\.search\.tnz\.yahoo\.co\.jp$/",$host2)) {# SoftBank 用の処理
	$pref['MOBILE'] = 'V';
	$pic = '.gif';
}elseif($user_agent[0] == 'DoCoMo') {# DoCoMo 用の処理
	$pref['MOBILE'] = 'D';
	$pic = '.gif';
}#else{#それ以外
#}

if(isset($_SERVER['HTTP_CACHE_CONTROL']) || isset($_SERVER['HTTP_FORWARDED']) || isset($_SERVER['HTTP_PROXY_CONNECTION']) || isset($_SERVER['HTTP_VIA']) || isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
	$pref['PROXY']=true;
}else{
	$pref['PROXY']=false;
}

if($ADMINLOCK){ADMDECODE();}else{DECODE();}

# 排除機能
$KICKOUT = 1;
if(isset($_SERVER['HTTP_REFERER'])){$ref_url = $_SERVER['HTTP_REFERER'];}
if(isset($base_list)){if(preg_match("/$base_list/i",$ref_url)) {$KICKOUT = 0;}}#last;}

if(!$KICKOUT || !$pref['base_url'] || ($pref['MOBILE'] && $pref['base_url'] == 2)){;}
elseif($in['Command']=='BRU' && $HEADERINDEX){;}
else{require $pref['LIB_DIR'].'/lib4.php';KICKOUT();}

# アクセス禁止
if($pref['acc_for']){
	#OKリスト確認
	$okflg = 0;
	foreach ($pref['oklist'] as $oklist2){
		if($host2 == $oklist2){
			$okflg = 1;
		}
	}
	#アクセス禁止チェック
	if($okflg == 0){
		foreach ($pref['kick'] as $kick2){
			if(strpos($host1,$kick2)!==false || strpos($host2,$kick2)!==false){
				ERROR('您被BAN了<BR>Your access has been forbidden.','HOST-'.$host,'PREF',__FUNCTION__,__LINE__);
			}
		}
	}
}

# 禁止エリアファイル読み込み
$areafile=$pref['area_file'];
$pref['arealist'] = @file($pref['area_file']);

$fl = @file($pref['end_flag_file']);# or ERROR("unable to open end_flag_file");
$fl = implode('',$fl);
list($y,$m,$d,$hh,$mm) = explode(',',trim($pref['arealist'][0]));
list($sec,$min,$hour,$mday,$month,$year,$wday,$yday,$isdst) = localtime($pref['now']);
$month++;$year += 1900;
global $hackflg;

if(!preg_match("/结束/",$fl)){#終了前
	#CONF
	$pgsplit = 1;
	$fixmax = 24;
	$tgttime  = mktime($hh,$mm,0,$m,$d,$y);#次回禁止エリア追加予定時間
	if($pref['now'] >= $tgttime){#次回禁止エリア後
		if($pref['now'] < ($tgttime + ($pgsplit * 60 * 60))){#次々回前
			ADDKINSHI();
		}elseif($pref['now'] > ($tgttime + ($fixmax * 60 * 60))){#不可能
		#禁止エリア一定時刻の場合は修復されません。
			if(isset($ADMINLOCK)){
				if($ADMINLOCK != 1){
					ERROR('您被时间抛弃了！<BR>Please contact with Administrator','绿坝娘来袭警报','pref');
				}
			}else{
				ERROR('您被时间抛弃了！<BR>Please contact with Administrator','绿坝娘来袭警报','pref');
			}
		}else{#修正可能
			for($i=1;$pref['now'] >= $tgttime;$i++){
				if($pref['now'] < $tgttime + ($i * $pgsplit * 60 * 60)){
					ADDKINSHI();
					break;
				}
				ADDKINSHI2();
				$tgttime  = mktime($hh,$mm,0,$m,$d,$y);#次回禁止エリア追加予定時間
			}
		}
	}
}

#禁止エリア取得
list($ar,$hackflg,$a) = explode(',',$pref['arealist'][1]);
#Re-get time
list($sec,$min,$hour,$mday,$month,$year,$wday,$yday,$isdst) = localtime($pref['now']);
if($hour < 10){$hour = '0'.$hour;}
if($min < 10){$min = '0'.$min;}
if($sec < 10){$sec = '0'.$sec;}
$month++;$year += 1900;

if(isset($ar)){
	# Stamina Max
	if($ar >= 7){$pref['maxsta'] = 500;}
	elseif($ar >= 4){$pref['maxsta'] = 400;}
	else{$pref['maxsta'] = 300;}
}
//初期化ここまで//
#===================#
# ■ 禁止エリア追加 #
#===================#
function ADDKINSHI(){
global $pref;

global $weth,$areafile,$ar,$ar2,$hackflg,$hh,$hour,$hackflg;
#CONF

$pgsplit = 1;

list($sec,$min,$hour,$mday,$month,$year,$wday,$yday,$isdst) = localtime($pref['now']+(60*60*$pgsplit));
$month++;$year += 1900;

$hour = $pgsplit * round($hour / $pgsplit);
if($hour >= 24){$hour-=24;$mday++;}

$weth = rand(0,9);
if(($weth < 0)&&($weth > 9)){$weth = '0';}
$pref['arealist'][0] = $year.','.$month.','.$mday.','.$hour.",0\n";	#エリア追加時刻
list($ar,$hackflg,$a) = explode(',',$pref['arealist'][1]);
$ar2 = $ar + 1;
if($hackflg){$hackflg--;}
$pref['arealist'][1] = $ar2.','.$hackflg.",\n";#禁止エリア数
$pref['arealist'][5] = $weth."\n";

$handle = @fopen($areafile,'w') or ERROR('unable to open areafile','','PREF',__FUNCTION__,__LINE__);
if(!@fwrite($handle,implode('',$pref['arealist']))){ERROR('unable to write areafile','','PREF',__FUNCTION__,__LINE__);}
fclose($handle);

#禁止エリア追加ログ
if($hackflg){LOGSAVE('HKARAD');}
else{LOGSAVE('AREAADD');}

$userlist = @file($pref['user_file']) or ERROR('unable to open user_file','','PREF',__FUNCTION__,__LINE__);

#禁止エリア者死亡処理
$userlist = KINSHIDEATH($userlist);

$handle = @fopen($pref['user_file'],'w') or ERROR('unable to open user_file','','PREF',__FUNCTION__,__LINE__);
if(!@fwrite($handle,implode('',$userlist))){ERROR('unable to write user_file','','PREF',__FUNCTION__,__LINE__);}
fclose($handle);

#Back Up
$pref['back_file'] = $pref['LOG_DIR'].'/BUF_'.$hour.'.log';
$handle = @fopen($pref['back_file'],'w') or ERROR('unable to open back_file',$hour,'PREF',__FUNCTION__,__LINE__);
if(!@fwrite($handle,implode('',$userlist))){ERROR('unable to write back_file',$hour,'PREF',__FUNCTION__,__LINE__);}
fclose($handle);
}
#=========================#
# ■ 禁止エリア追加(修正) #
#=========================#
function ADDKINSHI2(){
global $pref;

global $year,$month,$mday,$weth,$userlist,$areafile,$ar,$ar2,$hh,$hackflg;
#CONF

$pgsplit = 1;

$hour = $hh + $pgsplit;
if($hour > 24){$hour-=24;}

$pref['arealist'][0] = $year.','.$month.','.$mday.','.$hour.',0';	#エリア追加時刻
list($ar,$hackflg,$a) = explode(',',$pref['arealist'][1]);
$ar2 = $ar + 1;
if($hackflg){$hackflg--;}
$pref['arealist'][1] = $ar2.','.$hackflg.",\n";#禁止エリア数,ハッキング

#禁止エリア追加ログ
if($hackflg){LOGSAVE('HKARAD');}
else{LOGSAVE('AREAADD');}

#禁止エリア者死亡処理
$userlist = KINSHIDEATH($userlist);

$hh = $hour;
}
#=======================#
# ■ 禁止エリア死亡処理 #
#=======================#
function KINSHIDEATH($userlist){
global $pref;
global $ar,$ar2,$cnt,$hackflg,$deth,$fl;
global $w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item,$w_eff,$w_itai,$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP;

//分校追加
if($ar2==1){
	return $userlist;
}

list($ara[0],$ara[1],$ara[2],$ara[3],$ara[4],$ara[5],$ara[6],$ara[7],$ara[8],$ara[9],$ara[10],$ara[11],$ara[12],$ara[13],$ara[14],$ara[15],$ara[16],$ara[17],$ara[18],$ara[19],$ara[20],$ara[21]) = explode(',',$pref['arealist'][4]);
$mem = 0;#生存者数
$chg = false;#変更?
require_once $pref['LIB_DIR'].'/attack.php';
for ($i=0; $i<count($userlist); $i++){#ユーザーループ
	list($w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP,) = explode(",",$userlist[$i]);
	for ($cnt=0; $cnt<$ar2; $cnt++){#禁止エリアループ
		if($pref['place'][$w_pls] == $pref['place'][$ara[$cnt]] && $w_hit > 0 && strstr($fl,'结束')===false){#生存?&!終了?&禁止エリア?
			if($w_sts == 'NPC'){#NPC?
				if($wl_cl == '大叔' && $ar2 >= $pref['npc_run'] && $ar2 < count($pref['place'])){#兵士?
					$w_pls = RUN_AWAY($w_pls);#逃亡応戦自動回避(兵士?)
					$chg = true;
				}#else{}坂持&監査役?
			}else{#PC?
				if(($w_ousen=='逃跑姿态' || $ka==2/* 分校の次 */) && $ar2 < count($pref['place'])){#自動回避
					$w_pls = RUN_AWAY($w_pls);#逃亡応戦自動回避(逃亡?)
					$chg = true;
				}elseif(!$hackflg){	#ハッキング時以外は死亡
					LOGSAVE('DEATHAREA');
					$w_hit=0;$w_death=$deth;$w_sts='菊爆';
					$chg=true;
				}
			}
		}
	}
	if($w_hit > 0){
		if($w_sts != 'NPC'){$mem++;}
		if($ar2 == 4){$w_sta += 100;$chg = true;}#生存者の最大スタミナUP
		elseif($ar2 == 7){$w_sta += 100;$chg = true;}#生存者の最大スタミナUP
		elseif($ar2 == 18){$w_teamID=$w_teamPass='无';$chg = true;}#チーム解除
	}
	if($chg){#変更
		$userlist[$i] = "$w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP,\n";
	}
}
$b_limit = ($pref['battle_limit']) + 1;
if(strstr($fl,'结束')===false && (($mem == 0 && $ar > $b_limit)||($ar2 == count($pref['place'])))){#结束&生存者0 or エリア最後
	$handle = @fopen($pref['end_flag_file'], 'w');# or ERROR("unable to open end_flag_file","","BR",__FUNCTION__,__LINE__);
	if(!@fwrite($handle, "结束\n")){ERROR('unable to write end_flag_file','','BR',__FUNCTION__,__LINE__);}
	fclose($handle);
	LOGSAVE('END');
}
return $userlist;
}
#=================#
# ■ デコード処理 #
#=================#
function DECODE(){
global $in,$pref;

$p_flag=0;
//デコード処理
if($_SERVER['REQUEST_METHOD'] == 'POST') {
	if($_SERVER['CONTENT_LENGTH'] > 51200){
		ERROR('请求量过大','','LIB1',__FUNCTION__,__LINE__);
	}
	$in = array_map('DECODE2',$_POST);
}else{ 
	$in = array_map('DECODE2',$_GET);
}
extract($in);
if(!isset($in['Command'])){$in['Command']='';}
//list($usec, $sec) = explode(' ', microtime());
//return (float) $sec + ((float) $usec * 100000);
//PHP 4.2.0未満は下のコメントをはずす
//srand(make_seed());
}
#==================#
# ■ デコード処理2 #
#==================#
function DECODE2($string){
global $pref;
if(isset($pref['MOBILE']) && $pref['MOBILE']){
	//SJISからEUC-JPに変換
	#$string = mb_convert_encoding(urldecode($string),"EUC-JP","SJIS");
	$string = urldecode($string);
	$string = mb_convert_encoding($string,'UTF-8','GB2312');
}

//タグ解除処理
$replace_a=array('&amp;','&#039;','&quot;','&lt;','&gt;','&nbsp;');
$replace_b=array('&','\'','\"','<','>',' ');
$string = str_replace ($replace_a,$replace_b,$string );
$string = str_replace ($replace_b,$replace_a,$string );

$string = str_replace (array(',',"\n"),array('、',''),$string );

return trim($string);
}
#===================#
# ■ ユーザ情報保存 #
#===================#
function SAVE(){
global $in,$pref;

global $id,$password,$f_name,$l_name,$sex,$cl,$no,$endtime,$att,$def,$hit,$mhit,$level,$exp,$sta,$wep,$watt,$wtai,$bou,$bdef,$btai,$bou_h,$bdef_h,$btai_h,$bou_f,$bdef_f,$btai_f,$bou_a,$bdef_a,$btai_a,$tactics,$death,$msg,$sts,$pls,$kill,$icon,$item,$eff,$itai,$com,$dmes,$bid,$money,$wp,$wg,$wn,$wc,$wd,$comm,$limit,$bb,$inf,$ousen,$seikaku,$sinri,$item_get,$eff_get,$itai_get,$teamID,$teamPass;

$userlist = @file($pref['user_file']) or ERROR('unable to open user_file','','LIB1',__FUNCTION__,__LINE__);

$chksts = 0;
for($i=0;$i<count($userlist);$i++){
	list($w_i,$w_p,$a) = explode(',',$userlist[$i]);
	if(($in['Id'] == $w_i)&&($in['Password'] == $w_p)){#ID一致？
		$chksts = 1;$Index=$i;break;
	}
}
if($chksts){
	if($hit <= 0){$sts = "菊爆";$inf = $pref['now'];}
	$user1 = "$id,$password,$f_name,$l_name,$sex,$cl,$no,$endtime,$att,$def,$hit,$mhit,$level,$exp,$sta,$wep,$watt,$wtai,$bou,$bdef,$btai,$bou_h,$bdef_h,$btai_h,$bou_f,$bdef_f,$btai_f,$bou_a,$bdef_a,$btai_a,$tactics,$death,$msg,$sts,$pls,$kill,$icon,$item[0],$eff[0],$itai[0],$item[1],$eff[1],$itai[1],$item[2],$eff[2],$itai[2],$item[3],$eff[3],$itai[3],$item[4],$eff[4],$itai[4],$item[5],$eff[5],$itai[5],,$com,$dmes,$bid,".$in['club'].",$money,$wp,$wg,$wn,$wc,$wd,$comm,$limit,$bb,$inf,$ousen,$seikaku,$sinri,$item_get,$eff_get,$itai_get,$teamID,$teamPass,".$in['IP'].",";
	$user1 = ereg_replace("\x0D?\x0A?$","",$user1);/*改行コードの削除*/
	$userlist[$Index] = "$user1\n";

	$handle = @fopen ($pref['user_file'],'w') or ERROR('unable to open user_file','','LIB1',__FUNCTION__,__LINE__);
	if(!@fwrite($handle,implode('',$userlist))){ERROR('unable to write areafile','','LIB1',__FUNCTION__,__LINE__);}
	fclose($handle);
}else{
	ERROR('Internal Server Error','故障发生，请联络馆里猿 CODE:'.$in['Id'],'LIB1',__FUNCTION__,__LINE__);
}
}
#===============#
# ■ 敵情報保存 #
#===============#
function SAVE2(){
global $pref;

global $w_id,$w_pass,$chksts;

global $w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item,$w_eff,$w_itai,$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP;
$userlist = @file($pref['user_file']) or ERROR('unable to open user_file','','LIB1',__FUNCTION__,__LINE__);

if($w_hit <= 0){
	$w_sts = "菊爆";
	$w_inf = $pref['now'];
}
$user2 = "$w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP,";
$user2 = ereg_replace("\x0D?\x0A?$",'',$user2);/*改行コードの削除*/

for($i=0;$i<count($userlist);$i++){#ID一致？
	list($w_i,$w_p,$a) = explode(',',$userlist[$i]);
	if(($w_id == $w_i)&&($w_password == $w_p)){
		$chksts = 1;
		$Index=$i;
		break;
	}
}
if($chksts){
	$userlist[$Index] = $user2."\n";

	$handle = @fopen($pref['user_file'],'w') or ERROR('unable to open user_file','','LIB1',__FUNCTION__,__LINE__);
	if(!@fwrite($handle,implode('',$userlist))){ERROR('unbale to write areafile','','LIB1',__FUNCTION__,__LINE__);}
	fclose($handle);
}else{
	ERROR('Internal Server Error','故障发生，请联络馆里猿 CODE:'.$w_id,'LIB1',__FUNCTION__,__LINE__);
}
}
#=================#
# ■ クッキー読込 #
#=================#
function CREAD(){
global $c_id,$c_password,$c_f_name,$c_l_name,$c_sex,$c_cl,$c_no,$c_endtime,$c_att,$c_def,$c_hit,$c_mhit,$c_level,$c_exp,$c_sta,$c_wep,$c_watt,$c_wtai,$c_bou,$c_bdef,$c_btai,$c_bou_h,$c_bdef_h,$c_btai_h,$c_bou_f,$c_bdef_f,$c_btai_f,$c_bou_a,$c_bdef_a,$c_btai_a,$c_tactics,$c_death,$c_msg,$c_sts,$c_pls,$c_kill,$c_icon,$c_item,$c_eff,$c_itai,$c_log,$c_com,$c_dmes,$c_bid,$c_club,$c_money,$c_wp,$c_wg,$c_wn,$c_wc,$c_wd,$c_comm,$c_limit,$c_bb,$c_inf,$c_ousen,$c_seikaku,$c_sinri,$c_item_get,$c_eff_get,$c_itai_get,$c_teamID,$c_teamPass,$c_IP;
@list($c_id,$c_password,$c_f_name,$c_l_name,$c_sex,$c_cl,$c_no,$c_endtime,$c_att,$c_def,$c_hit,$c_mhit,$c_level,$c_exp,$c_sta,$c_wep,$c_watt,$c_wtai,$c_bou,$c_bdef,$c_btai,$c_bou_h,$c_bdef_h,$c_btai_h,$c_bou_f,$c_bdef_f,$c_btai_f,$c_bou_a,$c_bdef_a,$c_btai_a,$c_tactics,$c_death,$c_msg,$c_sts,$c_pls,$c_kill,$c_icon,$c_item[0],$c_eff[0],$c_itai[0],$c_item[1],$c_eff[1],$c_itai[1],$c_item[2],$c_eff[2],$c_itai[2],$c_item[3],$c_eff[3],$c_itai[3],$c_item[4],$c_eff[4],$c_itai[4],$c_item[5],$c_eff[5],$c_itai[5],$c_log,$c_com,$c_dmes,$c_bid,$c_club,$c_money,$c_wp,$c_wg,$c_wn,$c_wc,$c_wd,$c_comm,$c_limit,$c_bb,$c_inf,$c_ousen,$c_seikaku,$c_sinri,$c_item_get,$c_eff_get,$c_itai_get,$c_teamID,$c_teamPass,$c_IP,) = explode(',',$_COOKIE['BR']);
}
#=================#
# ■ クッキー保存 #
#=================#
function CSAVE(){
global $in,$pref;

global $id,$password,$f_name,$l_name,$sex,$cl,$no,$att,$def,$hit,$mhit,$level,$exp,$sta,$wep,$watt,$wtai,$bou,$bdef,$btai,$bou_h,$bdef_h,$btai_h,$bou_f,$bdef_f,$btai_f,$bou_a,$bdef_a,$btai_a,$tactics,$death,$msg,$sts,$pls,$kill,$icon,$item,$eff,$itai,$com,$dmes,$bid,$money,$wp,$wg,$wn,$wc,$wd,$comm,$limit,$bb,$inf,$ousen,$seikaku,$sinri,$item_get,$eff_get,$itai_get,$teamID,$teamPass,$expires;

$cook = "$id,$password,$f_name,$l_name,$sex,$cl,$no,0,$att,$def,$hit,$mhit,$level,$exp,$sta,$wep,$watt,$wtai,$bou,$bdef,$btai,$bou_h,$bdef_h,$btai_h,$bou_f,$bdef_f,$btai_f,$bou_a,$bdef_a,$btai_a,$tactics,$death,$msg,$sts,$pls,$kill,$icon,$item[0],$eff[0],$itai[0],$item[1],$eff[1],$itai[1],$item[2],$eff[2],$itai[2],$item[3],$eff[3],$itai[3],$item[4],$eff[4],$itai[4],$item[5],$eff[5],$itai[5],,$com,$dmes,$bid,".$in['club'].",$money,$wp,$wg,$wn,$wc,$wd,$comm,$limit,$bb,$inf,$ousen,$seikaku,$sinri,$item_get,$eff_get,$itai_get,$teamID,$teamPass,".$in['IP'].",";
setcookie('BR',$cook,$pref['now'] + $pref['save_limit'] * 86400);
}
#=================#
# ■ クッキー削除 #
#=================#
function CDELETE(){
global $pref,$expires;

$cook = '2YBRU,,,,,,,'.$pref['now'].',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,';
setcookie('BR',$cook,$pref['now'] + $pref['save_limit'] * 86400);
}
#=============#
# ■ ログ保存 #
#=============#
function LOGSAVE($work){
global $pref;

require_once $pref['LIB_DIR'].'/lib4.php';
LOGSAVE2($work);
}
#===============#
# ■ バッファー #
#===============#
function bru($buffer){
return(ereg_replace("\n",'',ereg_replace("\"",'',$buffer)));
}
#===============#
# ■ ヘッダー部 #
#===============#
function HEAD(){
global $in,$pref;

global $REGIST,$HEADERINDEX,$sts,$user_agent;
if($pref['MOBILE']){
	ini_set('output_buffering','on');#出力バッファリングを有効 
	ini_set('output_handler','mb_output_handler');#出力の変換を有効にするためにmb_output_handlerを指定 
	ini_set('mbstring.http_input','auto');#HTTP入力文字エンコーディングのデフォルト値 
	ini_set('mbstring.internal_encoding','UTF-8');#内部文字エンコーディングのデフォルト値 
	ini_set('mbstring.http_output','GB2312');#HTTP出力文字エンコーディングのデフォルト値 
	mb_internal_encoding('UTF-8');
	mb_http_output('GB2312');#HTTP出力文字エンコーディングのデフォルト値 
	ob_start('mb_output_handler');
?>
<HTML><HEAD><meta http-equiv="content-type" content="text/html;charset=GB2312"><TITLE><?=$pref['game']?></TITLE><HEAD><BODY text="#ffffff" link="#ff0000" vlink="#ff0000" aLink="#ff0000" bgcolor="#000000"><center>
<?
}else{
	@header("Content-type: text/html;charset=UTF-8\n\n");
	@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	// 常に修正されている
	@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	// HTTP/1.1
	@header("Cache-Control: no-store, no-cache, must-revalidate");
	@header("Cache-Control: post-check=0, pre-check=0", false);
	// HTTP/1.0
	@header("Pragma: no-cache");
	
	print '<HTML><HEAD><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><META HTTP-EQUIV="Pragma" CONTENT="no-cache"><META HTTP-EQUIV="Expires" CONTENT="-1"><TITLE>'.$pref['game'].'</TITLE><LINK REL="stylesheet" TYPE="text/css" HREF="BRU.CSS">';
	print '</HEAD><BODY rightmargin="0" topmargin="0" leftmargin="0" bgcolor="#000000" text="#ffffff" link="#ff0000" vlink="#ff0000" aLink="#ff0000"';
	if($HEADERINDEX){
		print ' onload=\'typeMain("tw","color=#black","第三次冲击结束后，好男人的数量急剧减少$$以阿部为首的TG核心在绿坝娘的指引下开拓新的世界$$ $$这个世界还有希望么！萝莉无处觅，御姐哪里寻，伪娘尽当道，河蟹独横行……$$绝望了，人类对这个没好男人的世界绝望了！！$$ $$为了培育更多的纱布……不对是阿部，TG通过了新的政策……",9)\'>';
	#}elseif(preg_match("/BATTLE|ATK/",$in['Command'])){print "onload=\"quake_init()\"><SCRIPT language=JavaScript src=\"BRU.JS\"></SCRIPT>";
	}else{
		if(preg_match("/睡觉|治疗|静养/",$sts)||(isset($in['Command']) && (($in['Command'] == "MOVE" && $in['Command2'] == "MAIN")||($in['Command'] == "ITMAIN" && $in['Command3'] == "MAIN")||($in['Command'] == "SPECIAL" && $in['Command4'] == "MAIN")||($in['Command'] == "kaifuku" && $in['Command5'] == "MAIN")))){
			print ' onload="document.MSG.Mess.focus()"><SCRIPT language=JavaScript src="BRU.JS"></SCRIPT>';
		}else{
			print '><SCRIPT language=JavaScript src="BRU.JS"></SCRIPT>';
		}
	}
	print '<center><DIV ID="BRU">';//<!--{$user_agent[0]}TABLE ID=\"BRU\" border=\"0\" height=\"100%\" width=\"100%\" cellspacing=\"0\" collspacing=\"0\"><TR><TD-->";
}
}
#=============#
# ■ フッタ部 #
#=============#
function FOOTER(){
global $pref;
if($pref['MOBILE']){
?>
<br>
<small>
BATTLE ROYALE <?=$pref['ver']?><br>
<a href="http://www.b-r-u.net/">BRU (Battle Royale Ultimate) Ver. <?=$pref['ver2y']?></a></font></b>
</small>
</center>
</BODY>
</HTML>
<?
	@ob_end_flush();
}else{
?>
</CENTER>
<!--/TD></TR><TR><TD height="20%"-->
<div width="100%">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="2">
<HR>
</tr></td>
<tr><td>
<font color="yellow">忠告：基多了会消耗体力，基少了会失去生命。<br/>　　　\纪念饭否永垂不朽谨献此作/</font>
</td><td>
<p style="text-align: right">
<b><font face="Viner Hand ITC" color="#ff0000" style="font-size: 9pt">
BATTLE ROYALE <?=$pref['ver']?><br>
<a href="http://www.b-r-u.net/">BRU (Battle Royale Ultimate) Ver. <?=$pref['ver2y']?></a></font></b>
</p></tr></td>
</table>
</TD></TR>
</table>
<?=$pref['footer']?>
</BODY>
</HTML>
<?
}
}
#===============#
# ■ エラー処理 #
#===============#
function debug($str){#■エラー画面
global $pref;

#New User Save
$handle = @fopen($pref['LOG_DIR'].'/debug.log', 'a') or ERROR('unable to open file user_file','','REGIST',__FUNCTION__,__LINE__);
if(!@fwrite($handle,$str)){ERROR('cannot write to user_file','','REGIST',__FUNCTION__,__LINE__);}
fclose($handle);

}
#===============#
# ■ エラー処理 #
#===============#
function ERROR($errmes0='未知错误',$errmes1='N/A',$errmes2='N/A',$errmes3='N/A',$errmes4='N/A'){#■エラー画面
global $in,$pref;

if($pref['lockf']){UNLOCK();}
HEAD();
?>
<B><FONT color="#ff0000" size="+3" face="ＭＳ 明朝">Alarm！Alarm！</FONT></B><BR><BR>
<FONT size="2"><?=$errmes0?><BR><BR>
Comment:<?=$errmes1?><BR><BR>
ERROR ID:<?=$errmes2?> - <?=$errmes3?> - <?=$errmes4?><BR><BR>
COMMAND: -1- <?=$in['Command']?> -2- <?=$in['Command2']?> -3- <?=$in['Command3']?> -4- <?=$in['Command4']?> -5- <?=$in['Command5']?><BR><BR>
<BR><B><FONT color="#ff0000"><A href="index.php">HOME</A></B></Font>
<?
FOOTER();
exit;
}
#=============#
# ■ 自動選択 #
#=============#
function Auto($AS_MES = '',$CMD = "sl"){#AS => Auto
global $ASN,$pref;
if($ASN == ''){$ASN = 0;}
if($AS_MES){
	print '<a title="'.$AS_MES.'"';
}else{
	$AS_MES = ' ';
	print '<a';
}
if($pref['MOBILE']){
	print '>';
}else{
	print ' onclick='.$CMD.'('.$ASN.'); onmouseover="status=\''.$AS_MES.'\';return true;">';
}
$ASN++;
}
#===============#
# ■ IDの暗号化 #
#===============#
function IDcrypt($chk_id){
	#return substr(crypt($chk_id,"br"),2);
	return $chk_id;
}
#===========#
# ■ ロック #
#===========#
function LOCK(){
global $pref,$fp;
$fp = fopen($pref['lockf'],'w+');
flock($fp,LOCK_EX);
$lockflag=1;
}
#===============#
# ■ アンロック #
#===============#
function UNLOCK(){
global $fp;
flock($fp,LOCK_UN);
}
?>