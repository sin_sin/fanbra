<?php
#□■□■□■□■□■□■□■□■□■□■□
#■ 	-    BR LIBRARY PROGRAM    - 	 ■
#□ 									 □
#■ 		  サブルーチン一覧  		 ■
#□ 									 □
#■ MOVE			-		移動		 ■
#□ SEARCH			-		探索処理	 □
#■ SEARCH2			-		探索処理2	 ■
#□ TACTGET			-		戦略計算	 □
#■ TACTGET2		-		戦略計算	 ■
#□■□■□■□■□■□■□■□■□■□■□
#=========#
# ■ 移動 #
#=========#
function MOVE(){
global $in,$pref;

global $pls,$log,$inf,$sta,$hackflg,$sts;
$mv = $in['Command2'];
$mv = str_replace('MV','',$mv);

#移動前処理
$in['Command'] = 'MAIN';
$in['Command2'] = '';
if($mv == $pls){$log .= '不能移动到同一个地方吧喂！';return;}
$ok = 1;
if((preg_match('/足/',$inf))&&($sta > 18)){
	$ok = 0;
}elseif((preg_match('/定向越野部|足球部/',$pref['clb'][$in['club']]))&&($sta > 11)){
	$ok = 0;
}elseif($sta > 13){
	$ok = 0;
}
if($ok){$log .= '体力不足，走不动啊……';return;}

if(preg_match('/足/',$inf)){$sta -= rand(13,18);}
elseif(preg_match('/定向越野部|足球部/',$pref['clb'][$in['club']])){$sta -= rand(6,11);}
else{$sta -= rand(8,13);}

list($ar[0],$ar[1],$ar[2],$ar[3],$ar[4],$ar[5],$ar[6],$ar[7],$ar[8],$ar[9],$ar[10],$ar[11],$ar[12],$ar[13],$ar[14],$ar[15],$ar[16],$ar[17],$ar[18],$ar[19],$ar[20],$ar[21]) = explode(",",$pref['arealist'][2]);
list($war,$a) = explode(',',$pref['arealist'][1]);
if($ar[$war] == $pref['place'][$mv]){
	$log .= $pref['place'][$mv].'到了。<br>但是据说绿坝娘会来这儿。'.$pref['arinfo'][$mv].'<br>';
}elseif(($ar[$war+1] == $pref['place'][$mv])||($ar[$war+2] == $pref['place'][$mv])){
	$log .= $pref['place'][$mv].'到了。<br>但是据说绿坝娘马上就来来这儿。<br>'.$pref['arinfo'][$mv].'<br>';
}else{
	for($i=0; $i<$war; $i++){
		if(($ar[$i] == $pref['place'][$mv])&&($hackflg == 0)&&(!preg_match('/NPC/',$sts))){#禁止エリア？
			$log .= $pref['place'][$mv].'是绿坝娘的地盘啊<BR>';
			return;
		}
	}
	$log .= $pref['place'][$mv].'到达了。<BR>'.$pref['arinfo'][$mv].'<br>';
}

$pls = $mv;#移動

if(preg_match("/毒/",$inf)){
	global $hit;
	$minus = rand(0,8);
	$hit -= $minus;
	$log .= '因为中毒你损失了<b><font color="red">'.$minus.'</font></b>点生命。<BR>';
	require $pref['LIB_DIR'].'/lib3.php';
	POISONDEAD();
}

SEARCH2();

SAVE();

}
#=============#
# ■ 探索処理 #
#=============#
function SEARCH(){
global $in,$pref;

global $inf,$sta,$log,$hit,$l_name,$chksts;

$ok = 1;
if((preg_match('/足/',$inf))&&($sta > 25)){
	$ok = 0;
}elseif((preg_match('/定向越野部|足球部/',$pref['clb'][$in['club']]))&&($sta > 15)){
	$ok = 0;
}elseif($sta > 20){
	$ok = 0;
}
if($ok){$log .= '体力不足，不能探索啊……';return;}

$log .= $l_name.'，随便转转吧…。<br>';

if(preg_match('/足/',$inf)){$sta -= rand(20,25);}
elseif(preg_match('/定向越野部|足球部/',$pref['clb'][$in['club']])){$sta -= rand(10,15);}
else{$sta -= rand(15,20);}

if(preg_match('/毒/',$inf)){
	$minus = rand(0,8);
	$hit -= $minus;
	$log .= '因为中毒你损失了<b><font color="red">'.$minus.'</font></b>点生命。<BR>';
	require $pref['LIB_DIR'].'/lib3.php';
	POISONDEAD();
}

SEARCH2();

if($chksts != 'OK'){$log .= '但是你什么也没有找到。<BR>';$in['Command'] = 'MAIN';}

SAVE();
}
#==============#
# ■ 探索処理2 #
#==============#
function SEARCH2(){
global $pref;

global $chkpnt2,$kiri,$chkpnt,$tactics,$pls,$id,$chksts,$chksts2,$teamID,$teamPass;
$i = 0;

$dice1 = rand(0,10);	#敵、アイテムどちらを発見

TACTGET();

$userlist = @file($pref['user_file']) or ERROR('unable to open file user_file user_file','','LIB2',__FUNCTION__,__LINE__);

$chksts='NG';$chksts2='NG';

$plist = array();

if($dice1 <= 5){	#敵発見？
	for ($i=0; $i<count($userlist); $i++){
		list($w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP) = explode(",",$userlist[$i]);
		if(($w_pls == $pls)&&($w_id != $id)){#同じ場所&他人
			$b_bid = $w_bid;
			if($tactics == '连斗行动'){
				$w_bid = '';
				$randam_set = 50;#連闘率
				if($w_tactics != '连斗行动'){$randam_set = 75;}#連闘率
				if(rand(0,100) >= $randam_set){$w_bid = $b_bid;}
			}
			if(preg_match("/\&/",$w_bid)){#分割
				list($w_bid1,$w_bid2) = explode('&',$w_bid);
			}else{
				$w_bid1 = $w_bid;
				$w_bid2 = '&';
			}
			if(($w_bid1 != $id)&&($w_bid2 != $teamID)){
				array_push($plist,$i);
			}
		}
	}

	$plist2 = array();
	if(isset($plist[0])){
		for($i=0;$i<count($plist);$i++){
			$r = rand(0,$i+1);
			$plist2[$r] = $plist[$i];
			array_push($plist2,$plist2[$r]);
		}
	}

	//敵GLOBAL
	global $w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item,$w_eff,$w_itai,$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP;
	foreach($plist2 as $i){
		$dice2 = rand(0,10);	#敵、アイテム発見
		$dice3 = rand(0,10);	#先制攻撃
		$dice6 = rand(0,100);	#視野
		list($w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP) = explode(",",$userlist[$i]);

		if(($w_pls == $pls)&&($w_id != $id)){	#場所一致他プレイヤー？
			$b_bid = $w_bid;
			if($tactics == '连斗行动'){
				$w_bid = '';
			}
			if(preg_match("/\&/",$w_bid)){#分割
				list($w_bid1,$w_bid2) = explode('&',$w_bid);
			}else{
				$w_bid1 = $w_bid;
				$w_bid2 = '&';
				if($teamID == ''){#team故障修正
					$teamID = $teamPass = '无';
				}
			}
			if(($w_bid1 != $id)&&($w_bid2 != $teamID)){
				list($w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP) = explode(",",$userlist[$i]);

				global $sen,$w_pls,$w_club;
				TACTGET2();$chk = (int)($dice2 * $sen);

				if($chk < $chkpnt){#相手発見
					global $bb;
					if($w_hit > 0){#生存？
						$kiri = 0;#霧Script
						#list($weth) = $pref['arealist'][5];
						#if(!preg_match("/0|1|2|7/",$weth)){#晴天・晴れ・曇りを除く
						#	$kiri = $weth * 3;#最大3*9=27(27%)
						#	if($dice6 < $kiri){$kiri = 1;}else{$kiri = 0;}
						#}
						$bb = $w_id;	#ブラウザバック対処用	↓チーム
						if((!$kiri)&&($teamID != '无')&&($teamID != '')&&($teamID == $w_teamID)&&($teamPass == $w_teamPass)){#譲渡
							require $pref['LIB_DIR'].'/att_etc.php';
							ATTJYO();$chksts='OK';$chksts2='NG';break;
						}elseif($dice3 <= $chkpnt2 || $w_ousen=='专注' || $w_ousen=='逃跑姿态'){//相手が专注/逃跑姿态の時は相手は奇襲ができないため
							require $pref['LIB_DIR'].'/att_etc.php';
							ATTACK();$chksts='OK';$chksts2='NG';break;	#先制攻撃
						}else{
							$w_bid = $id;
							if(($teamID != '无')&&($teamID != '')){$w_bid .= '&'.$teamID;}
							$bid = $w_id;
							if(($w_teamID != '无')&&($w_teamID != "")){$bid .= '&'.$w_teamID;}
							require $pref['LIB_DIR']."/attack.php";
							require $pref['LIB_DIR']."/att_kou.php";
							ATTACK2();$chksts='OK';$chksts2="NG";break;	#後攻攻撃（奇襲）
						}
					}else{#死体発見
						$chkflg = 0;
						$dice4 = rand(0,10);
						if($dice4 > 6){
							for($j=0;$j<6;$j++){
								if($w_item[$j] != '无' && $w_item[$j] != ""){
									$chkflg=1;break;
								}
							}
							if($chkflg){
								if((!preg_match('/空手/',$w_wep))||(!preg_match('/内衣/',$w_bou))||($w_bou_h != '无')||($w_bou_f != '无')||($w_bou_a != '无')||($w_money > 0)){
									$w_sta = -1;
									$w_bid = $id;
									SAVE2();
									$bb = $w_id;#ブラウザバック対処用
									require $pref['LIB_DIR'].'/att_etc.php';DEATHGET();break;
								}
							}
						}
					}
				}
			}else{
				$chksts2='OK';
			}
		}
	}

	if($chksts2 == 'OK'){#アイテム発見・空
		global $in,$log;
		$dice5 = rand(0,10);#アイテム発見
		if(($dice5 <= 5)&&($in['Command'] == 'SEARCH')){
			require $pref['LIB_DIR'].'/item1.php';ITEMGET();
		}else{
			$log .= '感觉有什么潜伏在附近……是士兵吗？<BR>';
		}
	}
}else{	#イベント・アイテム発見
	$dice2 = rand(0,10);
	if($dice2 <= $chkpnt){require $pref['LIB_DIR'].'/item1.php';ITEMGET();}#アイテム発見
	else{require $pref['LIB_DIR'].'/event.php';EVENT();}
}

}
#=============#
# ■ 戦略計算 #
#=============#
function TACTGET(){
global $in,$pref;

global $tactics,$jyuku1,$pls,$pls,$inf,$w_kind,$wp,$wg,$wn,$wc,$wd,$chkpnt,$chkpnt2,$weps,$mei;
global $atp,$dfp,$w_wtai;
$chkpnt = 5;	#敵、アイテム発見率
$chkpnt2 = 5;	#先制攻撃率
$atp = 1.00;
$dfp = 1.00;#					攻撃			防御		発見率		 先制攻撃率
#基本方針
if	  ($tactics == '菊爆攻击型态')	{$atp+=0.2;	$dfp-=0.2;}
elseif($tactics == '捂菊防御型态')	{$atp-=0.2;	$dfp+=0.2;					$chkpnt2-=1;}
elseif($tactics == '隐秘尾行型态')	{$atp-=0.2;	$dfp-=0.1;	$chkpnt-=1;		$chkpnt2+=2;}
elseif($tactics == '偷基摸菊形态')	{$atp-=0.05;$dfp-=0.05;	$chkpnt+=2;		$chkpnt2+=1;}
elseif($tactics == '连斗行动')	{$dfp-=0.1;	$dfp-=0.1;	$chkpnt-=0.5;	$chkpnt2-=0.05;}
#天気
if	  ($pref['arealist'][5] == '0')	{$atp+=0.2;	$dfp+=0.3;	$chkpnt+=2;		$chkpnt2+=2;}	#快晴
elseif($pref['arealist'][5] == '1')	{$atp+=0.2;	$dfp+=0.1;	$chkpnt+=1;		$chkpnt2+=1;}	#晴れ
elseif($pref['arealist'][5] == '2')	{;}														#曇り
elseif($pref['arealist'][5] == '3')	{$atp-=0.02;$dfp-=0.03;	$chkpnt-=0.2;	$chkpnt2-=0.3;}	#雨
elseif($pref['arealist'][5] == '4')	{$atp-=0.05;$dfp-=0.03;	$chkpnt-=0.3;	$chkpnt2-=0.5;}	#豪雨
elseif($pref['arealist'][5] == '5')	{$atp-=0.07;$dfp-=0.05;	$chkpnt-=0.7;	$chkpnt2-=0.5;}	#台風
elseif($pref['arealist'][5] == '6')	{$atp-=0.07;$dfp-=0.1;	$chkpnt-=1;		$chkpnt2-=0.7;}	#雷雨
elseif($pref['arealist'][5] == '7')	{$atp-=0.1;	$dfp-=0.15;	$chkpnt-=0.5;	$chkpnt2+=1;}	#雪
elseif($pref['arealist'][5] == '8')	{			$dfp-=0.2;	$chkpnt+=1;		$chkpnt2-=1;}	#霧
elseif($pref['arealist'][5] == '9')	{$atp+=0.5;	$dfp-=0.3;					$chkpnt2-=1;}	#濃霧
#エリア
if	  ($pref['arsts'][$pls] == 'AU')	{$atp+=0.1;}	#攻撃増
elseif($pref['arsts'][$pls] == 'AD')	{$atp-=0.1;}	#攻撃減
elseif($pref['arsts'][$pls] == 'DU')	{$dfp+=0.1;}	#防御増
elseif($pref['arsts'][$pls] == 'DD')	{$dfp-=0.1;}	#防御減
elseif($pref['arsts'][$pls] == 'SU')	{$chkpnt+=1;}	#発見増
elseif($pref['arsts'][$pls] == 'SD')	{$chkpnt-=1;}	#発見減
#部活
if	  ($pref['clb'][$in['club']] == '艺术部'){
						$chkpnt+=0.1;		$chkpnt2+=1;
}elseif($pref['clb'][$in['club']] == '管弦乐部'){
				$dfp+=0.1;	$chkpnt+=0.1;
}elseif($pref['clb'][$in['club']] == '军乐队部'){
						$chkpnt+=0.1;
}elseif($pref['clb'][$in['club']] == '蓝蓝路研究会' && preg_match('/绿坝娘中心|情侣宾馆废楼/',$pref['place'][$pls])){
	$atp+=0.1;	$dfp+=0.1;
}elseif($pref['clb'][$in['club']] == '书道部'){
				$dfp+=0.1;
}elseif($pref['clb'][$in['club']] == '围棋部'){
				$dfp+=0.1;
}elseif($pref['clb'][$in['club']] == '国际象棋部'){
						$chkpnt+=0.1;
}elseif($pref['clb'][$in['club']] == '篮球部' && $teamID != '无'){
	$atp+=0.1;
}elseif($pref['clb'][$in['club']] == '游泳部' && preg_match('/绿坝娘中心|情侣宾馆废楼/',$pref['place'][$pls])){
				$dfp-=0.1;
}elseif($pref['clb'][$in['club']] == '橄榄球部'){
	$atp+=0.1;
}elseif($pref['clb'][$in['club']] == '定向越野部'){
				$dfp-=0.1;
}
#負傷
if(preg_match('/腕/',$inf)){$atp -= 0.2;}

$kind = $w_kind;
$wmei = 0;
$wweps = '';
$jyuku1 = 0;#必殺技

if((preg_match('/G/',$kind))&&($w_wtai == 0)){$wweps = 'S';$wmei = 80;$wmei += round($wp/$pref['BASE']);$jyuku1 = $wp;}#棍棒/弾無し銃
elseif(preg_match('/C/',$kind))	{$wweps = 'M';$wmei = 70;$wmei += round($wc/$pref['BASE']);$jyuku1 = $wc;}#投
elseif(preg_match('/D/',$kind))	{$wweps = 'L';$wmei = 50;$wmei += round($wd/$pref['BASE']);$jyuku1 = $wd;}#爆
elseif(preg_match('/G/',$kind))	{$wweps = 'M';$wmei = 50;$wmei += round($wg/$pref['BASE']);$jyuku1 = $wg;}#銃
elseif(preg_match('/K/',$kind))	{$wweps = 'S';$wmei = 80;$wmei += round($wn/$pref['BASE']);$jyuku1 = $wn;}#斬
else 							{$wweps = 'S';$wmei = 70;$wmei += round($wp/$pref['BASE']);$jyuku1 = $wp;}#手

$weps = $wweps;
$mei = $wmei;

if(preg_match('/书道部|足球部/',$pref['clb'][$in['club']])){$mei += 10;}
if(preg_match('/头/',$inf)){$mei -= 20;}
}
#=============#
# ■ 戦略計算 #
#=============#
function TACTGET2(){
global $pref;

global $atn,$dfn,$sen,$w_ousen,$w_pls,$w_club,$w_club,$w_inf,$w_kind2,$weps2,$mei2,$jyuku2,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd;
global $pls,$w_teamID,$teamID;
global $atn,$dfn,$sen,$w_wtai;

$atn = 1.00;
$dfn = 1.00;
$sen = 1.0;#						攻撃		防御		遭遇率		奇襲(相手の先制攻撃率)
if	  ($w_ousen == '菊爆攻击型态')	{$atn+=0.2;	$dfn-=0.2;	$sen+=0.05;	} 
elseif($w_ousen == '捂菊防御型态')	{$atn-=0.2;	$dfn+=0.2;	$sen-=0.05;	}
elseif($w_ousen == '隐秘尾行型态')	{$atn-=0.1;	$dfn-=0.2;	$sen+=0.1;	}
#elseif($w_ousen == '偷基摸菊型态')	{$atn-=0.2;	$dfn-=0.2;	$sen-=0.4;	}
#elseif($w_ousen == '连斗行动')	{			$dfn-=0.4;	$sen-=0.3;	}
elseif($w_ousen == '专心治疗')	{			$dfn-=0.4;}
elseif($w_ousen == '逃亡姿态')	{			$dfn-=0.2;}
#天気
if	  ($pref['arealist'][5] == '0')	{$atn+=0.2;	$dfn+=0.3;}	#快晴
elseif($pref['arealist'][5] == '1')	{$atn+=0.2;	$dfn+=0.1;}	#晴れ
elseif($pref['arealist'][5] == '2')	{;}						#曇り
elseif($pref['arealist'][5] == '3')	{$atn-=0.02;$dfn-=0.03;}#雨
elseif($pref['arealist'][5] == '4')	{$atn-=0.05;$dfn-=0.03;}#豪雨
elseif($pref['arealist'][5] == '5')	{$atn-=0.07;$dfn-=0.05;}#台風
elseif($pref['arealist'][5] == '6')	{$atn-=0.07;$dfn-=0.1;}	#雷雨
elseif($pref['arealist'][5] == '7')	{$atn-=0.1;	$dfn-=0.15;}#雪
elseif($pref['arealist'][5] == '8')	{			$dfn-=0.2;}	#霧
elseif($pref['arealist'][5] == '9')	{$atn+=0.5;	$dfn-=0.3;}	#濃霧
#エリア
if	  ($pref['arsts'][$w_pls] == 'AU')	{$atn+=0.1;}#攻撃増
elseif($pref['arsts'][$w_pls] == 'AD')	{$atn-=0.1;}#攻撃減
elseif($pref['arsts'][$w_pls] == 'DU')	{$dfn+=0.1;}#防御増
elseif($pref['arsts'][$w_pls] == 'DD')	{$dfn-=0.1;}#防御減
#部活
if	  ($pref['clb'][$w_club] == '军乐队部'){
	$sen+=0.05;
}elseif(($pref['clb'][$w_club] == '合唱部')&&($w_teamID == $teamID)){
	$sen-=0.2;
}elseif(($pref['clb'][$w_club] == '排球部')&&($pref['place'][$pls] == '永琳医保定点单位')){
	$sen+=0.05;
}elseif($pref['clb'][$w_club] == '游泳部' && preg_match('/绿坝娘中心|情侣宾馆废楼/',$pref['place'][$pls])){
	$dfn-=0.1;
}elseif($pref['clb'][$w_club] == '定向越野部'){
	$dfn-=0.1;
}
#負傷
if(preg_match('/腕/',$w_inf)){$atn -= 0.1;}

$kind = $w_kind2;
$wmei = 0;
$wweps = '';
$jyuku2 = 0;#必殺技

if(preg_match('/G/',$kind) && $w_wtai == 0){$wweps = 'S';$wmei = 80;$wmei += round($w_wp/$pref['BASE']);$jyuku2 = $w_wp;}#棍棒/弾無し銃
elseif(preg_match('/C/',$kind))	{$wweps = 'M';$wmei = 70;$wmei += round($w_wc/$pref['BASE']);$jyuku2 = $w_wc;}#投
elseif(preg_match('/D/',$kind))	{$wweps = 'L';$wmei = 50;$wmei += round($w_wd/$pref['BASE']);$jyuku2 = $w_wd;}#爆
elseif(preg_match('/G/',$kind))	{$wweps = 'M';$wmei = 50;$wmei += round($w_wg/$pref['BASE']);$jyuku2 = $w_wg;}#銃
elseif(preg_match('/K/',$kind))	{$wweps = 'S';$wmei = 80;$wmei += round($w_wn/$pref['BASE']);$jyuku2 = $w_wn;}#斬
else 							{$wweps = 'S';$wmei = 70;$wmei += round($w_wp/$pref['BASE']);$jyuku2 = $w_wp;}#手

$weps2 = $wweps;
$mei2 = $wmei;

if(preg_match('/书道部|足球部/',$pref['clb'][$w_club])){$mei2 += 10;}
if(preg_match('/头/',$w_inf)){$mei2 -= 20;}
}

?>