<?
#□■□■□■□■□■□■□■□■□■□■□
#■ 	  -    BR ITEM SCRIPT    -  	 ■
#□ 									 □
#■ 		  サブルーチン一覧  		 ■
#□ 									 □
#■ ITEMGET		-	アイテム取得		 ■
#□ ITEMNEWXCG	-	アイテム交換		 □
#■ ITEMDELNEW	-	アイテムを捨てる	 ■
#□ ITEMGETNEW	-	アイテムを拾う		 □
#■ ITEMDEL		-	アイテム投棄		 ■
#■ ITEM		-	アイテム使用		 ■
#□ omikuji1	-	御神签ランダム		 □
#■ omikuji2	-	御神签ランダム		 ■
#□ WEPDEL		-	装備武器を外す処理	 □
#■ WEPDEL2		-	装備武器投棄		 ■
#□ BOUDELH		-	头防具を外す処理	 □
#■ BOUDELB		-	体防具を外す処理	 ■
#□ BOUDELA		-	腕防具を外す処理	 □
#■ BOUDELF		-	足防具を外す処理	 ■
#□ BOUDEL		-	装饰品を外す処理	 □
#■□■□■□■□■□■□■□■□■□■□■
#=================#
# ■ アイテム取得 #
#=================#
function ITEMGET(){
global $in,$pref;

global $chksts,$pls,$item,$eff,$itai,$log,$item_get,$eff_get,$itai_get;
$i = 0;
$chkflg = 0;
$function = '';

$filename = $pref['LOG_DIR'].'/'.$pls.$pref['item_file'];

$itemlist = @file($filename);
if(count($itemlist) <= 0){
	$in['Command'] = 'MAIN';
	$log .= '真是的，这里连片菊花瓣都被刮得一片不剩了么…？<BR>';
	$chksts = 'OK';
	return;
}else{
	$work = rand(0,count($itemlist)-1);#取得アイテムランダム
	list($getitem,$geteff,$gettai) = explode(",",$itemlist[$work]);#取得アイテム情報取得
	list($itname,$itkind) = explode("<>",$getitem);#取得アイテム分割
	$delitem = array_splice($itemlist,$work,1);#取得アイテム削除
	$handle = @fopen($filename,'w') or ERROR("unable to open filename","","ITEM1",__FUNCTION__,__LINE__);
	@fwrite($handle,implode('',$itemlist));#){ERROR("unable to write filename","","ITEM1",__FUNCTION__,__LINE__);}
	fclose($handle);

	if(preg_match("/<>TO/",$getitem)){#罠
		global $hit,$f_name,$l_name,$cl,$sex,$no;
		$result = round(rand(0,$geteff/2)+($geteff/2));
		$log .= '陷阱！被算计了！'.$itname.'居然弄破了我老垢厚如青砖的皮，<BR>　受到<font color="red"><b>'.$result.'</b></font>的伤害！。<BR>';
		$hit-=$result;
		if($hit <= 0){#死亡ログ
			$hit = 0;
			$log .= '<font color="red"><b>'.$f_name.' '.$l_name.'（'.$cl.' '.$sex.$no.'号）已经被菊爆了。</b></font><br>';
			LOGSAVE('DEATH');
			$mem--;
			if($mem == 1){LOGSAVE('END');}
		else
			LOGSAVE("TRAP");
		}
		return;
	}#↓アイテム取得コメントLog
	elseif(preg_match("/<>HH|<>HD/",$getitem))	{$function = "喝下去就可以回复生命的吧。";}
	elseif(preg_match("/<>SH|<>SD/",$getitem))	{$function = "喝下去就可以回复体力的吧。";}
	elseif(preg_match("/<>W/",$getitem))		{$function = "这个可以用作武器呢。";}#武器
	elseif(preg_match("/<>D/",$getitem))		{$function = "这个可以用作防具呢。";}#防具
	elseif(preg_match("/<>A/",$getitem))		{$function = "这个可以戴在身上的吧。";}#装饰
	elseif(preg_match("/<>TN/",$getitem))		{$function = "这个可以用来当作陷阱阴人的吧。";}#罠
	else										{$function = "肯定有什么用吧。";}

	list($itname,$kind) = explode("<>",$getitem);
	if(preg_match("/HH|HD/",$kind)){$itemtype = "【生命回复】";}
	elseif(preg_match("/SH|SD/",$kind)){$itemtype = "【体力回复】";}
	elseif($kind == "TN")		{$itemtype = "【陷阱】";}
	elseif(preg_match("/W/",$kind))	{$itemtype = "【武器：";
		if(preg_match("/G/",$kind))	{$itemtype = ($itemtype . "射");}
		if(preg_match("/K/",$kind))	{$itemtype = ($itemtype . "斩");}
		if(preg_match("/C/",$kind))	{$itemtype = ($itemtype . "投");}
		if(preg_match("/B/",$kind))	{$itemtype = ($itemtype . "殴");}
		if(preg_match("/D/",$kind))	{$itemtype = ($itemtype . "爆");}
		$itemtype = ($itemtype . "】");}
	elseif(preg_match("/D/",$kind))	{$itemtype = "【防具：";
		if(preg_match("/B/",$kind))	{$itemtype = ($itemtype . "体");}
		if(preg_match("/H/",$kind))	{$itemtype = ($itemtype . "头");}
		if(preg_match("/F/",$kind))	{$itemtype = ($itemtype . "足");}
		if(preg_match("/A/",$kind))	{$itemtype = ($itemtype . "腕");}
		$itemtype = ($itemtype . "】");}
	elseif(($kind == "R1")||($kind == "R2")){$itemtype = "【雷达】";}
	elseif($kind == "Y"){
		if(($itname == "腐烂的滋味酱")||($itname == "滋味酱")){$itemtype = "【滋味酱】";}
		elseif($itname == "子弹"){$itemtype = "【弹】";}
		else{$itemtype = "【道具】";}}
	elseif($kind == "A"){$itemtype = "【装饰品】";}
	else{$itemtype = "【不明】";}

	$log .= $itname.'（效:'.$geteff.'数:'.$gettai.'）<font color="00ffff">'.$itemtype.'</font>被发现了。<br>'.$function.'<BR>';

	$chkflg = 0;
	for ($i=0;$i<5;$i++){#同一アイテム判断
		if($item[$i] == $getitem){
			if(preg_match("/子弹/",$item[$i])){
				$eff[$i] += $geteff;
				$chkflg = 1;
				break;
			}elseif(preg_match("/御神签|磨刀石|钉\<|解毒剂|裁缝道具|电池/",$item[$i])){#效1数X
				$itai[$i] *= $eff[$i];
				$gettai *= $geteff;
				$eff[$i] = 1;
				$itai[$i] += $gettai;
				$chkflg = 1;
				break;
			#}elseif(preg_match("/<>WC|<>WD|<>TN|御神签|磨刀石|钉|解毒剂|裁缝道具|电池/",$item[$i])){
			#	$chkflg = 0;
			#	break;
			}elseif($eff[$i] == $geteff && preg_match("/水|面包/",$item[$i])){
				$in['Command2'] = "SAME_ITEM";
				break;
			}
		}
	}

	if(!$chkflg){
		$in['Command'] = "ITMAIN";
		$in['Command3'] = "GET";
		$item_get = $getitem;$eff_get = $geteff;$itai_get = $gettai;
	}
}
SAVE();
$chksts="OK";
}
#=================#
# ■ アイテム交換 #
#=================#
function ITEMNEWXCG(){
global $in,$pref;

global $item,$eff,$itai,$item_get,$eff_get,$itai_get,$log,$pls;
$wk = $in['Command'];
$wk = str_replace ('ITEMNEWXCG_','',$wk);

if(($item[$wk] == "无")||($item_get == "无")){ERROR("不正当登录。","","ITEM1",__FUNCTION__,__LINE__);}
list($itn, $ik) = explode("<>",$item_get);
list($itn2, $ik2) = explode("<>",$item[$wk]);

$log .= $itn.'被放进包里，我把'.$itn2.'扔了。<br>';

$filename = $pref['LOG_DIR'].'/'.$pls.$pref['item_file'];

$itemlist = @file($filename);
array_push($itemlist,"$item[$wk],$eff[$wk],$itai[$wk],\n");
$handle = @fopen($filename,'w') or ERROR("unable to open filename","","ITEM1",__FUNCTION__,__LINE__);
if(!@fwrite($handle,implode('',$itemlist))){ERROR("unable to write filename","","ITEM1",__FUNCTION__,__LINE__);}
fclose($handle);

$item[$wk] = $item_get;
$eff[$wk] = $eff_get;
$itai[$wk] = $itai_get;

$item_get = "无";$eff_get = 0;$itai_get = 0;
$in['Command'] = "MAIN";

SAVE();
}
#=================#
# ■ アイテム整理 #
#=================#
function ITEMSEINEW(){
global $in;

global $log,$item,$eff,$itai,$item_get,$eff_get,$itai_get;

$wk = $in['Command'];
$wk = str_replace('ITEMSEINEW_','',$wk);

list($itn, $ik) = explode('<>',$item[$wk]);
list($itn2,$ik2) = explode('<>',$item_get);
$ik3 = '';

if($item[$wk]=='无' || $item_get=='无'){
	ERROR('不正当登录。','','ITEM1',__FUNCTION__,__LINE__);
}

$log .= '道具整理。<br>';

if($itn==$itn2 && $eff[$wk]==$eff_get && $ik{0}==$ik2{0} && ($ik{1}=='H' || $ik{1}=='D') && ($ik2{1}=='H' || $ik2{1}=='D')){//回复アイテム整理
	if($ik{1}=='D' || $ik2{1}=='D'){//毒
		if($ik{2}==$ik2{2}){
			$ik3 = $ik{0}.'D'.$ik2{2};
		}elseif($ik{2}=='2'){
			$ik3 = $ik;
		}elseif($ik2{2}=='2'){
			$ik3 = $ik2;
		}elseif($ik{2}=='1'){
			$ik3 = $ik;
		}elseif($ik2{2}=='1'){
			$ik3 = $ik2;
		}elseif(strlen($ik2)==2){
			$ik3 = $ik;
		}elseif(strlen($ik)==2){
			$ik3 = $ik2;
		}else{
			$ik3 = $ik{0}.'D';
		}
	}else{
		$ik3 = $ik;
	}
}else{#違うアイテム・纏められない物選択
	ERROR('不正当登录。','','ITEM1',__FUNCTION__,__LINE__);
}

$itai[$wk] = $itai[$wk] + $itai_get;
$item[$wk] = $itn.'<>'.$ik3;
$item_get = '无';$eff_get = 0;$itai_get = 0;
$log .= $itn.'合并了。<br>';

$in['Command']  = 'MAIN';
$in['Command2'] = '';

SAVE();

}
#=====================#
# ■ アイテムを捨てる #
#=====================#
function ITEMDELNEW(){
global $in,$pref;

global $item_get,$eff_get,$itai_get,$log,$pls;
if(($item_get == "无")||($item_get == '')){$log .= '拿起来看看还是不要了。<br>';
}elseif(($eff_get != 0)||($itai_get != 0)){
	list($itn, $ik) = explode("<>",$item_get);
	if($ik != ''){
		if($itn['Command'] == "ITEMDELNEW"){$log .= $itn.'拿起来看看还是不要了。<br>';}#アイテムの書き出し
		$filename = $pref['LOG_DIR'].'/'.$pls.$pref['item_file'];
		$itemlist = @file($filename);
		if($itemlist == ''){$itemlist = array();}
		array_push($itemlist,"$item_get,$eff_get,$itai_get,\n");
		$handle = @fopen($filename,'w') or ERROR("unable to open filename","","ITEM1",__FUNCTION__,__LINE__);
		if(!@fwrite($handle,implode('',$itemlist))){ERROR("unable to write filename","","ITEM1",__FUNCTION__,__LINE__);}
		fclose($handle);
	}
}
$item_get = "无";$eff_get = 0;$itai_get = 0;
$in['Command'] = "MAIN";

SAVE();
}
#===================#
# ■ アイテムを拾う #
#===================#
function ITEMGETNEW(){
global $in;

global $item_get,$item,$log,$itname,$eff_get,$itai_get,$eff,$itai,$sub;
if($item_get == "无"){ERROR("不正当登录。","NO NEW TITEM","ITEM1",__FUNCTION__,__LINE__);}
$chkflg = -1;
for ($i=0;$i<5;$i++){if($item[$i] == "无"){$chkflg = $i;break;}}#空きアイテム？
if($chkflg == -1){$log .= '不过，包里再也装不下了<BR>'.$itname.'拿起来看看还是不要了…。<BR>';}#所持品オーバー
else{
	if($item[$chkflg] == "无"){$item[$chkflg] = $item_get;$eff[$chkflg] = $eff_get;$itai[$chkflg] = $itai_get;}
	list($itname,$kind) = explode("<>",$item_get);
	$log .= $itname.'被捡了起来。'.$sub.'<BR>';
}

$item_get = "无";$eff_get = 0;$itai_get = 0;
$in['Command'] = "MAIN";

SAVE();

}
#=================#
# ■ アイテム投棄 #
#=================#
function ITEMDEL(){
global $in,$pref;

global $item,$log,$pls,$eff,$itai;

$wk = $in['Command'];
//$wk =~ s/DEL_//g;
$wk = str_replace('DEL_','',$wk);

if($item[$wk] == "无"){ERROR("不正当登录。","","ITEM1",__FUNCTION__,__LINE__);}

list($itn, $ik) = explode('<>',$item[$wk]);

$log .= $itn.'被捡了起来。<br>';

$filename = $pref['LOG_DIR'].'/'.$pls.$pref['item_file'];
$itemlist = @file($filename);
array_push($itemlist,"$item[$wk],$eff[$wk],$itai[$wk],\n");
$handle = @fopen($filename,'w') or ERROR("unable to open filename","","ITEM1",__FUNCTION__,__LINE__);
if(!@fwrite($handle,implode('',$itemlist))){ERROR("unable to write filename","","ITEM1",__FUNCTION__,__LINE__);}
fclose($handle);

$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;
$in['Command'] = "MAIN";

SAVE();

}
#=================#
# ■ アイテム使用 #
#=================#
function ITEM(){
global $in,$pref;

global $item,$wep,$bou,$log,$eff,$itai,$inf,$wk,$pls;
$result = 0;$wep2 = '';$watt2 = 0;$wtai2 = 0;$up = 0;

$wk = $in['Command'];
$wk = str_replace ('ITEM_','',$wk);

if($item[$wk] == "无"){ERROR("不正当登录。","","ITEM1",__FUNCTION__,__LINE__);}

list($itn,$ik) = explode("<>",$item[$wk]);
list($w_name,$w_kind) = explode("<>",$wep);
list($d_name,$d_kind) = explode("<>",$bou);

if(preg_match("/<>SH/",$item[$wk])){#スタミナ回复
	global $sta;
	if(($pref['maxsta'] - $sta) > 0){
		$old_sta = $sta;
		$sta += $eff[$wk];
		if($sta > $pref['maxsta']){$sta = $pref['maxsta'];}
		$work = $sta - $old_sta;
		$log .= $itn.'食用后。<BR>体力竟然回复了 '.$work.'，我还能战五百年！<BR>';
		$itai[$wk] --;
		if($itai[$wk] == 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
	}else{
		$log .= $itn.'就算使用也没有效果<BR>';
	}
}elseif(preg_match("/<>HH/",$item[$wk])){#体力回复
	global $hit,$mhit;
	if(($mhit - $hit) > 0){
		$old_hit = $hit;
		$hit += $eff[$wk];
		if($hit > $mhit){$hit = $mhit;}
		$work = $hit - $old_hit;
		$log .= $itn.'食用后。<BR>生命竟然回复了 '.$work.'，阿部我来了！<BR>';
		$itai[$wk] --;
		if($itai[$wk] == 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
	}else{
		$log .= $itn.'就算使用也毫无用处<BR>';
	}
}elseif($itn == "毒药"){#解毒
	$in['Command'] = "SPECIAL";
	$in['Command4'] = "POISON";
	return;
}elseif($itn == "解毒剂"){#解毒
	if(preg_match("/毒/",$inf)){$log .= $itn.'服用后。<BR>毒被解除了<BR>';
		$inf = str_replace ('毒','',$inf);
		$itai[$wk] --;
		if($itai[$wk] == 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
	}else{
		$log .= $itn.'就算使用也毫无用处<BR>';
	}
}elseif(preg_match("/<>SD|<>HD/",$item[$wk])){	#毒入り
	global $hit,$sinri,$poisoni,$poisondeadchk;
	if	 (preg_match("/<>SD2|<>HD2/",$item[$wk])){$result = round($eff[$wk]*2);}#料理研究部特製？
	elseif(preg_match("/<>SD1|<>HD1/",$item[$wk])){$result = round($eff[$wk]*1.5);}#能力者
	else{$result = $eff[$wk];}
	$inf = str_replace ('毒','',$inf);
	$inf = ($inf . "毒");
	$hit -= $result;
	$log .= '呃…不好！<BR>好像混进了毒药！<BR><font color="red"><b>受到'.$result.'伤害</b></font>！<BR>';
	LOGSAVE("POISON");
	$itai[$wk]--;
	if(preg_match('/-/',$item[$wk])){
		list($tp,$poisonid) = explode('-', $item[$wk]);
		$sinri = $poisonid;//暫定
	}
	$poisoni = $item[$wk];
	if($itai[$wk] == 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
	$poisondeadchk = 1;
	if($hit <= 0){require $pref['LIB_DIR']."/lib3.php";POISONDEAD();}
}elseif((preg_match("/<>W/",$item[$wk]))&&(!preg_match("/<>WF/",$item[$wk]))){ #武器装備
	global $watt,$wtai;
	$log .= $itn.'已经装备。<BR>';
	$wep2 = $wep;$watt2 = $watt;$wtai2 = $wtai;
	$wep = $item[$wk];$watt = $eff[$wk];$wtai = $itai[$wk];
	if(!preg_match("/空手/",$wep2)){$item[$wk] = $wep2;$eff[$wk] = $watt2;$itai[$wk] = $wtai2;}
	else{$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(preg_match("/<>DB/",$item[$wk])){#防具装備（体）
	global $bou,$bdef,$btai;
	$log .= $itn.'已经穿在身上。<BR>';
	$bou2 = $bou;$bdef2 = $bdef;$btai2 = $btai;
	$bou = $item[$wk];$bdef = $eff[$wk];$btai = $itai[$wk];
	if(!preg_match("/内衣/",$bou2)){$item[$wk] = $bou2;$eff[$wk] = $bdef2;$itai[$wk] = $btai2;}
	else{$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(preg_match("/<>DH/",$item[$wk])){#防具装備（头）
	global $bou_h,$bdef_h,$btai_h;
	$log .= $itn.'已经戴在头上。<BR>';
	$bou2 = $bou_h;$bdef2 = $bdef_h;$btai2 = $btai_h;
	$bou_h = $item[$wk];$bdef_h = $eff[$wk];$btai_h = $itai[$wk];
	if(!preg_match("/无/",$bou2)){$item[$wk] = $bou2;$eff[$wk] = $bdef2;$itai[$wk] = $btai2;}
	else{$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(preg_match("/<>DF/",$item[$wk])){#防具装備（足）
	global $bou_f,$bdef_f,$btai_f;
	$log .= $itn.'已经穿在脚上。<BR>';
	$bou2 = $bou_f;$bdef2 = $bdef_f;$btai2 = $btai_f;
	$bou_f = $item[$wk];$bdef_f = $eff[$wk];$btai_f = $itai[$wk];
	if(!preg_match("/无/",$bou2)){$item[$wk] = $bou2;$eff[$wk] = $bdef2;$itai[$wk] = $btai2;}
	else{$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(preg_match("/<>DA/",$item[$wk])){#防具装備（腕）
	global $bou_a,$bdef_a,$btai_a;
	$log .= $itn.'已经佩在腕上。<BR>';
	$bou2 = $bou_a;$bdef2 = $bdef_a;$btai2 = $btai_a;
	$bou_a = $item[$wk];$bdef_a = $eff[$wk];$btai_a = $itai[$wk];
	if(!preg_match("/无/",$bou2)){$item[$wk] = $bou2;$eff[$wk] = $bdef2;$itai[$wk] = $btai2;}
	else{$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(preg_match("/<>A/",$item[$wk])){ #アクセサリ装備
	$log .= $itn.'已经佩在身上。<BR>';
	$bou2 = $item[5];$bdef2 = $eff[5];$btai2 = $itai[5];
	$item[5] = $item[$wk];$eff[5] = $eff[$wk];$itai[5] = $itai[$wk];
	if(!preg_match("/无/",$bou2)){$item[$wk] = $bou2;$eff[$wk] = $bdef2;$itai[$wk] = $btai2;}
	else{$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(preg_match("/<>R/",$item[$wk])){ #レーダー
	HEAD();require $pref['LIB_DIR'].'/dsp.php';READER();FOOTER();exit();
}elseif(preg_match("/<>TN/",$item[$wk])){#罠
	global $pls;
	$log .= $itn.'已经被设计成完美的陷阱。<BR>小心自己反被阴到…。<BR>';
	$filename = $pref['LOG_DIR'].'/'.$pls.$pref['item_file'];
	$itemlist = @file($filename);
	array_push($itemlist,str_replace('TN','TO',$item[$wk]).','.$eff[$wk].','.$itai[$wk].",\n");
	$handle = @fopen($filename,'w') or ERROR('unable to open filename','','ITEM1',__FUNCTION__,__LINE__);
	if(!@fwrite($handle,implode('',$itemlist))){ERROR('unable to write filename','','ITEM1',__FUNCTION__,__LINE__);}
	fclose($handle);
	$itai[$wk]--;
	if($itai[$wk] <= 0){$item[$wk] = '无';$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(($itn == "磨刀石")&&(preg_match("/<>WK/",$wep))){#磨刀石使用＆ナイフ系装備？
	global $watt;
	$watt += (rand(0,2) + $eff[$wk]);
	$log .= $itn.'经过打磨。<BR>'.$w_name.'的攻击力变为 '.$watt.' 。<BR>';
	$itai[$wk] --;
	if($itai[$wk] <= 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(($itn == "钉")&&(preg_match("/棒球棍<>WB/",$wep))){#棒球棍使用時
	global $watt;
	$watt += (rand(0,2) + $eff[$wk]);
	$wep = "狼牙甲子园之梦<>WB";
	$log .= $itn.'安装了上去。<BR>'.$w_name.'的攻击力变为 '.$watt.' 。<BR>';
	$itai[$wk] --;
	if($itai[$wk] <= 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(($itn == "钉")&&(preg_match("/木棍<>WB/",$wep))){#木棍使用時
	global $watt;
	$watt += (rand(0,2) + $eff[$wk]);
	$wep = "狼牙打狗棒<>WB";
	$log .= $itn.'安装了上去。<BR>'.$w_name.'的攻击力变为 '.$watt.' 。<BR>';
	$itai[$wk] --;
	if($itai[$wk] <= 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(($itn == "钉")&&(preg_match("/铁棍<>WB/",$wep))){#铁棍使用時
	global $watt;
	$watt += (rand(0,2) + $eff[$wk]);
	$wep = "狼牙撬棒<>WB";
	$log .= $itn.'安装了上去。<BR>'.$w_name.'的攻击力变为 '.$watt.' 。<BR>';
	$itai[$wk] --;
	if($itai[$wk] <= 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(($itn == "钉")&&(preg_match("/水管<>WB/",$wep))){#水管使用時
	global $watt;
	$watt += (rand(0,2) + $eff[$wk]);
	$wep = "狼牙大钉管<>WB";
	$log .= $itn.'安装了上去。<BR>'.$w_name.'的攻击力变为 '.$watt.' 。<BR>';
	$itai[$wk] --;
	if($itai[$wk] <= 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(($itn == "裁缝道具")&&($d_kind == "DB")&&($d_name != "内衣")){#裁缝道具＆服系装備？
	global $btai;
	$btai += (rand(0,2) + $eff[$wk]);
	$log .= $itn.'经过你的妙手。<BR>'.$d_name.'的耐久度增加到 '.$btai.' 。<BR>';
	$itai[$wk] --;
	if($itai[$wk] <= 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif((preg_match("/子弹/",$itn))&&(preg_match("/<>WG|WGB/",$wep))){ #子弹使用＆射系装備？
	global $wtai;

	$up = $eff[$wk] + $wtai;if($up > 6){$up = 6 - $wtai;}else{$up = $eff[$wk];}
	$wtai += $up;$eff[$wk] -= $up;

	if($eff[$wk] <= 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
	if(preg_match("/<>WGB/",$wep)){$wep = str_replace ('<>WGB','<>WG',$wep);}
	if($up == 0){
		$log .= '再也不能把'.$itn.'向'.$w_name.' 装填了。<BR>';
	}else{
		$log .= $itn.'已经向'.$w_name.' 装填。<BR>'.$w_name.'的使用次数上升 '.$up.' 次。<BR>';
	}
}elseif(($itn == "消声器")&&(preg_match("/<>WG/",$wep))&&(!preg_match("/S/",$wep))){	#消声器
	$wep = ($w_name . "<>" . $w_kind . "S");
	$log .= $itn.'已经装上。<BR>'.$w_name.'附有了消声器。<BR>';
	$itai[$wk] --;
	if($itai[$wk] <= 0){$item[$wk] = '无';$eff[$wk] = 0;$itai[$wk] = 0;}
}elseif(preg_match("/电池/",$itn)){
	$pc_ck = 0;
	for ($paso=0;$paso<5;$paso++){
		if(($item[$paso] == "移动PC<>Y")&&($itai[$paso] < 5)){
			$itai[$paso] += $eff[$wk];
			if($itai[$paso] > 5){$itai[$paso] = 5;}
			$itai[$wk] --;
			if($itai[$wk] <= 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
			$log .= $itn.' 向移动PC插入。<BR>移动PC的使用次数变为 '.$itai[$paso].'。<BR>';
			$pc_ck = 1;
			break;
		}
	}
	if($pc_ck == 0){$log .= '这个该用在什么地方呢…。<BR>';$in['Command']="MAIN";}
}elseif($itn == "移动PC"){
	$in['Command'] = "SPECIAL";
	$in['Command4'] = "HACK";
	require $pref['LIB_DIR']."/lib3.php";
	HACKING();
}elseif($itn == "滋味酱"){
	if($pls == 0){
		$inf = ($inf . "解");
		$handle = @fopen($pref['end_flag_file'],'w') or ERROR("unable to open end_flag_file","","ITEM1",__FUNCTION__,__LINE__);
		if(!@fwrite($handle,"解除结束\n")){ERROR("unable to write end_flag_file","","ITEM1",__FUNCTION__,__LINE__);}
		fclose($handle);
		LOGSAVE("EX_END");
		$log .= '使用滋味酱让绿坝娘解开了最后的心结！<br>程序解除了！<BR>';$in['Command']="MAIN";
		SAVE();
	}else{
		$log .= '这里使用没有什么意义…。<BR>';$in['Command']="MAIN";
	}
}elseif($itn == "御神签"){#御神签使用
	$log .= '御神签么…打开看看。<BR>';
	$omi = rand(0,100);
	if	 ($omi < 20){list($lvuphit,$lvupatt,$lvupdef,$sign) = omikuji(3,1);$log .= '大吉！好像能碰上好事情！<BR>';}
	elseif($omi < 40){list($lvuphit,$lvupatt,$lvupdef,$sign) = omikuji(2,1);$log .= '中吉么。一般般啦！<BR>';}
	elseif($omi < 60){list($lvuphit,$lvupatt,$lvupdef,$sign) = omikuji(1,1);$log .= '小吉么。可有可无呢…<BR>';}
	elseif($omi < 80){list($lvuphit,$lvupatt,$lvupdef,$sign) = omikuji(1,0);$log .= '凶么…。真是晦气呢…<BR>';}
	else			{list($lvuphit,$lvupatt,$lvupdef,$sign) = omikuji(2,0);$log .= '大凶？？难道滋味要找我殉情！？…<BR>';}
	$log .= '<font color="00FFFF">【体】'.($sign ? '+' :'-').$lvuphit.' 【攻】'.($sign ? '+' :'-').$lvupatt.' 【防】'.($sign ? '+' :'-').$lvupdef.'</font><BR>';

	$itai[$wk] --;
	if($itai[$wk] <= 0){$item[$wk] = "无";$eff[$wk] = 0;$itai[$wk] = 0;}
}else{
	$log .= '这个该怎么用呢…。<BR>';
	$in['Command']="MAIN";
}

$in['Command'] = "MAIN";

SAVE();

}
#===================#
# ■ 御神签ランダム #
#===================#
function omikuji($omi,$sign){
	global $hit,$mhit,$att,$att,$def;
	$lvuphit = rand(0,$omi);$lvupatt = rand(0,$omi);$lvupdef = rand(0,$omi);
	if($sign){
		$hit += $lvuphit;$mhit += $lvuphit;$att += $lvupatt;$def += $lvupdef;
	}else{
		$hit -= $lvuphit;$mhit -= $lvuphit;$att -= $lvupatt;$def -= $lvupdef;
	}
	return array($lvuphit,$lvupatt,$lvupdef,$sign);
}
#=======================#
# ■ 装備武器を外す処理 #
#=======================#
function WEPDEL(){
global $in;

global $wep,$log,$l_name,$item,$eff,$watt,$itai,$wtai;

$j = 0;
if(preg_match("/空手/",$wep)){$log .= $l_name.'没有装备武器。<br>';$in['Command'] = "MAIN";return;}
list($w_name,$w_kind) = explode("<>",$wep);
#所持品空きスペース確認
$chk = "NG";
for ($j=0;$j<5;$j++){if($item[$j] == "无"){$chk = "ON";break;}}
if($chk == "NG"){$log .= '背包里再也放不下了。<br>';
}else{
	$log .= $w_name.'被我放入包包。<br>';
	$item[$j] = $wep;$eff[$j] = $watt;$itai[$j] = $wtai;
	$wep = "空手<>WP";$watt = 0;$wtai = "∞";
	SAVE();
}
$in['Command'] = "MAIN";
}
#=================#
# ■ 装備武器投棄 #
#=================#
function WEPDEL2(){
global $in,$pref;

global $wep,$log,$l_name,$pls,$watt,$wtai;

if(preg_match("/空手/",$wep)){$log .= $l_name.'没有装备武器。<br>';$in['Command'] = "MAIN";return;}
list($itn, $ik) = explode("<>",$wep);
$log = ($log . "{$itn}被我扔了。<br>");
#武器投棄
$filename = $pref['LOG_DIR'].'/'.$pls.$pref['item_file'];
$itemlist = @file($filename);
array_push($itemlist,"$wep,$watt,$wtai,\n");
$handle = @fopen($filename,'w') or ERROR("unable to open filename","","ITEM1",__FUNCTION__,__LINE__);
if(!@fwrite($handle,implode('',$itemlist))){ERROR("unable to write filename","","ITEM1",__FUNCTION__,__LINE__);}
fclose($handle);
$wep = "空手<>WP";$watt = 0;$wtai = "∞";
$in['Command'] = "MAIN";
SAVE();
}
#=====================#
# ■ 头防具を外す処理 #
#=====================#
function BOUDELH(){
global $in;

global $bou_h,$log,$l_name,$item,$eff,$itai,$bdef_h,$btai_h;

$j = 0;
if(preg_match("/无/",$bou_h)){$log .= $l_name.'没有装备头部防具。<br>';$in['Command'] = "MAIN";return;}
list($w_name,$w_kind) = explode("<>",$bou_h);
#所持品空きスペース確認
$chk = "NG";
for ($j=0;$j<5;$j++){if($item[$j] == "无"){$chk = "ON";break;}}
if($chk == "NG"){$log .= '背包里再也放不下了。<br>';
}else{
	$log .= $w_name.'被我放入包包。<br>';
	$item[$j] = $bou_h;$eff[$j] = $bdef_h;$itai[$j] = $btai_h;
	$bou_h = "无";$bdef_h = $btai_h = 0;
	SAVE();
}
$in['Command'] = "MAIN";
}
#=====================#
# ■ 体防具を外す処理 #
#=====================#
function BOUDELB(){
global $in;

global $bou,$log,$l_name,$item,$eff,$itai,$bdef,$btai;
$j = 0;
if(preg_match("/内衣/",$bou)){$log .= $l_name.'没有装备身体防具。<br>';$in['Command'] = "MAIN";return;}
list($w_name,$w_kind) = explode("<>",$bou);
#所持品空きスペース確認
$chk = "NG";
for ($j=0;$j<5;$j++){if($item[$j] == "无"){$chk = "ON";break;}}
if($chk == "NG"){$log .= '背包里再也放不下了。<br>';
}else{
	$log .= $w_name.'被我放入包包。<br>';
	$item[$j] = $bou;$eff[$j] = $bdef;$itai[$j] = $btai;
	$bou = "内衣<>DN";$bdef = 0;$btai = 0;
	SAVE();
}
$in['Command'] = "MAIN";
}
#=====================#
# ■ 腕防具を外す処理 #
#=====================#
function BOUDELA(){
global $in;

global $bou_a,$log,$l_name,$item,$eff,$itai,$bdef_a,$btai_a;
$j = 0;
if(preg_match("/无/",$bou_a)){$log .= $l_name.'没有装备腕部防具。<br>';$in['Command'] = "MAIN";return;}
list($w_name,$w_kind) = explode("<>",$bou_a);
#所持品空きスペース確認
$chk = "NG";
for ($j=0;$j<5;$j++){if($item[$j] == "无"){$chk = "ON";break;}}
if($chk == "NG"){$log .= '背包里再也放不下了。<br>';
}else{
	$log .= $w_name.'被我放入包包。<br>';
	$item[$j] = $bou_a;$eff[$j] = $bdef_a;$itai[$j] = $btai_a;
	$bou_a = "无";$bdef_a = 0;$btai_a = 0;
	SAVE();
}
$in['Command'] = "MAIN";
}
#=====================#
# ■ 足防具を外す処理 #
#=====================#
function BOUDELF(){
global $in;

global $bou_f,$log,$l_name,$item,$eff,$itai,$bdef_f,$btai_f;
$j = 0;
if(preg_match("/无/",$bou_f)){$log .= $l_name.'没有装备脚部防具。<br>';$in['Command'] = "MAIN";return;}
list($w_name,$w_kind) = explode("<>",$bou_f);
#所持品空きスペース確認
$chk = "NG";
for ($j=0;$j<5;$j++){if($item[$j] == "无"){$chk = "ON";break;}}
if($chk == "NG"){$log .= '背包里再也放不下了。<br>';
}else{
	$log .= $w_name.'被我放入包包。<br>';
	$item[$j] = $bou_f;$eff[$j] = $bdef_f;$itai[$j] = $btai_f;
	$bou_f = "无";$bdef_f = 0;$btai_f = "∞";
	SAVE();
}
$in['Command'] = "MAIN";
}
#=====================#
# ■ 装饰品を外す処理 #
#=====================#
function BOUDEL(){
global $in;

global $log,$l_name,$item,$eff,$itai;
$j = 0;
if(preg_match("/无/",$item[5])){$log .= $l_name.'没有装备装饰品。<br>';$in['Command'] = "MAIN";return;}
list($w_name,$w_kind) = explode("<>",$item[5]);
#所持品空きスペース確認
$chk = 'NG';
for ($j=0;$j<5;$j++){if($item[$j] == "无"){$chk = "ON";break;}}
if($chk == "NG"){$log .= '背包里再也放不下了。<br>';
}else{
	$log .= $w_name.'被我放入包包。<br>';
	$item[$j] = $item[5];$eff[$j] = $eff[5];$itai[$j] = $itai[5];
	$item[5] = "无";$eff[5] = 0;$itai[5] = "0";
	SAVE();
}
$in['Command'] = 'MAIN';
}