<?php
#□■□■□■□■□■□■□■□■□■□■□■□
#■ 	-       BR ATTACK PROGRAM      - 	 ■
#□ 									 	 □
#■ 			サブルーチン一覧		 	 ■
#□ 									 	 □
#■ WEPTREAT	-	武器種別処理		 	 ■
#□ DEATH		-	自分死亡処理		 	 □
#■ DEATH2		-	敵死亡処理			 	 ■
#□ RUNAWAY		-	逃亡処理			 	 □
#■ DEFTREAT	-	防具種別処理		 	 ■
#□ LVUPCHK		-	レベルアップ処理	 	 □
#■ lvuprand	-	レベルアップランダム処理 ■
#□ EN_KAIFUKU	-	敵回復処理			 	 □
#■ BLOG_CK		-	敵戦闘ログ自動削除処理	 ■
#□ RUN_AWAY	-	自動移動				 □
#■□■□■□■□■□■□■□■□■□■□■□■
#=================#
# ■ 武器種別処理 #
#=================#
#					武器	武器	残数	攻撃者名	防御者名	攻撃種別(攻撃/反撃)	攻撃者（PC/NPC)
function WEPTREAT(	$wname,	$wkind,	$wwtai,	$pn,		$nn,		$ind,				$attman){
global $pref;
global $log,$wk;

if($attman == 'PC'){$nn = '对方';}
elseif($attman == 'NPC'){$pn = '对方';}

$dice3 = rand(0,100);	#怪我1
$dice4 = rand(0,4);	#怪我2
$dice5 = rand(0,100);	#破壊？

$kega = $hakai = 0;
$kegainf = $k_work = '';

if(((preg_match('/B/',$wkind))||((preg_match('/G|A/',$wkind))&&($wwtai == 0)))&&($wname != '空手')){#棍棒,弾無し銃,矢無し弓
	$log .= $pn.' 的'.$ind.'！<BR>　抄起 '.$wname.' 向 '.$nn.' 发出攻击！<BR>';
	if($attman == 'PC'){
		global $wp;
		$wp++;$wk=$wp;
	}else{
		global $w_wp;
		$w_wp++;$wk=$w_wp;
	}
	$kega = 15;$kegainf = "头腕";		#怪我率、怪我個所
	$hakai = 2;	#破壊率
}elseif(preg_match("/C/",$wkind)){		#投系
	$log .= $pn.' 的'.$ind.'！<BR>　抡起 '.$wname.' 投向 '.$nn.' 的方向！<BR>';
	if($attman == 'PC'){
		global $wc;
		$wc++;$wk=$wc;
	}else{
		global $w_wc;
		$w_wc++;$wk=$w_wc;
	}
	$kega = 15;$kegainf = "头腕";		#怪我率、怪我個所`
	$hakai = 0;	#破壊率
}elseif(preg_match("/D/",$wkind)){		#爆系
	$log .= $pn.' 的 '.$ind.'！<BR>　拉开 '.$wname.' 的保险向 '.$nn.' 大力扔去！<BR>';
	if($attman == 'PC'){
		global $wd;
		$wd++;$wk=$wd;
	}else{
		global $w_wd;
		$w_wd++;$wk=$w_wd;
	}
	$kega = 15;$kegainf = "头腕腕足";	#怪我率、怪我個所
	$hakai = 0;	#破壊率
}elseif(preg_match("/G/",$wkind)){		#銃系
	$log .= $pn.' 的 '.$ind.'！<BR>　举起 '.$wname.' 向 '.$nn.' 一阵乱射！<BR>';
	if($attman == 'PC'){
		global $wg,$pls;
		$wg++;$wk=$wg;$ps=$pls;
	}else{
		global $w_wg,$w_pls;
		$w_wg++;$wk=$w_wg;$ps=$w_pls;
	}
	$kega = 25;$kegainf = "头腕腹足";	#怪我率、怪我個所
	$hakai = 1;	#破壊率
	if(!preg_match("/S/",$wkind)){#発砲音 書き込み
		global $id,$w_id;

		$gunlog = @file($pref['gun_log_file']) or ERROR('无法打开 gun_log_file','','ATTACK',__FUNCTION__,__LINE__);
		$gunlog[0] = $pref['now'].','.$pref['place'][$ps].','.$id.','.$w_id.",\n";

		$handle = @fopen($pref['gun_log_file'],'w') or ERROR('无法打开 gun_log_file','','ATTACK',__FUNCTION__,__LINE__);
		if(!@fwrite($handle,implode('',$gunlog))){ERROR('无法打开 gun_log_file','','ATTACK',__FUNCTION__,__LINE__);}
		fclose($handle);
	}
}elseif(preg_match('/K/',$wkind)){		#斬系
	$log .= $pn.' 的'.$ind.'！<BR>　提起 '.$wname.' 向 '.$nn.'挥去！<BR>';
	if($attman == 'PC'){
		global $wn;
		$wn++;$wk=$wn;
	}else{
		global $w_wn;
		$w_wn++;$wk=$w_wn;
	}
	$kega = 25;$kegainf = "头腕腹足";	#怪我率、怪我個所
	$hakai = 1;	#破壊率
}elseif(preg_match("/P/",$wkind)){		#殴系
	$log .= $pn.' 的'.$ind.'！<BR>　舞起 '.$wname.' 向 '.$nn.' 打去！<BR>';
	if($attman == 'PC'){
		global $wp;
		$wp++;$wk=$wp;
	}else{
		global $w_wp;
		$w_wp++;$wk=$w_wp;
	}
	$kega = 0;$kegainf = '';			#怪我率、怪我個所
	$hakai = 0;	#破壊率
}else{						#その他
	$log .= $pn.' 的'.$ind.'！<BR>　'.$wname.' 击中了 '.$nn.'！<BR>';
	if($attman == 'PC'){
		global $wp;
		$wp++;$wk=$wp;
	}else{
		global $w_wp;
		$w_wp++;$wk=$w_wp;
	}
	$kega = 0;$kegainf = '';			#怪我率、怪我個所
	$hakai = 0;	#破壊率
}

$wk = (int)($wk/$pref['BASE']);$wk = ($wk * 0.02);$wk = (0.89 + $wk);#熟練度による攻撃の変化率
#相手？自分？
if($attman == 'PC'){
	global $wep,$watt,$wtai,$w_inf,$wep_2,$watt_2,$wtai_2,$w_inf_2;
	$wep_2 = $wep;$watt_2 = $watt;$wtai_2 = $wtai;$w_inf_2 = $w_inf;
}else{
	global $w_wep,$w_watt,$w_wtai,$inf,$w_wep_2,$w_watt_2,$w_wtai_2,$inf_2;
	$w_wep_2 = $w_wep;$w_watt_2 = $w_watt;$w_wtai_2 = $w_wtai;$inf_2 = $inf;
}

# 武器破壊
global $hakaiinf2,$hakaiinf3;
if($dice5 < $hakai){#破壊？
	if($attman == 'PC'){
		$wep_2 = '空手<>WP';$watt_2 = 0;$wtai_2 = '∞';$hakaiinf3 = '武器损坏！';#PC
	}else{
		$w_wep_2 = '空手<>WP';$w_watt_2 = 0;$w_wtai_2 = '∞';$hakaiinf2 = '武器损坏！';
	}
}else{$hakaiinf2 = $hakaiinf3 = '';}

# 怪我処理
if($dice3 < $kega){
	if	  (($dice4 == 0)&&(preg_match('/头/',$kegainf))){$k_work = '头';}#頭
	elseif(($dice4 == 1)&&(preg_match('/腕/',$kegainf))){$k_work = '腕';}#腕
	elseif(($dice4 == 2)&&(preg_match('/腹/',$kegainf))){$k_work = '腹';}#腹
	elseif(($dice4 == 3)&&(preg_match('/足/',$kegainf))){$k_work = '足';}#足
	else{return;}

	if($attman == 'PC'){#PC
		global $w_item,$w_eff,$w_itai,$w_bou,$w_bou_h,$w_bou_f,$w_bou_a,$kega3,$w_btai,$w_btai_h,$w_btai_f,$w_btai_a,$w_bdef,$w_bdef_h,$w_bdef_f,$w_bdef_a,$w_inf_2;
		if(((preg_match('/AD/',$w_item[5]))||(preg_match('/<>DB/',$w_bou)))&&($k_work == '腹')){	#腹？
			if(preg_match('/AD/',$w_item[5])){$w_itai[5] --;if($w_itai[5] <= 0){$w_item[5]='无';$w_eff[5]=$w_itai[5]=0;}}
			else{$w_btai --;if($w_btai <= 0){$w_bou = '内衣<>DN';$w_bdef=0;$w_btai='∞';}}
			return;
		}
		elseif((preg_match('/<>DH/',$w_bou_h))&&($k_work == '头')){$w_btai_h --;if($w_btai_h <= 0){$w_bou_h='无';$w_bdef_h=$w_btai_h=0;}return;}#頭？
		elseif((preg_match('/<>DF/',$w_bou_f))&&($k_work == '足')){$w_btai_f --;if($w_btai_f <= 0){$w_bou_f='无';$w_bdef_f=$w_btai_f=0;}return;}#足？
		elseif((preg_match('/<>DA/',$w_bou_a))&&($k_work == '腕')){$w_btai_a --;if($w_btai_a <= 0){$w_bou_a='无';$w_bdef_a=$w_btai_a=0;}return;}#腕？
		else{$w_inf_2 = preg_replace("/$k_work/",'',$w_inf_2);$w_inf_2 = ($w_inf_2 . $k_work);$kega3 = ($k_work . "部负伤");}
	}else{
		global $item,$eff,$bou,$itai,$bou_h,$bou_f,$bou_a,$kega2,$btai,$btai_h,$btai_f,$btai_a,$bdef,$bdef_h,$bdef_f,$bdef_a,$inf_2;
		if(((preg_match('/AD/',$item[5]))||(preg_match('/<>DB/',$bou)))&&($k_work == '腹')){	#腹？
			if(preg_match('/AD/',$item[5])){$itai[5] --;if($itai[5] <= 0){$item[5]='无';$eff[5]=$itai[5]=0;}}
			else{$btai --;if($btai <= 0){$bou = '内衣<>DN';$bdef=0;$btai='∞';}}
			return;
		}
		elseif((preg_match('/<>DH/',$bou_h))&&($k_work == '头')){$btai_h --;if($btai_h <= 0){$bou_h='无';$bdef_h=$btai_h=0;}return;}#頭？
		elseif((preg_match('/<>DF/',$bou_f))&&($k_work == '足')){$btai_f --;if($btai_f <= 0){$bou_f='无';$bdef_f=$btai_f=0;}return;}#足？
		elseif((preg_match('/<>DA/',$bou_a))&&($k_work == '腕')){$btai_a --;if($btai_a <= 0){$bou_a='无';$bdef_a=$btai_a=0;}return;}#腕？
		else{$inf_2 = preg_replace("/$k_work/",'',$inf_2);$inf_2 = ($inf_2 . $k_work);$kega2 = $k_work.'部负伤';}
	}
}
}
#=================#
# ■ 自分死亡処理 #
#=================#
function DEATH(){
global $pref;
global $hit,$w_kill,$mem,$log,$f_name,$l_name,$cl,$sex,$no,$w_log,$w_f_name,$w_l_name,$w_msg,$hour,$min,$sec,$b_limit,$ar,$w_inf,$gunlog,$pls,$id,$w_id;

$hit = 0;$w_kill++;
$mem--;

$com = rand(0,6);

$log .= '<font color="red"><b>'.$f_name.' '.$l_name.'（'.$cl.' '.$sex.$no.'号）已经被菊爆。</b></font><br>';
if($w_msg != ''){$log .= '<font color="lime"><b>'.$w_f_name.' '.$w_l_name.'『'.$w_msg.'』</b></font><br>';}
$w_log .= '<font color="yellow"><b>'.$hour.':'.$min.':'.$sec.' '.$f_name.' '.$l_name.'（'.$cl.' '.$sex.$no.'号）醉生梦死，被菊爆了。<BR>　【剩余'.$mem.'人】</b></font><br>';

$b_limit = ($pref['battle_limit']) + 1;

if(($mem == 1)&&($w_sts != 'NPC')&&($ar > $b_limit)){$w_inf .= '胜';}

$gunlog = @file($pref['gun_log_file']) or ERROR('无法打开 gun_log_file','','ATTACK',__FUNCTION__,__LINE__);
$gunlog[1] = $pref['now'].','.$pref['place'][$pls].','.$id.','.$w_id.",\n";
$handle = @fopen($pref['gun_log_file'],'w') or ERROR('无法打开 gun_log_file','','ATTACK',__FUNCTION__,__LINE__);
if(!@fwrite($handle,implode('',$gunlog))){ERROR('无法打开 gun_log_file','','ATTACK',__FUNCTION__,__LINE__);}
fclose($handle);

#死亡ログ
LOGSAVE('DEATH2');
$death = $deth;
}
#===============#
# ■ 敵死亡処理 #
#===============#
function DEATH2(){
global $in,$pref;

global $w_hit,$kill,$w_id,$bb,$w_cl,$mem,$w_com,$log,$w_f_name,$w_l_name,$w_cl,$w_sex,$w_no,$w_dmes,$msg,$w_death;
global $f_name,$l_name,$msg,$pls,$id,$w_id,$deth;

$w_hit = 0;$kill++;
$bb = $w_id;#ブラウザバック対処
if(($w_cl != $pref['BOSS'])||($w_cl != $pref['KANN'])||($w_cl != $pref['ZAKO'])){$mem--;}

$w_com = rand(0,6);
$log .= '<font color="red"><b>'.$w_f_name.' '.$w_l_name.'（'.$w_cl.' '.$w_sex.$w_no.'号）被菊爆了。<BR>　【剩余'.$mem.'人】</b></font><br>';

if(strlen($w_dmes) > 1){$log .= '<font color="yellow"><b>'.$w_f_name.' '.$w_l_name.'『'.$w_dmes.'』</b></font><br>';}
if(strlen($msg) > 1){$log .= '<font color="lime"><b>'.$f_name.' '.$l_name.'『'.$msg.'』</b></font><br>';}

$b_limit = ($pref['battle_limit']) + 1;
if(($mem == 1)&&($ar > $b_limit)){$inf .= '胜';}

$gunlog = @file($pref['gun_log_file']) or ERROR('无法打开 gun_log_file','','ATTACK',__FUNCTION__,__LINE__);
$gunlog[1] = $pref['now'].','.$pref['place'][$pls].','.$id.','.$w_id.",\n";
$handle = @fopen($pref['gun_log_file'],'w') or ERROR('无法打开 gun_log_file','','ATTACK',__FUNCTION__,__LINE__);
if(!@fwrite($handle,implode('',$gunlog))){ERROR('无法打开 gun_log_file','','ATTACK',__FUNCTION__,__LINE__);}
fclose($handle);

#死亡ログ
LOGSAVE('DEATH3');

$in['Command'] = 'BATTLE2';
$w_death = $deth;
#$w_bid = '';

}
#=============#
# ■ 逃亡処理 #
#=============#
function RUNAWAY(){
global $in;

global $log,$l_name;
$log .= $l_name.' 捂着菊门全速逃开…。<BR>';
$in['Command'] = 'MAIN';
}
#=================#
# ■ 防具種別処理 #
#=================#
#					武器種別	防御側(PC/NPC)
function DEFTREAT(	$wkind,		$defman){
global $pnt;

$p_up = 1.1;
$p_down = 0.9;

if($defman == 'PC'){#PC?
	global $bou,$bou_h,$bou_f,$bou_a,$item;
	@list($b_name,$b_kind)     = explode('<>',$bou);
	@list($b_name_h,$b_kind_h) = explode('<>',$bou_h);
	@list($b_name_f,$b_kind_f) = explode('<>',$bou_f);
	@list($b_name_a,$b_kind_a) = explode('<>',$bou_a);
	@list($b_name_i,$b_kind_i) = explode('<>',$item[5]);
}else{
	global $w_bou,$w_bou_h,$w_bou_f,$w_bou_a,$w_item;
	@list($b_name,$b_kind)     = explode('<>',$w_bou);
	@list($b_name_h,$b_kind_h) = explode('<>',$w_bou_h);
	@list($b_name_f,$b_kind_f) = explode('<>',$w_bou_f);
	@list($b_name_a,$b_kind_a) = explode('<>',$w_bou_a);
	@list($b_name_i,$b_kind_i) = explode('<>',$w_item[5]);
}

if	 (($wkind == 'WG')&&($b_kind_i == 'ADB'))	{$pnt = $p_down;}	#銃→防弾
elseif(($wkind == 'WG')&&($b_kind_h == 'DH'))	{$pnt = $p_up;}		#銃→頭
elseif(($wkind == 'WK')&&($b_kind == 'DBK'))	{$pnt = $p_down;}	#斬→鎖
elseif(($wkind == 'WN')&&($b_kind_i == 'ADB'))	{$pnt = $p_up;}		#斬→防弾
elseif((($wkind == 'WB')||($wkind == 'WGB')||($wkind == 'WAB'))&&($b_kind_h == 'DH')){$pnt = $p_down;}#殴→頭
elseif((($wkind == 'WB')||($wkind == 'WGB')||($wkind == 'WAB'))&&(preg_match('/DBA/',$b_kind))){$pnt = $p_up;}	 #殴→鎧
else{$pnt = 1.0;}
}
#=====================#
# ■ レベルアップ処理 #
#=====================#
function LVUPCHK(){
global $pref;
global $exp,$level,$hit,$w_exp,$w_level,$w_hit;
if(($exp >= (int)($level*$pref['baseexp']+(($level-1)*$pref['baseexp'])))&&($hit > 0)){#レベルアップ
	global $log,$mhit,$att,$def,$level,$sta;
	list($lvuphit,$lvupatt,$lvupdef) = lvuprand();
	$log .= '你在阿部的试炼下升级了。<font color="#00FFFF">【体】+'.$lvuphit.' 【攻】+'.$lvupatt.' 【防】+'.$lvupdef.'</font><br>';
	$hit += $lvuphit;$mhit += $lvuphit;$att += $lvupatt;$def += $lvupdef;$level++;$sta += $pref['lvup_sta'];if($pref['maxsta'] < $sta){$sta = $pref['maxsta'];};
}
if(($w_exp >= (int)($w_level*$pref['baseexp']+(($w_level-1)*$pref['baseexp'])))&&($w_hit > 0)){#レベルアップ
	global $w_log,$w_mhit,$w_att,$w_def,$w_level,$w_sta;
	list($lvuphit,$lvupatt,$lvupdef) = lvuprand();
	$w_log .= '你在阿部的试炼下升级了。<font color="#00FFFF">【体】+'.$lvuphit.' 【攻】+'.$lvupdef.' 【防】+'.$lvupatt.'</font><br>';
	$w_hit += $lvuphit;$w_mhit += $lvuphit;$w_att += $lvupdef;$w_def += $lvupatt;$w_level++;$w_sta += $pref['lvup_sta'];if($pref['maxsta'] < $w_sta){$w_sta = $pref['maxsta'];};
}
}
#=============================#
# ■ レベルアップランダム処理 #
#=============================#
function lvuprand(){$lvuphit = rand(4,6);$lvupatt = rand(2,4);$lvupdef = rand(2,3);return array($lvuphit,$lvupatt,$lvupdef);}
#===============#
# ■ 敵回復処理 #
#===============#
function EN_KAIFUKU(){#敵回復処理
global $pref;

global $w_endtime,$w_inf,$w_ousen,$w_sts,$w_sta,$w_mhit,$w_hit;
$up = (int)(($pref['now'] - $w_endtime) / (1 * $pref['kaifuku_time']));
$w_endtime = $pref['now'];
if((preg_match('/腹/',$w_inf))&&($w_ousen != '专注治疗')){$up = (int)($up / 2);}
if((!preg_match('/腹/',$w_inf))&&($w_ousen == '专注治疗')){$up = (int)($up * 2);}
if($w_sts == '睡觉'){
	$w_sta += $up;
	if($w_sta > $pref['maxsta']){$w_sta = $pref['maxsta'];}
}elseif($w_sts == '治疗'){
	if($pref['kaifuku_rate'] == 0){$pref['kaifuku_rate'] = 1;}
	$up = (int)($up / $pref['kaifuku_rate']);
	$w_hit += $up;
	if($w_hit > $w_mhit){$w_hit = $w_mhit;}
}elseif($w_sts == '静养'){
	$w_sta += $up;
	if($w_sta > $pref['maxsta']){$w_sta = $pref['maxsta'];}
	if($pref['kaifuku_rate'] == 0){$pref['kaifuku_rate'] = 1;}
	$up = (int)($up / $pref['kaifuku_rate']);
	$w_hit += $up;
	if($w_hit > $w_mhit){$w_hit = $w_mhit;}
}
}
#===========================#
# ■ 敵戦闘ログ自動削除処理 #
#===========================#
function BLOG_CK(){
global $w_log,$w_sts,$hour,$min,$sec;
$log_len = strlen($w_log);
if(($w_sts == 'NPC')&&($log_len > 100)){$w_log = '<font color="yellow"><b>自动解除</b></font><br>';}
elseif($log_len > 1000){$w_log = '<font color="yellow"><b>'.$hour.':'.$min.':'.$sec.' 战斗限界已经自动解除了。</b></font><br>';}
}
#=============#
# ■ 自動移動 #
#=============#
function RUN_AWAY($pls,$i=1){
global $pref;
global $ar;
list($ara[0],$ara[1],$ara[2],$ara[3],$ara[4],$ara[5],$ara[6],$ara[7],$ara[8],$ara[9],$ara[10],$ara[11],$ara[12],$ara[13],$ara[14],$ara[15],$ara[16],$ara[17],$ara[18],$ara[19],$ara[20],$ara[21]) = explode(',',$pref['arealist'][4]);

$return = $ar + rand(0,(count($ara) - $ar - 1));

if(isset($ara[$return])){return $ara[$return];}//移動
elseif(($i)&&($ara[$return] == $pls)){RUN_AWAY($pls,0);}//もう一回検索
else{return $pls;}//現状維持

}
?>