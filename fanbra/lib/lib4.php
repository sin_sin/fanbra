<?php
#□■□■□■□■□■□■□■□■□■□■□
#■ 	-    BR LIBRARY PROGRAM    - 	 ■
#□ 									 □
#■ 		  サブルーチン一覧  		 ■
#□ 									 □
#■ MESS		-		メッセージ登録	 ■
#■ MSG_DEL		-		メッセージ削除	 ■
#□ LOGSAVE2	-			ログ保存	 □
#■ KICKOUT		-			不法侵入	 ■
#□ news		-			ニュース	 □
#■□■□■□■□■□■□■□■□■□■□■
#=======================#
# ■ メッセージ登録処理 #
#=======================#
function MESS(){
global $in,$pref;

global $log,$id,$f_name,$l_name;
$in['Command']='MAIN';
$in['mode']='main';
# 入力情報チェック
if($in['Mess']==''){$log .= '没有输入信息。<br>';return;}
# 伝言情報保存Command2
$messagelist = @file($pref['mes_file']);# or ERROR("unable to open mes_file");

array_unshift($messagelist,$pref['now'].','.$id.','.$f_name.','.$l_name.','.$in['M_Id'].','.$in['Mess'].",\n");
if(count($messagelist) >= $pref['listmax']){array_pop($messagelist);}
$handle = @fopen($pref['mes_file'],'w') or ERROR('unable to open mes_file','','LIB3',__FUNCTION__,__LINE__);
if(!@fwrite($handle,implode('',$messagelist))){ERROR('unable to write mes_file','','LIB3',__FUNCTION__,__LINE__);}
fclose($handle);
}
#=======================#
# ■ メッセージ削除処理 #
#=======================#
function MSG_DEL(){
global $in,$pref;

global $log,$id,$f_name,$l_name;
$in['Command']='MAIN';
$in['mode']='main';
# 入力情報チェック
if(!ctype_digit($in['Command2'])){$log .= '没有输入信息。<br>';return;}
# 伝言情報保存Command2
$messagelist = @file($pref['mes_file']);# or ERROR("unable to open mes_file");
foreach($messagelist as $key => $message){
	list($mes_time,$from_id,$w_f_name,$w_l_name,$to_id,$from_message) = explode(',',$message);
	if($from_id==$id && $mes_time==$in['Command2']){
		$messagelist[$key]=$mes_time.','.$from_id.','.$w_f_name.','.$w_l_name.',DEL,'.$from_message.",\n";
		break;
	}
}

$handle = @fopen($pref['mes_file'],'w') or ERROR('unable to open mes_file','','LIB3',__FUNCTION__,__LINE__);
if(!@fwrite($handle,implode('',$messagelist))){ERROR('unable to write mes_file','','LIB3',__FUNCTION__,__LINE__);}
fclose($handle);
}
#=============#
# ■ ログ保存 #
#=============#
function LOGSAVE2($work){
global $in,$pref;

global $deth;

$newlog = '';

if($work == "NEWENT"){		#新規登録
	global $cl,$no,$host2,$host;
	$newlog = $pref['now'].",".$in['F_Name'].",".$in['L_Name'].",".$in['Sex'].",$cl,$no,,,,,$host2,ENTRY,$host,\n";
}elseif($work == "G-DATT"){	#グループ脱退
	global $f_name,$l_name,$sex,$cl,$no,$teamold;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,,,,,,G-DATT,$teamold,\n";
}elseif($work == "G-JOIN"){	#グループ結成
	global $f_name,$l_name,$sex,$cl,$no;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,,,,,,G-JOIN,".$in['teamID2'].",\n";
}elseif($work == "G-KANYU"){	#グループ加入
	global $f_name,$l_name,$sex,$cl,$no;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,,,,,,G-KANYU,".$in['teamID2'].",\n";
}elseif($work == "ITEM_J"){	#グループ加入
	global $f_name,$l_name,$sex,$cl,$no,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$item_j;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,ITEM_J,$item_j,\n";
}elseif($work == "DEATH"){	#自分死亡(要因:罠)
	global $f_name,$l_name,$sex,$cl,$no,$dmes,$msg;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,,,,,,DEATH0,$dmes,\n";
	$death = "被陷阱算计得菊花遍地开";$msg=$dmes;
}elseif($work == "TRAP"){	#中罠)
	global $f_name,$l_name,$sex,$cl,$no,$dmes,$msg;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,,,,,,TRAP,\n";
}elseif($work == "POISON"){	#中毒
	global $f_name,$l_name,$sex,$cl,$no;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,,,,,,POISON,\n";
}elseif($work == "DEATH1"){	#自分死亡(要因:毒殺)
	global $f_name,$l_name,$sex,$cl,$no,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$dmes,$msg;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,DEATH1,$dmes,\n";
	$death = "服用毒物菊花过紧 导致菊栓提前爆炸";$msg=$dmes;
}elseif($work == "DEATH2"){	#自分死亡（要因：敗死）
	global $w_wep,$f_name,$l_name,$sex,$cl,$no,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$dmes,$d2,$w_msg,$msg;
	list($w_name,$w_kind) = explode("<>", $w_wep);
	if (preg_match("/K/",$w_kind)){$d2 = "被刀玩得菊花凋零了";}#斬系
	elseif((preg_match("/G/",$w_kind))&&($w_wtai > 0)){$d2 = "被射得太过威猛结果菊散人殒";}#銃系
	elseif(preg_match("/C/",$w_kind)){$d2 = "被强而有力的爱抚碾菊而破";}#投系
	elseif(preg_match("/D/",$w_kind)){$d2 = "被炉火纯青的菊爆直接超度";}#爆系	↓棍棒 or 弾無し銃 or 矢無し弓
	elseif((preg_match("/B/",$w_kind))||((preg_match("/G|A/",$w_kind))&&($w_wtai == 0))){$d2 = "配合得不太好菊瓣被玩烂了";}
	else {$d2 = "被菊爆";}
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,DEATH2,$dmes,\n";
	if ($w_no == "黑幕"){$deth = "$w_f_name $w_l_name 的$d2";}
	else {$deth = "$w_f_name $w_l_name（$w_cl $w_sex$w_no号）的$d2";}
	if ($w_msg != ""){$msg = "$w_f_name $w_l_name『$w_msg』";}
	else {$msg = "";}
}elseif($work == "DEATH3"){#敵死亡(要因:敗死)
	global $wep,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$f_name,$l_name,$sex,$cl,$no,$w_dmes,$d2,$w_msg,$msg;
	list($w_name,$w_kind) = explode("<>", $wep);
	if (preg_match("/K/",$w_kind)){$d2 = "用刀技术不太好让菊花凋零了";}#斬系
	elseif((preg_match("/G/",$w_kind))&&($wtai > 0)){$d2 = "射击太过威猛结果菊散人殒";}#銃系
	elseif(preg_match("/C/",$w_kind)){$d2 = "强力爱抚碾菊而破";}#投系
	elseif(preg_match("/D/",$w_kind)){$d2 = "菊爆水平炉火纯青直接超度";}#爆系	↓棍棒 or 弾無し銃 or 矢無し弓
	elseif((preg_match("/B/",$w_kind))||((preg_match("/G|A/",$w_kind))&&($wtai == 0))){$d2 = "配合不太好菊瓣被玩烂了";}
	else {$d2 = "爆菊" ;}
	$newlog = $pref['now'].",$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$f_name,$l_name,$sex,$cl,$no,DEATH3,$w_dmes,\n";
	$deth = "{$f_name} {$l_name}（{$cl} {$sex}{$no}号）的{$d2}";
	if ($msg != '') {$w_msg = "{$f_name} {$l_name}『{$msg}』";}
	else {$w_msg = '' ;}
	$w_log = "";
}elseif($work == "DEATH4"){	#政府による殺害
	global $w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_dmes,$log,$w_msg;
	$newlog = $pref['now'].",$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,,,,,,DEATH4,$w_dmes,\n";
	$deth = "被黑幕转学";
	$log ="";
	if($w_msg != "") {$msg = "黑幕『$w_msg』";}
	else{$msg = "" ;}
}elseif($work == "DEATH5"){	#政府による殺害2
	global $f_name,$l_name,$sex,$cl,$no,$dmes,$log,$msg;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,,,,,,DEATH4,$dmes,\n";
	$deth = "被绿坝娘和谐掉";
	$log ="";
	$msg = "绿坝娘『不行哦,做些奇怪的事情什么的!哼!』";
}elseif($work == "DEATHAREA"){	#死亡（要因：禁止エリア）
	global $w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_dmes,$log,$msg;
	$newlog = $pref['now'].",$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,,,,,,DEATHAREA,$w_dmes,\n";
	$deth = "没给绿坝娘让路";
	$msg = "" ;$log ="";
}elseif($work == "END"){	#終了
	global $f_name,$l_name,$sex,$cl,$no,$dmes;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,,,,,,END,$dmes,\n";
	$handle = @fopen ($pref['end_flag_file'], 'w');# or ERROR("unable to open file end_flag_file","","LIB4",__FUNCTION__,__LINE__);
	if(!@fwrite($handle,"结束\n")){ERROR("cannot write to end_flag_file","","LIB4",__FUNCTION__,__LINE__);}
	fclose($handle);
}elseif($work == "WINNER"){	#優勝決定
	global $f_name,$l_name,$sex,$cl,$no,$dmes;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,,,,,,WINNER,$dmes,\n";
}elseif($work == "HACK"){	#ハッキング成功
	global $f_name,$l_name,$sex,$cl,$no,$dmes;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,,,,,,HACK,$dmes,\n";
}elseif($work == "EX_END"){	#ハッキングによりプログラムを停止
	global $f_name,$l_name,$sex,$cl,$no,$dmes;
	$newlog = $pref['now'].",$f_name,$l_name,$sex,$cl,$no,,,,,,EX_END,$dmes,\n";
}elseif($work == "AREAADD"){	#禁止エリア追加
	global $ar,$ar2,$hh,$weth;
	$ar = $ar2 - 1 ;
	$newlog = $pref['now'].",$ar2,$ar,$hh,$weth,,,,,,,AREA,,\n";
} elseif ($work == "HKARAD"){	#ハッキング时禁止エリア追加
	global $ar2,$hh,$weth,$hackflg;
	$newlog = $pref['now'].",$ar2,$hackflg,$hh,$weth,,,,,,,HKARAD,,\n";
}

$loglist = @file($pref['log_file']) or ERROR("unable to open log_file","","LIB4",__FUNCTION__,__LINE__);
$loglist = implode('',$loglist);
$newlog .= $loglist;

$handle = @fopen($pref['log_file'],'w') or ERROR("unable to open file log_file","","LIB4",__FUNCTION__,__LINE__);
if(!@fwrite($handle,$newlog)){ERROR("cannot write to log_file","","LIB4",__FUNCTION__,__LINE__);}
fclose($handle);

}
#=============#
# ■ 不法侵入 #
#=============#
function KICKOUT(){#■エラー画面
global $in,$pref;

global $lockflag,$host,$ref_url,$HEADERINDEX,$ver,$ver2y;
if($lockflag){UNLOCK();}
if($ref_url == ""){$ref_url = "N/A";}
$Server_Name = $_SERVER{'SERVER_NAME'};

@header("Content-type: text/html;charset=UTF-8\n\n");
@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
// 常に修正されている
@header("Last-Modified: " . gmdate("D, d M Y H:i:s,$time") . " GMT");
// HTTP/1.1
@header("Cache-Control: no-store, no-cache, must-revalidate");
@header("Cache-Control: post-check=0, pre-check=0", false);
// HTTP/1.0
@header("Pragma: no-cache");

print <<<_HERE_
<HTML><HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html;CHARSET=UTF-8">
<META http-equiv=refresh content=3;url=..>
<TITLE>{$pref['game']}</TITLE>
<LINK REL="stylesheet" TYPE="text/css" HREF="BRU.CSS">
</HEAD>
<BODY contextmenu="false" bottommargin="0" rightmargin="0" topmargin="0" leftmargin="0" bgcolor="#000000" text="#ffffff" link="#ff0000" vlink="#ff0000" aLink="#ff0000" 

_HERE_;

print "onload=\"BRU.scrollIntoView(true)\"><CENTER><DIV ID=\"BRU\">";
print "<SCRIPT language=JavaScript src=\"BRU.JS\"></SCRIPT>";
print <<<_HERE_
<center><font color="#FF0000" face="ＭＳ Ｐ明朝" size="6"><span id="BR" style="width:100%;filter:blur(add=1,direction=135,strength=9):glow(strength=5,color=gold); font-weight:700; text-decoration:underline">不法入侵者发现</span></font></center><BR>
<P><FONT size="3">～请从正确入口进入游戏～</p></FONT>
3秒后将自动跳转。<br>
如果没有自动跳转，请点击<a href="..">这里<a>。<br>
<table border=0>
<tr><td>CODE-A : $ref_url</td></tr>
<tr><td>CODE-B : $host</td></tr>
<tr><td>CODE-C : $Server_Name</td></tr>
<tr><td>CODE-D : $in[Command]-$HEADERINDEX</td></tr>
</table>
<BR><B><FONT color="#ff0000"><U>HOME</U></B></Font>
</CENTER>
</DIV>
<div width="100%">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="2">
<HR>
</tr></td>
<tr><td>
<p style="text-align: right">
<b><font face="Viner Hand ITC" color="#ff0000" style="font-size: 9pt">
<a href="http://www.happy-ice.com/battle/">BATTLE ROYALE $ver</a><br>
<a href="mailto:2Y\@mindspring.com">BRU (Battle Royale Ultimate) Ver. $ver2y</a></font></b>
</p></tr></td>
</table>
</div>
</BODY>
</HTML>
_HERE_;
exit;
}
#=============#
# ■ ニュース #
#=============#
function news($log_file,$ar){
global $pref;

$kinshiarea = '';

$loglist = @file($log_file) or ERROR("unable to open log_file","","LIB4",__FUNCTION__,__LINE__);

$getmonth=$getday=$cnt=0;
$log = array();
foreach ($loglist as $loglisttmp){
	list($gettime,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_f_name2,$w_l_name2,$w_sex2,$w_cl2,$w_no2,$getkind,$host)= explode(",",$loglisttmp);
	list($sec,$min,$hour,$mday,$month,$year,$wday,$yday,$isdst) = localtime($gettime);
	if($hour < 10){$hour = "0$hour";}
	if ($min < 10){$min = "0$min";}
	$month++;
	$year += 1900;
	$weekday_array=array("日","一","二","三","四","五","六");
	$week = $weekday_array[$wday];
	if (($getmonth != $month)||($getday != $mday)){
		if ($getmonth !=0){array_push($log,"</LI></UL>");}
		$getmonth=$month;$getday = $mday;
		array_push($log,"<P><font color=\"lime\"><B>{$month}月{$mday}日 (星期{$week})</B></font><BR><UL>");
	}

	$wethe = "";
	$w_name = "{$w_f_name} {$w_l_name}（{$w_cl} {$w_sex}{$w_no}号）";
	$w_name2 = "{$w_f_name2} {$w_l_name2}（{$w_cl2} {$w_sex2}{$w_no2}号）";
	if	 (preg_match("/DEATH/",$getkind)){	#死亡
		if	  ($getkind == "DEATH0"){	#死亡(罠)
			array_push($log,"<LI>{$hour}时{$min}分、<font color=\"red\"><b>{$w_name}</b></font> 因 <font color=\"red\">陷阱</font> 而杯具。『<font color=\"yellow\"><b>{$host}</b></font>』<BR>");
		}elseif($getkind == "DEATH1"){	#死亡(毒殺)
			array_push($log,"<LI>{$hour}时{$min}分、<font color=\"red\"><b>{$w_name}</b></font> 因 <font color=\"red\">中毒</font> 而杯具。『<font color=\"yellow\"><b>{$host}</b></font>』<BR>");
		}elseif($getkind == "DEATH2"){	#死亡(他殺)
			array_push($log,"<LI>{$hour}时{$min}分、<font color=\"red\"><b>{$w_name}</b></font> 因 <font color=\"red\">$w_name2</font> 而杯具。『<font color=\"yellow\"><b>{$host}</b></font>』<BR>");
		}elseif($getkind == "DEATH3"){	#死亡(他殺)
			array_push($log,"<LI>{$hour}时{$min}分、<font color=\"red\"><b>{$w_name}</b></font> 因 <font color=\"red\">$w_name2</font> 而杯具。『<font color=\"yellow\"><b>{$host}</b></font>』<BR>");
		}elseif($getkind == "DEATH4"){	#死亡(政府)
			array_push($log,"<LI>{$hour}时{$min}分、<font color=\"red\"><b>{$w_name}</b></font> 因 <font color=\"red\">黑幕</font> 而杯具。『<font color=\"yellow\"><b>{$host}</b></font>』<BR>");
		}elseif($getkind == "DEATHAREA"){#死亡(禁止エリア)
			array_push($log,"<LI>{$hour}时{$min}分、<font color=\"red\"><b>{$w_name}</b></font> 因 <font color=\"red\">没给绿坝娘让路</font> 而杯具。『<font color=\"yellow\"><b>{$host}</b></font>』<BR>");
		}
	}elseif(preg_match("/G-/",$getkind)){	#グループ
		if	  ($getkind == "G-DATT"){	#グループ脱退
			array_push($log,"<LI>{$hour}时{$min}分、<font color=\"skyblue\"><b>{$w_name}</b></font> 从小组 <font color=\"skyblue\"><b>【{$host}】</b></font> 退出。<BR>");
		}elseif($getkind == "G-JOIN"){	#グループ結成
			array_push($log,"<LI>{$hour}时{$min}分、<font color=\"skyblue\"><b>{$w_name}</b></font> 结成了 <font color=\"skyblue\"><b>【{$host}】</b></font> 小组。<BR>");
		}elseif($getkind == "G-KANYU"){	#グループ加入
			array_push($log,"<LI>{$hour}时{$min}分、<font color=\"skyblue\"><b>{$w_name}</b></font> 加入了 <font color=\"skyblue\"><b>【{$host}】</b></font> 小组。<BR>");
		}
	}elseif(preg_match("/POISON/",$getkind)){	#中毒
		array_push($log,"<LI>{$hour}时{$min}分、<font color=\"red\"><b>{$w_name}</b></font> 吃了带毒的东西脸色潮红见人就扑，各位小碰友请看紧自己的菊花。");
	}elseif(preg_match("/TRAP/",$getkind)){	#中陷阱
		array_push($log,"<LI>{$hour}时{$min}分、<font color=\"red\"><b>{$w_name}</b></font> 中了某人安放的温柔陷阱，犯人在黑暗的角落窃笑。");
	}elseif($getkind == "ITEM_J"){		#ITEM譲渡
		@list($i_name,$i_kind) = explode("<>",$host);
		if	(preg_match("/HH|HD/",$i_kind))	{$itemtype = "【生命回复】";}
		elseif	(preg_match("/SH|SD/",$i_kind))	{$itemtype = "【体力回复】";}
		elseif	($i_kind == "TN")	{$itemtype = "【陷阱】";}
		elseif	(preg_match("/W/",$i_kind))	{$itemtype = "【武器：";
			if	(preg_match("/G/",$i_kind))	{$itemtype = ($itemtype . "射");
				if	(preg_match("/S/",$i_kind))	{$itemtype = ($itemtype . "-消音");}
				$itemtype = ($itemtype . ")");}
			if	(preg_match("/K/",$i_kind))	{$itemtype = ($itemtype . "斩");}
			if	(preg_match("/C/",$i_kind))	{$itemtype = ($itemtype . "投");}
			if	(preg_match("/B/",$i_kind))	{$itemtype = ($itemtype . "殴");}
			if	(preg_match("/D/",$i_kind))	{$itemtype = ($itemtype . "爆");}
			$itemtype = ($itemtype . "】");}
		elseif	(preg_match("/D/",$i_kind))	{$itemtype = "【防具：";
			if	(preg_match("/B/",$i_kind))	{$itemtype = ($itemtype . "体");}
			if	(preg_match("/H/",$i_kind))	{$itemtype = ($itemtype . "头");}
			if	(preg_match("/F/",$i_kind))	{$itemtype = ($itemtype . "足");}
			if	(preg_match("/A/",$i_kind))	{$itemtype = ($itemtype . "腕");}
				if	(preg_match("/K/",$i_kind))	{$itemtype = ($itemtype . "-对：斩 ");}
			$itemtype = ($itemtype . "】");}
		elseif(($i_kind == "R1")||($i_kind == "R2")) {$itemtype = "【雷达】";}
		elseif($i_kind == "Y") {
			if(($i_name == "腐烂的滋味酱")||($i_name == "滋味酱")) {$itemtype = "【滋味酱】";}
			elseif($i_name == "子弹") {$itemtype = "【子弹】";}
			else {$itemtype = "【道具】";}}
		elseif($i_kind == "A") {$itemtype = "【装饰品】";}
		else {$itemtype = "【不明】";}
		array_push($log,"<LI>{$hour}时{$min}分、<font color=\"#00ffff\"><b>{$w_name}</b></font> 将 <font color=\"#00ffff\"><b>道具$itemtype</b></font> 转让给了 <font color=\"#00ffff\">$w_name2</font>。<BR>");
	}elseif($getkind == "HACK"){		#HACK成功
		array_push($log,"<LI>{$hour}时{$min}分、<font color=\"#CA9DE1\"><b>{$w_name}</b></font> 的 <font color=\"red\">Hacking</font> 成功了。<font color=\"red\"><b>{$host}</b></font><BR>");
	}elseif($getkind == "WINNER"){	#終了
		$log_num = array_pop($log);
		if (preg_match("/优胜者/",$log_num)){array_push($log,$log_num);}
		else{array_push($log,$log_num);array_push($log,"<LI>{$hour}时{$min}分、<font color=\"lime\"><b>优胜者：{$w_name}</B></font> <BR>");}
	}elseif($getkind == "END"){	#終了
		$log_num = array_pop($log);
		if (preg_match("/游戏结束/",$log_num)){array_push($log,$log_num);}
		else{array_push($log,$log_num);array_push($log,"<LI>{$hour}时{$min}分、<font color=\"lime\"><b>游戏结束，剩余人员获得了进修好男人课程的资格</B></font> <BR>");}
	}elseif ($getkind == "EX_END"){	#プログラム停止
		$log_num = array_pop($log);
		if (preg_match("/游戏结束/",$log_num)){array_push($log,$log_num);}
		else{
			array_push($log,$log_num);
			array_push($log,"<LI>{$hour}时{$min}分、<font color=\"lime\"><b>菊栓全部停止运作，游戏结束</B></font> <BR>") ;
		}
	}elseif(($getkind == "AREA")&&($ar != '')){	#禁止エリア追加
		$log_num = array_pop($log);
		if ($w_cl != ""){$wethe = "<font color=\"orange\">【天气：".$pref['weather'][$w_cl]."】</font>";}
		if ((!preg_match("/游戏结束/",$log_num))||(!preg_match("/<UL>/",$log_num))){
			array_push($log,$log_num);

			#CONF
			$pgsplit = 1;
			
			$kinshi0 = $w_sex;
			$kinshi1 = $w_sex + $pgsplit;
			$kinshi2 = $w_sex + $pgsplit + $pgsplit;
			$kinshi3 = $w_sex + $pgsplit + $pgsplit + $pgsplit;
			
			if($kinshi0 < 10){$kinshi0 = '0'.$kinshi0;}
			if($kinshi1 >= 24){$kinshi1 -= 24;}
			if($kinshi2 >= 24){$kinshi2 -= 24;}
			if($kinshi3 >= 24){$kinshi3 -= 24;}
			if($kinshi1 < 10){$kinshi1 = '0'.$kinshi1;}
			if($kinshi2 < 10){$kinshi2 = '0'.$kinshi2;}
			if($kinshi3 < 10){$kinshi3 = '0'.$kinshi3;}
			
			$kinshi = array($kinshi0,$kinshi1,$kinshi2,$kinshi3);


			if(isset($pref['place'][$ar[$w_f_name]])){
				$add = "下次绿坝娘会到 <font color=\"lime\">{$kinshi[1]}时 <b>".$pref['place'][$ar[$w_f_name]];
				if(isset($pref['place'][$ar[$w_f_name+1]])){
					$add .= "、</b>{$kinshi[2]}时 <b>".$pref['place'][$ar[$w_f_name+1]];
					if(isset($pref['place'][$ar[$w_f_name+2]])){
						$add .= "、</b>".$kinshi[3]."时 <b>".$pref['place'][$ar[$w_f_name+2]]."</b></font>。";
					}else{
						$add .= "</b></font>。";
					}
				}else{
					$add .= "</b></font>。";
				}
			}

			array_push($log,"<LI>{$kinshi[0]}时00分、<font color=\"lime\"><b>".$pref['place'][$ar[$w_l_name]]."</b></font> 迎接了绿坝娘的到来。{$add}{$wethe}<BR>") ;

			if ($kinshiarea == 0){$kinshiarea = $w_sex;}
		}else{array_push($log,$log_num);}
	} elseif ($getkind == "HKARAD"){	#ハック时禁止エリア追加
		$log_num = array_pop($log);
		if ($w_cl != ""){$wethe = "<font color=\"orange\">【天气：".$pref['weather'][$w_cl]."】</font>";}
		if ((!preg_match("/游戏结束/",$log_num))||(!preg_match("/<UL>/",$log_num))){
			array_push($log,$log_num);$hkrem = $w_l_name*8;

			#CONF
			$pgsplit = 1;

			$kinshi0 = $w_sex;
			$kinshi1 = $w_sex + $pgsplit;
			$kinshi2 = $w_sex + $pgsplit + $pgsplit;
			$kinshi3 = $w_sex + $pgsplit + $pgsplit + $pgsplit;
			
			if($kinshi0 < 10){$kinshi0 = '0'.$kinshi0;}
			if($kinshi1 >= 24){$kinshi1 -= 24;}
			if($kinshi2 >= 24){$kinshi2 -= 24;}
			if($kinshi3 >= 24){$kinshi3 -= 24;}
			if($kinshi1 < 10){$kinshi1 = '0'.$kinshi1;}
			if($kinshi2 < 10){$kinshi2 = '0'.$kinshi2;}
			if($kinshi3 < 10){$kinshi3 = '0'.$kinshi3;}
			
			$kinshi = array($kinshi0,$kinshi1,$kinshi2,$kinshi3);

			if(isset($pref['place'][$ar[$w_f_name]])){
				$add = "下次绿坝娘会到 <font color=\"lime\">{$kinshi[1]}时 <b>".$pref['place'][$ar[$w_f_name]];
				if(isset($pref['place'][$ar[$w_f_name+1]])){
					$add .= "、</b>{$kinshi[2]}时 <b>".$pref['place'][$ar[$w_f_name+1]];
					if(isset($pref['place'][$ar[$w_f_name+2]])){
						$add .= "、</b>{$kinshi[3]}时 <b>".$pref['place'][$ar[$w_f_name+2]]."</b></font>。";
					}else{
						$add .= "</b></font>。";
					}
				}else{
					$add .= "</b></font>。";
				}
			}

			array_push($log,"<LI>{$kinshi[0]}时00分、离管理系统再开还有<font color=\"lime\"><b>{$hkrem}</b></font> 小时。{$add}{$wethe}<BR>") ;

			if ($kinshiarea == 0){$kinshiarea = $w_sex;}
		}else{array_push($log,$log_num);}
		if (($host != "")&&($pref['host_view'])) {$host = "($host)"; }
		array_push($log,"<LI>{$hour}时{$min}分、<font color=\"yellow\"><b>{$w_name}</b></font>转学过来了。『<font color=\"black\">$host</font>』<BR>") ;
	} elseif ($getkind == "NEWGAME"){#管理人によるデータ初期化
		if (isset($pref['weather'][$w_f_name])){$wethe = "<font color=\"orange\">【天気：".$pref['weather'][$w_f_name]."】</font>";}
		array_push($log,"<LI>{$hour}时{$min}分、下一轮游戏开始。{$wethe}<BR>") ;
	}
	$cnt++;
}
$log = implode("",$log);
return $log;
}
?>
