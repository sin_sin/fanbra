<?php
#□■□■□■□■□■□■□■□■□■□■□
#■ 	- BR ADMINISTRATOR SCRIPT - 	 ■
#□ 									 □
#■ 		  サブルーチン一覧  		 ■
#□ 									 □
#□ DATARESET	-	データ初期化		 □
#□■□■□■□■□■□■□■□■□■□■□
#=================#
# ■ データ初期化 #
#=================#
function DATARESET(){
global $pref;
global $RESET0,$RESET1,$RESET2,$RESET3,$RESET4,$RESET5,$RESET6,$RESET7,$RESET8,$RESET9,$RESET10,$RESET11,$RESET12;
global $res_j,$res_f,$res_m,$res_d,$res_y;
if(!$RESET9){ERROR('请同意');}#同意
if($RESET11){#日時指定
	if(mktime(date('G'),date('i'),0,date('n'),date('j'),date('Y')) >= mktime($res_j,$res_f,0,$res_m,$res_d,$res_y)){
		ERROR('请稍候进行设定.现在的时间'.date('Y').'年'.date('m').'月'.date('d').'日'.date('H').'时'.date('i').'分');
	}else{
		if($RESET12){
			$pre_reg = 1;
		}else{
			$pre_reg = 0;
		}
	}
	$ka = 0;
}else{
	if($RESET12){
		ERROR('如果要马上进行初始化,请不要事先登录。');
	}else{
		$res_y = date('Y');
		$res_m = date('n');
		$res_d = date('j');
		$res_j = date('G');
		$res_f = date('i');
		$pre_reg = 0;
		#禁止エリアファイル更新
		$pgsplit = 1;
		$res_j = $pgsplit * (round($res_j / $pgsplit)+1);
		if($res_j >= 24){$res_j-=24;$res_d++;}
		$ka = 1;
	}
}


if($RESET0){#NPC 初期化
	$baselist = @file($pref['npc_file']) or ERROR("unable to open file npc_file");
	$basyo = 1;
	if(count($baselist) && $pref['npc_mode']){
		for ($i=0; $i<count($baselist); $i++){
			list($w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,) = explode(",",$baselist[$i]);
			if($w_cl == $pref['BOSS']){	#政府側のNPC
				$w_wep = "滋味的热狗<>WG";$w_watt = "200";$w_wtai = "500";
				$w_bou = "滋味的围裙<>DB";$w_bdef = "140";$w_btai = "233";
				$w_bou_h = "滋味的脸盆<>DH";$w_bdef_h = "120";$w_btai_h = "233";
				$w_bou_f = "滋味的木屐<>DF";$w_bdef_f = "120";$w_btai_f = "233";
				$w_bou_a = "滋味的手套<>DA";$w_bdef_a = "80";$w_btai_a = "233";
				if($w_l_name == "滋味酱"){$w_icon = "42";}
				$w_item[0] = "滋味酱<>Y";$w_eff[0] = "1";$w_itai[0] = "1";
				$w_item[1] = "无";$w_eff[1] = "0";$w_itai[1] = "0";
				$w_item[2] = "无";$w_eff[2] = "0";$w_itai[2] = "0";
				$w_item[3] = "无";$w_eff[3] = "0";$w_itai[3] = "0";
				$w_item[4] = "无";$w_eff[4] = "0";$w_itai[4] = "0";
				$w_item[5] = "路人身份证<>A";$w_eff[5] = "1";$w_itai[5] = "1";
				$w_dmes = "只不过,要记住『人生就是一场游戏。』";$w_com = "不可以哦,如果弄疼了老师就不好了";$w_msg = "啊?可以看到了";
				$w_att = 300;$w_def = 500;$w_hit = 3000;
				$w_level = 30; $w_exp = (int)($w_level*$pref['baseexp']+(($w_level-1)*$pref['baseexp'])) - 17;
				$w_tactics = "无"; $w_death = "";
				$w_pls = 0;
				$w_wp=$w_wg=$w_wn=$w_wc=$w_wd=100;
				$w_mhit=$w_hit; $w_sta = $pref['maxsta'];
				$w_sts = "NPC";
				$w_money = "2000";
			}elseif($w_cl == $pref['KANN']){	#政府側のNPC
				$w_wep = "突击步枪α<>WG";$w_watt = "100";$w_wtai = "200";
				$w_bou = "军服α<>DB";$w_bdef = "50";$w_btai = "100";
				$w_bou_h = "军帽α<>DH";$w_bdef_h = "80";$w_btai_h = "100";
				$w_bou_f = "军靴α<>DF";$w_bdef_f = "80";$w_btai_f = "100";
				$w_bou_a = "军服α<>DA";$w_bdef_a = "80";$w_btai_a = "100";
				if($w_l_name == "准"){$w_icon = "43";}
				elseif($w_l_name == "灵梦"){$w_icon = "44";}
				elseif($w_l_name == "喵喵"){$w_icon = "45";}
				elseif($w_l_name == "七夜"){$w_icon = "46";}
				$w_item[0] = "无";$w_eff[0] = "0";$w_itai[0] = "0";
				$w_item[1] = "无";$w_eff[1] = "0";$w_itai[1] = "0";
				$w_item[2] = "无";$w_eff[2] = "0";$w_itai[2] = "0";
				$w_item[3] = "无";$w_eff[3] = "0";$w_itai[3] = "0";
				$w_item[4] = "无";$w_eff[4] = "0";$w_itai[4] = "0";
				$w_item[5] = "路人身份证<>A";$w_eff[5] = "1";$w_itai[5] = "1";
				$w_dmes = "路人也要欺负TAT";$w_com = "不好意思，路过~";$w_msg = "路过中";
				$w_att = 200;$w_def = 400;$w_hit = 2000;
				$w_level = 20; $w_exp = (int)($w_level*$pref['baseexp']+(($w_level-1)*$pref['baseexp'])) - 17;
				$w_tactics = "无"; $w_death = "";
				$w_pls = rand(0, count($pref['area'])-1);
				$w_wp=$w_wg=$w_wn=$w_wc=$w_wd=100;
				$w_mhit=$w_hit; $w_sta = $pref['maxsta'];
				$w_sts = "NPC";
				$w_money = "1500";
			}elseif($w_cl == $pref['ZAKO']){	#政府側のNPC
				$w_wep = "军用步枪β<>WG";$w_watt = "50";$w_wtai = "200";
				$w_bou = "军服β<>DB";$w_bdef = "50";$w_btai = "100";
				$w_bou_h = "军帽β<>DH";$w_bdef_h = "50";$w_btai_h = "100";
				$w_bou_f = "军靴β<>DF";$w_bdef_f = "50";$w_btai_f = "100";
				$w_bou_a = "军服β<>DA";$w_bdef_a = "50";$w_btai_a = "100";
				if($w_l_name == "阿部"){$w_icon = "47";}
				elseif($w_l_name == "蓝叔"){$w_icon = "48";}
				elseif($w_l_name == "小R"){$w_icon = "49";}
				elseif($w_l_name == "受寒"){$w_icon = "50";}
				$w_item[0] = "无";$w_eff[0] = "0";$w_itai[0] = "0";
				$w_item[1] = "无";$w_eff[1] = "0";$w_itai[1] = "0";
				$w_item[2] = "无";$w_eff[2] = "0";$w_itai[2] = "0";
				$w_item[3] = "无";$w_eff[3] = "0";$w_itai[3] = "0";
				$w_item[4] = "无";$w_eff[4] = "0";$w_itai[4] = "0";
				$w_item[5] = "大叔好人卡<>A";$w_eff[5] = "1";$w_itai[5] = "1";
				$w_dmes = "我会记住你菊花的!";$w_com = "Nice菊!";$w_msg = "窟窟窟~";
				$w_att = 100;$w_def = 300;$w_hit = 1000;
				$w_level = 10; $w_exp = (int)($w_level*$pref['baseexp']+(($w_level-1)*$pref['baseexp'])) - 17;
				$w_tactics = "无"; $w_death = "";
				$w_pls = rand(0, count($pref['area'])-1);
				$w_wp=$w_wg=$w_wn=$w_wc=$w_wd=20;
				$w_mhit=$w_hit; $w_sta = $pref['maxsta'];
				$w_sts = "NPC";
				$w_money = "800";
			}else{;}#その他のNPCはこっち
			$w_comm = "NPC";
			$w_sts = "NPC";
			$w_limit = $w_kill = $w_endtime = 0;
			$w_id = ($pref['a_id'] . "$i");
			$w_log = $w_bid = $w_bb = $w_inf = $w_IP = "";
			$w_seikaku = "无";	#性格未完成
			$w_sinri = "无";		#心理未完成
			$w_teamID = $pref['a_group_id'];	#チームID未完成
			$w_teamPass = $pref['a_group_pass'];	#チームパス
			$w_tactics = $w_ousen = "通常";
			$w_club = 20;
			$w_password = $pref['a_pass_npc'];
			$w_item_get = "无";
			$w_eff_get = "0";
			$w_itai_get = "0";
			$userlist[$i] = "$w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP,\n";
		}
	}
	$handle = @fopen($pref['user_file'],'w') or ERROR("unable to open user_file");
	if(!@fwrite($handle,implode('',$userlist))){ERROR("unbale to write user_file");}
	fclose($handle);
}if($RESET1){#時間 初期化
	#時間ファイル更新
	$endtime = $pref['now'] + ($pref['battle_limit']*60*60);
	$timelist=$pref['now'].','.$endtime.",\n";
	$handle = @fopen($pref['time_file'],'w') or ERROR("unable to open time_file");
	if(!@fwrite($handle,$timelist)){ERROR("unbale to write time_file");}
	fclose($handle);
}if($RESET2){#生徒番号 初期化
	#生徒番号ファイル更新
	$memberlist="0,0,0,0,\n";
	$handle = @fopen($pref['class_file'],'w') or ERROR("unable to open ".$pref['class_file']);
	if(!@fwrite($handle,$memberlist)){ERROR("unbale to write class_file");}
	fclose($handle);
}if($RESET3){#禁止エリア 初期化
	global $areadata;
	#禁止エリアファイル更新
	#初期化設定

	$areadata[0] = $res_y.','.$res_m.','.$res_d.','.(int)$res_j.",0\n";#エリア追加時刻
	$areadata[1] = $ka.",0,\n";#禁止エリア数、ハッキングフラグ

	$work = $pref['place'];
	$work2 = $pref['area'];
	$work3 = $pref['arno'];

	$ar = implode('',array_splice($work,0,1));
	$areadata[2] = $ar.',';
	$ar2 = implode('',array_splice($work2,0,1));
	$areadata[3] = $ar2.',';
	$ar3 = implode('',array_splice($work3,0,1));
	$areadata[4] = $ar3.',';
	$temp='';
	for ($i=1; $i<count($pref['place']); $i++){
		$chk = count($work) - 1;$index = rand(0,$chk);
		$ar  = implode('',array_splice($work,$index,1));$areadata[2] = ($areadata[2].$ar.',');
		$ar2 = implode('',array_splice($work2,$index,1));$areadata[3] = ($areadata[3].$ar2.',');
		$ar3 = implode('',array_splice($work3,$index,1));$areadata[4] = ($areadata[4].$ar3.',');
	}

	if(!$RESET12){#いま初期化
		$res_y = date('Y');
		$res_m = date('n');
		$res_d = date('j');
		$res_j = date('G');
		$res_f = date('i');
		$pre_reg = 0;
	}

	#天気
	$weth = rand(0,9);
	$areadata[2] = ($areadata[2] . "\n");
	$areadata[3] = ($areadata[3] . "\n");
	$areadata[4] = ($areadata[4] . "\n");
	$areadata[5] = $weth."\n";
	#BR開催回数UP?
	$BRNUM = rtrim($pref['arealist'][6]);
	if($RESET10){$BRNUM++;}
	$areadata[6] = $BRNUM . "\n";
	$areadata[7] = $pre_reg.','.$res_y.','.$res_m.','.$res_d.','.$res_j.','.(int)$res_f.',0,';

	$handle = @fopen($pref['area_file'],'w') or ERROR('unable to open area_file');
	if(!@fwrite($handle,implode('',$areadata))){ERROR('unbale to write area_file');}
	fclose($handle);
}if($RESET4){#ログの初期化 及び プログラム開始ログ 追加
	#Newgame Log
	$loglist = $pref['now'].','.$weth.",,,,,,,,,,NEWGAME,,\n";
	$handle = @fopen($pref['log_file'],'w') or ERROR('unable to open log_file');
	if(!@fwrite($handle,$loglist)){ERROR('unbale to write log_file');}
	fclose($handle);
}if($RESET5){#アイテムログ 初期化
	global $filename;
	$areaitems = array();
	#Del Area Item
	for ($i=0; $i<count($pref['area']); $i++){
		$areaitems[$i]='';
		$filename = $pref['LOG_DIR'].'/'.$i.$pref['item_file'];
		$handle = @fopen($filename,'w') or ERROR("unable to open $filename","パーミッションエラー","adm_res",__FUNCTION__,__LINE__);
		fclose($handle);
	}

	#Put Area Item
	$itemlist = @file($pref['DAT_DIR']."/itemfile.dat") or ERROR("unable to open itemfile");

	for ($i=0;$i<count($itemlist);$i++){
		list($idx,$count,$w_i,$w_e,$w_t,$br) = explode(",",$itemlist[$i]);
		if($idx == 99){
			for ($a=$count; $a>0;$a--){
				$areaitems[rand(0,count($pref['area'])-1)] .= "$w_i,$w_e,$w_t,\n";
			}
		}else{
			$areaitems[$idx] .= str_repeat("$w_i,$w_e,$w_t,\n",$count);
		}
	}

	foreach($areaitems as $area => $items){
		$filename = $pref['LOG_DIR'].'/'.$area.$pref['item_file'];
		$handle = @fopen($filename,'w') or ERROR("unable to open $filename","パーミッションエラー","adm_res",__FUNCTION__,__LINE__);
		if(!@fwrite($handle,$items)){ERROR("unbale to write $filename");}
		fclose($handle);
	}

	//print_r($areaitems);

}if($RESET5){#売店アイテム 初期化
	#売店ファイル更新
	$baitendata = @file($pref['DAT_DIR']."/item_baiten.dat") or ERROR("unable to open item_baiten");
	$handle = @fopen($pref['baiten_file'],'w') or ERROR("unable to open baiten_file");
	if(!@fwrite($handle,implode('',$baitendata))){ERROR("unbale to write baiten_file");}
	fclose($handle);
}if($RESET6){#銃声ログ 初期化
	#銃声ログファイル更新
	$null_data[0] = "0,,,,\n";
	$null_data[1] = "0,,,,\n";
	$null_data[2] = "0,,,,\n";
	$null_data[3] = "0,,,,\n";
	$null_data[4] = "0,,,,\n";
	$null_data[5] = "0,,,,\n";
	$handle = @fopen($pref['gun_log_file'],'w') or ERROR("unable to open gun_log_file");
	if(!@fwrite($handle,implode('',$null_data))){ERROR("unbale to write gun_log_file");}
	fclose($handle);
}if($RESET7){#ユーザー保存データフォルダ 削除
	#global ;
	#ユーザ保存データ削除
}if($RESET8){#Flagファイル 更新
	#FLAGファイル更新
	$handle = @fopen($pref['end_flag_file'],'w') or ERROR("unable to open end_flag_file");
	fclose($handle);
}if($RESET9){#Messenger 更新
	$handle = @fopen($pref['mes_file'],'w') or ERROR("unable to open mes_file");
	fclose($handle);
	$handle = @fopen($pref['member_file'],'w') or ERROR("unable to open memberfile","パーミッションエラー","adm_res",__FUNCTION__,__LINE__);
	fclose($handle);
	$handle = @fopen($pref['end_flag_file'],'w') or ERROR("unable to open end_flag_file");
	fclose($handle);
}
HEAD();
print <<<_HERE_
<center><font color="#FF0000" face="ＭＳ Ｐ明朝" size="6"><span id="BR" style="width:100%;filter:blur(add=1,direction=135,strength=9):glow(strength=5,color=gold); font-weight:700; text-decoration:underline">Administrative Mode</span></font></center>
初期化しました。<br>
<br><B><FONT color="#ff0000">>><a href="index.php">HOME</a> >><a href="admin.php">ADMIN</a></b></FONT>
_HERE_;
FOOTER();
}
?>