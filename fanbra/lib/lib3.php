<?php
#□■□■□■□■□■□■□■□■□■□■□
#■ 	-    BR LIBRARY PROGRAM    - 	 ■
#□ 									 □
#■ 		サブルーチン一覧			 ■
#□ 									 □
#■ BAITEN		-	売店処理			 ■
#□ POISON		-	毒物混入処理		 □
#■ PSCHECK		-	毒見処理			 ■
#□ POISONDEAD	-	毒物死亡処理		 □
#■ WINCHG		-	口癖変更処理		 ■
#□ TEAM		-	チーム変更処理		 □
#■ OUKYU		-	応急処置処理		 ■
#□ KOUDOU		-	基本方針変更		 □
#■ OUSEN		-	応戦方針変更		 ■
#□ SPEAKER		-	携帯スピーカ使用	 □
#■ HACKGIN		-	ハッキング処理		 ■
#□■□■□■□■□■□■□■□■□■□■□
#=============#
# ■ 売店処理 #
#=============#
function BAITEN(){
global $in,$pref;

global $BN;
$BT = $in['Command'];
$BT = preg_replace("/BAITEN_/","",$BT);
print "想买哪种道具呢？？<BR><BR>";

$baitenlist = @file($pref['baiten_file']) or ERROR("unable to open baiten_file","","LIB3",__FUNCTION__,__LINE__);
for ($i=0;$i<count($baitenlist);$i++){
	list($b_b,$b_c,$b_i,$b_e,$b_t) = explode(",",$baitenlist[$i]);
	$cost = $b_c * 100;
	if(preg_match("/<>/",$b_i)){list($b_ii,$b_ik) = explode("<>",$b_i);}
	if((($BT == "SPECIAL")||($BT == "BAITEN"))&&($b_b == $b_c)&&($b_e == "")&&($b_t == "")){#アイテム選択なし→種類表示
		Auto();print "<INPUT type=\"radio\" name=\"Command\" value=\"BAITEN_$b_b\">$b_i</A><BR>";
	}elseif(($b_b == $b_c)&&($b_e == "")&&($b_t == "")){#種類番号取得
		$BN = $b_b;
	}elseif($BT == $BN){#アイテム表示
		if($b_b){Auto();print "<INPUT type=\"radio\" name=\"Command\" value=\"BAITEN2_{$BT}_{$b_ii}\">{$b_ii} [剩:{$b_b} 价:{$cost}]</A><BR>";}
	}
}
if(($BT != "SPECIAL")&&($BT != "BAITEN")){
	Auto();print "<INPUT type=\"radio\" name=\"Command\" value=\"BAITEN\" checked>返回商店</A><BR>";
}#アイテム選択なし→種類表示
Auto();print "<INPUT type=\"radio\" name=\"Command\" value=\"MAIN\" checked>返回</A><BR>";
print "<BR><INPUT type=\"submit\" name=\"Enter\" value=\"决定\">";
}
#==============#
# ■ 売店処理2 #
#==============#
function BAITEN2(){
global $in,$pref;

global $item,$money,$log;
$BT = $in['Command'];
$BT = preg_replace("/BAITEN2_/","",$BT);
list($itn, $ik) = explode("_",$BT);

$baitenlist = @file($pref['baiten_file']) or ERROR("unable to open baiten_file","","LIB3",__FUNCTION__,__LINE__);
$chkflg = 1;
for ($b=0;$b<count($baitenlist);$b++){
	list($b_b,$b_c,$b_i,$b_e,$b_t) = explode(",",$baitenlist[$b]);
	if(preg_match("/<>/",$b_i)){list($b_ii,$b_ik) = explode("<>",$b_i);}
	if(($b_b == $b_c)&&($b_e == "")&&($b_t == "")){#種類番号取得
		$BN = $b_b;
	}elseif(($itn == $BN)&&($ik == $b_ii)&&($b_e != "")&&($b_t != "")){#アイテム一致
		$chkflg=0;break;
	}
}
if($chkflg){ERROR("Internal Server Error","break error","LIB3",__FUNCTION__,__LINE__);}
list($b_ii,$b_ik) = explode("<>",$b_i);
$chkflg = -1;
for ($i=0; $i<5; $i++){if($item[$i] == "无"){$chkflg = $i;break;}}#空きアイテム？
if($b_b <= 0){$log = ($log . "$b_ii好像卖光了呢…<BR>");}
elseif($b_c > $money){$log = ($log . "你钱不够呢…好像…<BR>");}
elseif($chkflg == -1){$log = ($log . "但是包里再也放不下东西了呢。<BR>${b_ii}什么的下次再买吧…。<BR>");}#所持品オーバー
else{
	global $eff,$itai;
	$money = $money - $b_c;#支払い
	if($item[$chkflg] == "无"){$item[$chkflg] = $b_i; $eff[$chkflg] = $b_e; $itai[$chkflg] = $b_t;}
	else{ERROR("Internal Server Error","CHKFLG error","LIB3",__FUNCTION__,__LINE__);}
	$log = ($log . "{$b_ii}买到了。");
	$b_b--;#個数マイナス
	$baitenlist[$b] = "$b_b,$b_c,$b_i,$b_e,$b_t,\n";

	$handle = @fopen($pref['baiten_file'],'w') or ERROR("unable to open baiten_file","","LIB3",__FUNCTION__,__LINE__);
	if(!@fwrite($handle,implode('',$baitenlist))){ERROR("unable to baiten_file","","LIB3",__FUNCTION__,__LINE__);}
	fclose($handle);
}
SAVE();
}
#=================#
# ■ 毒物混入処理 #
#=================#
function POISON(){
global $in,$pref;

global $item,$eff,$itai,$id,$log;
for ($i=0; $i<5; $i++){
	if(strpos($item[$i],'毒药')!==false){break;}
}
$wk = $in['Command'];
$wk = str_replace('POI_','',$wk);
if(!preg_match('/<>SH|<>HH|<>SD|<>HD/',$item[$wk]) || strpos($item[$i],'毒药')===false){ERROR('非法访问','','LIB3',__FUNCTION__,__LINE__);}
$itai[$i]--;
if($itai[$i] <= 0){$item[$i] = '无'; $eff[$i] = $itai[$i] = 0;}
list($itn, $ik) = explode('<>',$item[$wk]);
$log .= $itn.' 已经下了毒。<BR>小心不要自己吃到哦。<br>';

$poison_n='';
if($pref['clb'][$itn['club']]=='料理部'){
	$poison_n='2';
}elseif($item[5]=='毒物说明书<>A'){
	$poison_n='1';
}

if(substr($ik,0,1)=='H'){
	$item[$wk]=$itn.'<>HD'.$poison_n.'-'.$id;
}else{
	$item[$wk]=$itn.'<>SD'.$poison_n.'-'.$id;
}

SAVE();
$in['Command'] = "MAIN";
}
#=============#
# ■ 毒見処理 #
#=============#
function PSCHECK(){
global $in,$pref;

global $item,$sta,$log;
$wk = $in['Command'];
$wk = preg_replace("/PSC_/","",$wk);
if((!preg_match("/<>SH|<>HH|<>SD|<>HD/",$item[$wk]))||($pref['clb'][$in['club']] != "料理部")){ERROR("非法操作","Not in such club","LIB3",__FUNCTION__,__LINE__);}
elseif($sta <= $pref['dokumi_sta']){$log = ($log . "体力不足，没能发现毒物啊");return;}
list($itn, $ik) = explode("<>",$item[$wk]);
if(preg_match("/SH|HH/",$ik)){$log = ($log . "嗯？ {$itn} 吃起来很安全的样子…。<br>");}
else{$log = ($log . "嗯？ {$itn} 里面混入了毒物啊…。<br>");}
$sta -= $pref['dokumi_sta'];

SAVE();
$in['Command'] = "MAIN";
}
#=================#
# ■ 毒物死亡処理 #
#=================#
function POISONDEAD(){
global $pref;

global $hit,$mem,$poisondeadchk,$sinri,$wb,$poison,$poisoni;
if($hit <= 0){

$hit = 0;
$mem--;

$com = rand(0,6);
$poisonid="2Y0";

if($poisondeadchk){list($tp,$poisonid) = explode('-',$poisoni);}
else{$poisonid = $sinri;}

$userlist = @file($pref['user_file']) or ERROR("unable to open user_file","","LIB3",__FUNCTION__,__LINE__);

#チェック
for ($i=0; $i<count($userlist); $i++){
	list($w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP,) = explode(",",$userlist[$i]);
	if($poisonid == $w_id){#同一ID
		#経験地-敵
		$expup = round(($level - $w_level)/5);if($expup <1){$expup = 1;}if($hit < 1){$expup += 1;}$w_exp += $expup;
		$w_log = ($w_log . "<font color=\"yellow\"><b>$hour:$min:$sec $f_name $l_name（$cl $sex$no号）吃了自己下的药，菊花一紧，于是死于菊爆。【剩余$mem人】</b></font><br>");
		$w_kill++;$w_bid = $id;SAVE2;break;
	}
}

$log = ($log . "<font color=\"lime\"><b>$w_f_name $w_l_name『$w_msg』</b></font><br>");

$b_limit = ($pref['battle_limit']) + 1;

if(($mem == 1) && ($w_sts != "NPC") && ($ar > $b_limit)){$w_inf = ($w_inf . "胜");}

#死亡ログ
LOGSAVE("DEATH1");
$w_death = $deth;

$bid = $w_id;

SAVE();

}else{
#	&ERROR("Internal Server Error","Player is Alive","LIB3-POISONDEAD");
}
}
#=================#
# ■ 口癖変更処理 #
#=================#
function WINCHG(){
global $in;

global $msg,$dmes,$com,$log;
$msg = $in['Message'];
$dmes = $in['Message2'];
$com = $in['Comment'];
$log = ($log . "变更口癖。<br>");
SAVE();
$in['Command'] = "MAIN";
}
#===================#
# ■ チーム変更処理 #
#===================#
function TEAM(){
global $in,$pref;

global $teamID,$teamPass,$log,$teamold,$sta;

$teamold = $teamID;
if(strlen($in['teamID2']) > 24){$log = ($log . "组名太长了。（最多全角12字）<BR>"); return;}
if(strlen($in['teamPass2']) > 24){$log = ($log . "入组密码太长了。（最多全角12字）<BR>"); return;}
if(preg_match("/\_|\,|\;|\<|\>|\(|\)|&|\/|\./",$in['teamID2'])){$log = ($log . "组名出现了禁止事项的文字<BR>"); return;}
if(preg_match("/\_|\,|\;|\<|\>|\(|\)|&|\/|\./",$in['teamPass2'])){$log = ($log . "入组密码出现了禁止事项的文字<BR>"); return;}

if((($in['teamID2'] == $teamID))&&(($in['teamPass2'] == "")||($in['teamPass2'] == "就这样不再变更"))){$in['Command'] = "MAIN";return;}
elseif((($in['teamID2'] == "无")&&($in['teamPass2'] == "无"))||(($in['teamID2'] == "『无』")&&($in['teamPass2'] == "『无』"))){
	if($sta <= $pref['team_sta']){
		$log = ($log . "没有退出小组所必要的体力。<br>");
		return;
	}elseif($teamID != "无"){
		$sta -= $pref['team_sta'];
		$teamID = '无';
		$teamPass = '无';
		LOGSAVE("G-DATT");
		$log = ($log . "从小组{$teamold}退出了。<br>");
	}else{
		$log = ($log . "没有所属小组。<br>");
		return;
	}
}elseif(($in['teamID2'] == "")||($in['teamPass2'] == "")||($in['teamID2'] == "无")||($in['teamPass2'] == "无")||($in['teamID2'] == "『无』")||($in['teamPass2'] == "『无』")){
	ERROR("退出小组。<BR>组名与入组密码都没有输入。<br>","No Grop Name Entered","LIB3",__FUNCTION__,__LINE__);
}elseif($teamID != $in['teamID2']){
	if($teamID != "无"){
		ERROR("在退出小组之前，请先创建或加入一个小组","Need to get out before get in","LIB3",__FUNCTION__,__LINE__);
	}elseif(($in['teamPass2'] == "就这样不再变动")||($in['teamPass2'] == "")){
		ERROR("请输入密码","No Password has been entered","BATTLE-TEAM");
	}elseif($in['teamPass2'] == $in['teamID2']){
		ERROR("组名与入组密码相同","Group Name is same as Pass","BATTLE-TEAM");
	}elseif((preg_match("/$in[teamID2]/",$in['teamPass2']))||(preg_match("/teamPass2/",$in['teamID2']))){
		ERROR("组名与入组密码相似","Group Name and Pass is similar","BATTLE-TEAM");
	}
	#get User file
	$userlist = @file($pref['user_file']) or ERROR("unable to open user_file","","LIB3",__FUNCTION__,__LINE__);
	#Same Name and ID check
	$ng = 0;
	foreach ($userlist as $usrlst){
		list($w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP,) = explode(",", $usrlst);
		if(($in['teamID2'] == $w_teamID)&&(($in['teamPass2'] != $w_teamPass)&&($w_sts != "菊爆"))){
			ERROR("同一小组名存在，或是密码不正确。","Same Group Exists or Pass miss-match","LIB3",__FUNCTION__,__LINE__);
		}elseif(($in['teamID2'] == $w_teamID)&&($in['teamPass2'] == $w_teamPass)){
			$ng++;
		}
	}
	if($ng == 0){
		LOGSAVE("G-JOIN");
		$teamID = $in['teamID2'];
		$teamPass = $in['teamPass2'];
		$log = ($log . "小组 {$teamID} 组成了。<br>");
	}elseif($ng >= $pref['team_max']){
		ERROR("小组的最大人数为{$pref['team_max']}人","Max Group Number","BATTLE-TEAM","LIB3",__FUNCTION__,__LINE__);
	}else{
		LOGSAVE("G-KANYU");
		$teamID = $in['teamID2'];
		$teamPass = $in['teamPass2'];
		$log = ($log . "加入 {$teamID} 小组了。<br>");
	}
}

SAVE();

$in['Command'] = "MAIN";
}
#=================#
# ■ 応急処置処理 #
#=================#
function OUKYU(){
global $in,$pref;

global $sta,$log,$inf;
if($sta <= $pref['okyu_sta']){$log .= '体力不足…不能做应急处理啊…';return;}

$wk = $in['Command'];
$wk = str_replace('OUK_','',$wk);

if($wk==0){#頭
	$inf = str_replace('头','',$inf);
}elseif($wk==1){#腕
	$inf = str_replace('腕','',$inf);
}elseif($wk==2){#腹部
	$inf = str_replace('腹','',$inf);
}elseif($wk==3){#足
	$inf = str_replace('足','',$inf);
}elseif($wk==4 && $pref['clb'][$in['club']]=='茶道部'){#毒
	$inf = str_replace('毒','',$inf);
	$sta += $pref['okyu_sta'];
}else{
	$log .= '没有应急处理的必要';
	return;
}

$log .= '应急处理完毕。<BR>';
$sta -= $pref['okyu_sta'];
SAVE();
$in['Command'] = 'MAIN';
}
#=================#
# ■ 基本方針変更 #
#=================#
function KOUDOU(){
global $in;

global $tactics,$log;

$wk = $in['Command'];
$wk = preg_replace('/KOU_/','',$wk);
$old_tactics = $tactics;

if	  ($wk == 1){$tactics = '菊爆攻击型态';}
elseif($wk == 2){$tactics = '捂菊防御型态';}
elseif($wk == 3){$tactics = '隐秘尾行型态';}
elseif($wk == 4){$tactics = '偷基摸菊型态';}
elseif($wk == 5){$tactics = '连斗行动';}
else			{$tactics = '通常';}

$log .= '基本方针从 <b>'.$old_tactics.'</b> 变更到 <b>'.$tactics.'</b> 。<BR>';

SAVE();

$in['Command'] = "MAIN";
}
#=================#
# ■ 応戦方針変更 #
#=================#
function OUSEN(){
global $in;

global $ousen,$log;

$wk = $in['Command'];
$wk = preg_replace("/OUS_/","",$wk);
$old_ousen = $ousen;

if	  ($wk == 1){$ousen = '菊爆攻击型态';}
elseif($wk == 2){$ousen = '捂菊防御型态';}
elseif($wk == 3){$ousen = '隐秘尾行型态';}
#elseif($wk == 4){$ousen = '偷基摸菊型态';}
#elseif($wk == 5){$ousen = '连斗行动';}
elseif($wk == 6){$ousen = '专注治疗';}
elseif($wk == 7){$ousen = '逃往姿态';}
else			{$ousen = '通常';}

$log .= '迎战方针从 <b>'.$old_ousen.'</b> 变更到 <b>'.$ousen.'</b> 。<BR>';

SAVE();

$in['Command'] = "MAIN";
}
#=====================#
# ■ 携帯スピーカ使用 #
#=====================#
function SPEAKER(){
global $in,$pref;

for($i=0;$i<5;$i++){
	if(preg_match("/大喇叭/",$item[$i])){
		break;
	}
}

if(!preg_match("/大喇叭/",$item[$i])){ERROR("非法操作。","","LIB3",__FUNCTION__,__LINE__);}

$log .= ' '.$in['speech'].'<BR>';
$log .= ' 想说吗？<BR>';

$gunlog = @file($pref['gun_log_file']) or ERROR("unable to open gun_log_file","","LIB3",__FUNCTION__,__LINE__);
$namae = "$f_name $l_name";
$gunlog[2] = $pref['now'].",".$pref['place'][$pls].",".$namae.",".$in['speech'].",\n";

$handle = @fopen($pref['gun_log_file'],'w') or ERROR("unable to open gun_log_file","","LIB3",__FUNCTION__,__LINE__);
if(!@fwrite($handle,implode('',$gunlog))){ERROR("unable to gun_log_file","","LIB3",__FUNCTION__,__LINE__);}
fclose($handle);

$in['Command'] = "MAIN";

}
#===================#
# ■ ハッキング処理 #
#===================#
function HACKING(){
global $in,$pref;

global $log,$item,$eff,$itai;
global $hit,$sts,$desh,$mem,$f_name,$l_name,$cl,$sex,$no;

$junbi=0;
for ($paso=0; $paso<5; $paso++){
	if(($item[$paso] == '移动PC<>Y')&&($itai[$paso] >= 1)){
		$junbi = 1;
		break;
	}
}

if($in['Command'] != 'SPECIAL' || $in['Command4'] != 'HACK' || !$junbi){ERROR('非法操作。','','LIB3',__FUNCTION__,__LINE__);}

$dice1 = rand(0,99);
$dice2 = rand(0,10);

if(preg_match("/Computer Help Desk/",$pref['clb'][$in['club']]))	{#パソコン部の基本成功率
	$bonus = 15;
}elseif(preg_match("/Computer Task Force/",$pref['clb'][$in['club']])){#パソコン部の基本成功率
	$bonus = 20;
}else{
	$bonus = 10;
}

$kekka = $bonus;

$log .= 'Hacking下试试吧…';

if($dice1 <= $kekka){	 #ハッキング成否判定
	$wk_arealist = @file($pref['area_file']) or ERROR("unable to open area_file","","LIB3",__FUNCTION__,__LINE__);
	list($wk_ar,$wk_hack,$wk_a) = explode(",",$wk_arealist[1]);  #ハッキングフラグ取得
	$wk_hack = 2;
	$wk_arealist[1] = "$wk_ar,$wk_hack,\n";

	$handle = @fopen($pref['area_file'],'w') or ERROR("unable to open area_file","","LIB3",__FUNCTION__,__LINE__);
	if(!@fwrite($handle,implode('',$wk_arealist))){ERROR("unable to area_file","","LIB3",__FUNCTION__,__LINE__);}
	fclose($handle);
	$log .= 'Hacking成功！所有的地区都脱离了绿坝娘的控制！！<BR>';
	LOGSAVE('HACK');
}else{
	$log .= '可是Hacking失败了…<BR>';
}

for($paso=0;$paso<5;$paso++){
	if(($item[$paso] == '移动PC<>Y')&&($itai[$paso] >= 1)){
		break;
	}
}

if($dice1 >= 95){   #バッテリ消耗＆ファンブル時機材破壊
	$item[$paso] = '无'; $eff[$paso] = $itai[$paso] = 0;
	$log .= '什么啊！什么都没有发生嘛…除了机器坏了…<BR>';
	if($dice2 >= 9){  #神様(％ファンブル)時政府により首輪爆破！
		$hit = 0; $sts = "菊爆"; $death = $deth = "Hacking失败 菊花拴提前爆破";$mem--;
		if($mem == 1){
			$handle = @fopen($pref['end_flag_file'],'w');# or ERROR("unable to open end_flag_file","","LIB3",__FUNCTION__,__LINE__);
			if(!@fwrite($handle,"结束\n")){ERROR("unable to end_flag_file","","LIB3",__FUNCTION__,__LINE__);}
			fclose($handle);
		}
		LOGSAVE('DEATH5');

		$gunlog = @file($pref['gun_log_file']) or ERROR('unable to open gun_log_file','','LIB3',__FUNCTION__,__LINE__);
		$gunlog[1] = $pref['now'].','.$pref['place'][$pls].','.$id.",,\n";

		$handle = @fopen($pref['gun_log_file'],'w') or ERROR('unable to open gun_log_file','','LIB3',__FUNCTION__,__LINE__);
		if(!@fwrite($handle,implode('',$gunlog))){ERROR('unable to gun_log_file','','LIB3',__FUNCTION__,__LINE__);}
		fclose($handle);
		$log .= '什么啊！什么都没有发生嘛…除了机器坏了…<br><br>…什么？…菊花拴在振动…！？<BR><BR><font color="red">・・・！！・・・<br><br><b>'.$f_name.' '.$l_name.'（'.$cl.' '.$sex.$no.'号）被菊爆。</b></font><br>';
	}
}else{
	$itai[$paso] --;if($itai[$paso] == 0){
		$log .= '移动PC电力用完了。<BR>';
	}
}

if($eff[$paso] <= 0){
	$item[$paso] = '无';
	$eff[$paso] = 0;
	$itai[$paso] = 0;
}
SAVE();
}
?>