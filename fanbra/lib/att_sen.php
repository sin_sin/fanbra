<?php
#□■□■□■□■□■□■□■□■□■□■□
#■ 	-    BR ATTACK PROGRAM    - 	 ■
#□ 									 □
#■ 		  サブルーチン一覧  		 ■
#□ 									 □
#■ ATTACK1			-		先制攻撃処理 ■
#□■□■□■□■□■□■□■□■□■□■□
#=================#
# ■ 先制攻撃処理 #
#=================#
function ATTACK1(){
global $in,$pref;

global $mhit,$tactics,$id,$kiri,$log,$w_l_name,$w_no,$wep,$w_pls,$w_club,$watt,$att,$atp,$def,$bou,$bdef,$bdef_h,$bdef_a,$bdef_f,$item,$dfp,$atn,$dfn,$pls;
global $mei2,$wtai,$l_name,$wp,$mei,$weps,$weps2,$l_name,$wk;
global $limit,$level,$jyuku1,$jyuku2,$pnt,$hakaiinf2,$kega2,$hakaiinf3,$kega3;
global $wep_2,$watt_2,$wtai_2,$w_inf_2,$exp,$hour,$min,$sec,$f_name,$l_name,$cl,$sexx,$no,$exp,$hissatsu1,$hissatsu2;
global $btai,$hit,$w_wep_2,$w_watt_2,$w_wtai_2,$inf_2,$w_kind,$w_kind2,$bid,$w_bid,$w_teamID,$teamID;

require $pref['LIB_DIR'].'/lib2.php';
$kega2 = $kega3 = $hakaiinf2 = $hakaiinf3 = '';

$i = 0;
$result = 0;
$result2 = 0;
$dice1 = rand(0,100);
$dice2 = rand(0,100);
if(preg_match('/军乐队部|围棋部|基翁公国|足球部|曲棍球部/',$pref['clb'][$in['club']])){$dice3 = rand(0,110);}
else{$dice3 = rand(0,100);}
list($a,$w_kind,$wid) = explode("_",$in['Command']);

$userlist = @file($pref['user_file']) or ERROR('unable to open file user_file','','ATT_SEN',__FUNCTION__,__LINE__);
global $w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item,$w_eff,$w_itai,$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP;
for($i=0;$i<count($userlist);$i++){
	list($w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP,) = explode(",",$userlist[$i]);
	if(IDcrypt($w_id) == $wid){break;}
}
#<---連闘
if(preg_match("/&/",$w_bid)){#分割
	list($w_bid1,$w_bid2) = explode("/&/",$w_bid);
}else{
	$w_bid1 = $w_bid;
	$w_bid2 = '';
}
if((($tactics != "连斗行动")&&($w_bid1 == $id)&&($w_bid2 == $teamID))||($w_hit <= 0)){ERROR("非法接入","rento is not valid","ATT_SEN",__FUNCTION__,__LINE__);}
#連闘--->
BB_CK();#ブラウザバック対処
if($kiri){$log = ($log . "战斗开始！<br>");}else{$log = ($log . "{$w_f_name} {$w_l_name}（{$w_cl} 与 {$w_sex}{$w_no}号）战斗开始！<br>");}

list($w_name,$a) = explode("<>",$wep);
list($w_name2,$w_kind2) = explode("<>",$w_wep);
TACTGET();TACTGET2();#基本行動

#プレイヤー
if(((preg_match("/G|A/",$wep))&&($wtai == 0))||((preg_match("/G|A/",$wep))&&($w_kind == "WB"))){$att_p = (($watt/10) + $att) * $atp;}
else{$att_p = ($watt + $att) * $atp;}
$ball = $def + $bdef + $bdef_h + $bdef_a + $bdef_f;
if(preg_match("/AD/",$item[5])){$ball += $eff[5];}#装飾が防具？
$def_p = $ball * $dfp;
#敵
if((preg_match("/G|A/",$w_wep))&&($w_wtai == 0)){$att_n = (($w_watt/10) + $w_att) * $atn;}
else{$att_n = ($w_watt + $w_att) * $atn;}
$ball2 = $w_def + $w_bdef + $w_bdef_h + $w_bdef_a + $w_bdef_f;
if(preg_match("/AD/",$w_item[5])){$ball2 += $w_eff[5];}#装飾が防具？
$def_n = $ball2 * $dfn;

BLOG_CK();
EN_KAIFUKU();

$in['Command']="BATTLE";

if($w_pls != $pls){
	if($kiri){$log = ($log . "但是,逃跑了！<br>");SAVE();return;}
	else{$log = ($log . "但是、{$w_f_name} {$w_l_name}（{$w_cl} {$w_sex}{$w_no}号）逃走了！<br>");SAVE();return;}
}#既に移動？

if((strlen($in['Dengon']) > 0)&&(!preg_match("/超必杀技|必杀技/",$in['Dengon']))){
	if($kiri){
		$log = ($log . "<font color=\"lime\"><b>不明「".$in['Dengon']."」</b></font><br>");
		$w_log = ($w_log . "<font color=\"lime\"><b>{$hour}:{$min}:{$sec} 不明「".$in['Dengon']."」</b></font><br>");
	}else{
		$log = ($log . "<font color=\"lime\"><b>{$f_name} {$l_name}（{$cl} {$sexx}{$no}号）「".$in['Dengon']."」</b></font><br>");
		$w_log = ($w_log . "<font color=\"lime\"><b>{$hour}:{$min}:{$sec} {$f_name} {$l_name}（{$cl} {$sexx}{$no}号）「".$in['Dengon']."」</b></font><br>");
	}
}

WEPTREAT($w_name,$w_kind,$wtai,$l_name,$w_l_name,"攻击","PC");
if($dice1 < $mei){#攻撃成功
	#攻撃(攻撃力*熟練度)
	$result = $att_p * $wk;
	#放送部特殊
	if((strlen($in['Dengon']) > 0)&&($pref['clb'][$in['club']] == "放送部")){$result *= 1.2;}
	#必杀技
	$hissatsu1 = rand(0,100);
	if($limit >= 100){
		$result *= 1.7;
		$log = ($log . "　<font color=\"red\"><b>限界解除发动！</b></font><br>");
		$hissatsu1 = "(奥义)";
		$limit -= 100;
	}elseif(($level >= 10)&&($jyuku1 >= 200)&&($hissatsu1 < 1)){
		$result *= 1.6;
		$log = ($log . "　<font color=\"red\"><b>秘技发动！</b></font><br>");
		$hissatsu1 = "(秘技)";
	}elseif(($jyuku1 >= 100)&&($hit <= 100)&&($in['Dengon'] == "超必杀技")){
		$in['Dengon'] = '';
		if($limit >= 30){
			if((int)($mhit * 0.9) >= 0){
				$result += (int)($mhit * 0.1);
				$mhit = (int)($mhit * 0.9);
				if($hit > $mhit){$hit = $mhit;}
				$limit -= 30;
				$log = ($log . "　<font color=\"red\"><b>超必杀技发动！</b></font><br>");
				$hissatsu1 = "(超杀)";
			}else{$log = ($log . "　<font color=\"red\">缺少发动超必杀技的必要生命值</font><br>");}
		}else{$log = ($log . "　<font color=\"red\">缺少发动超必杀技的必要限界值</font><br>");}
	}elseif(($jyuku1 >= 40)&&($in['Dengon'] == "必杀技")){
		$in['Dengon'] = '';
		if($limit >= 15){
			if($hit > (int)($mhit * 0.1)){
				$result += (int)($mhit * 0.06);
				$hit -= (int)($mhit * 0.1);
				$limit -= 15;
				$log = ($log . "　<font color=\"red\"><b>必杀技发动！</b></font><br>");
				$hissatsu1 = "(必杀)";
			}else{$log = ($log . "　<font color=\"red\">缺少发动必杀技的必要生命值</font><br>");}
		}else{$log = ($log . "　<font color=\"red\">缺少发动超必杀技的必要限界值</font><br>");}
	}elseif(($level >= 3)&&($jyuku1 >= 20)&&($hissatsu1 < 5)){
		$result *= 1.3;
		$log = ($log . "　<font color=\"red\"><b>爆击！</b></font><br>");
		$hissatsu1 = "(爆击)";
	}else{$hissatsu1 = '';}
	#攻撃(-防御/2+=rand)
	$result -= $def_n;
	$result /= 2;
	$result += rand(0,$result);

	DEFTREAT($w_kind,"NPC");
	$result = (int)($result * $pnt);

	if($result <= 0){$result = 1;}
	if($pref['clb'][$w_club]=='攀岩部' && $result > $w_hit && 1 == rand(1,10)){$result = $w_hit - 1;}
	$log = ($log . "　　<font color=\"red\"><b>{$result}伤害 {$hakaiinf3} {$kega3}</b></font>！<br>");
	$w_hit -= $result;$w_btai--;
	if($w_btai <= 0){$w_bou = "内衣<>DN";$w_bdef=0;$w_btai="∞";}

	$wep = $wep_2;$watt = $watt_2;$wtai = $wtai_2;$w_inf = $w_inf_2;
	#経験値-自分
	$expup = (int)(($w_level - $level)/3);if($expup < 1){$expup = 1;}$exp += $expup;
	#リミット-相手
	if($pref['clb'][$w_club]=='攀岩部'){$div=2.5;}else{$div=3;}
	$w_limitup = (int)(($level - $w_level)/$div);if($w_limitup < 1){$w_limitup = 1;}$w_limit += $w_limitup;
}else{$kega3='';$log = ($log . "　　但是,躲开了！<br>");}

if($w_hit <= 0){DEATH2();}#敵死亡？
elseif(($dice3 < 50)&&(!preg_match("/专注治疗|逃跑姿态/",$w_ousen))){#反撃

	if($weps == $weps2 || $weps2 == 'M'){#距離一緒？

		WEPTREAT($w_name2,$w_kind2, $w_wtai,$w_l_name,$l_name,"反击","NPC");

		if($dice2 < $mei2){#攻撃成功
			#攻撃(攻撃力*熟練度)
			$result2 = $att_n*$wk;
		#必杀技
			$hissatsu2 = rand(0,100);
			if($w_limit >= 100){
				$result2 *= 1.5;
				$w_log = ($w_log . "　<font color=\"red\"><b>限界解除发动！</b></font><br>");
				$hissatsu2 = "(奥义)";
				$w_limit -= 100;
			}elseif(($w_level >= 10)&&($jyuku2 >= 200)&&($hissatsu2 < 1)){
				$result2 *= 1.4;
				$w_log = ($w_log . "　<font color=\"red\"><b>秘技发动！</b></font><br>");
				$hissatsu2 = "(秘技)";
			}elseif(($w_level >= 3)&&($jyuku2 >= 20)&&($hissatsu2 < 4)){
				$result2 *= 1.1;
				$w_log = ($w_log . "　<font color=\"red\"><b>爆击！</b></font><br>");
				$hissatsu2 = "(爆击)";
			}else{$hissatsu2 = '';}
			#攻撃(-防御/2+=rand)
			$result2 -= $def_p;
			$result2 /= 2;
			$result2 += rand(0,$result2);
			$result2 = (int)($result2);

			DEFTREAT($w_kind2,"PC");
			$result2 = (int)($result2 * $pnt);

			if($result2 <= 0){$result2 = 1;}
			if($pref['clb'][$club]=='攀岩部' && $result2 > $hit && 1 == rand(1,10)){$result2 = $hit - 1;}
			$log = ($log . "　　<font color=\"red\"><b>{$result2}伤害 {$kega2}</b></font>！<br>");

			$btai--;$hit -= $result2;

			if($btai <= 0){$bou = "内衣<>DN";$bdef=0;$btai="∞";}

			if($hit <=0){DEATH();}#死亡？
			else{#逃亡
				if($kiri){$log = ($log . "对手逃跑了…。<br>");}
				else{$log = ($log . "$w_l_name 逃跑…。<br>");}
			}
			if($kiri){$w_log = ($w_log . "<font color=\"yellow\"><b>{$hour}:{$min}:{$sec} 战斗：不明 攻{$hissatsu2}:{$result2} 受{$hissatsu1}:{$result} {$hakaiinf2} {$kega3}</b></font><br>");}
			else{$w_log = ($w_log . "<font color=\"yellow\"><b>{$hour}:{$min}:{$sec} 战斗：{$f_name} {$l_name}（{$cl} {$sexx}{$no}号） 攻{$hissatsu2}:{$result2} 受{$hissatsu1}:{$result} {$hakaiinf2} {$kega3}</b></font><br>");}
			$w_wep = $w_wep_2;$w_watt = $w_watt_2;$w_wtai = $w_wtai_2;$inf = $inf_2;
		#経験値-相手
			$expup = (int)(($level - $w_level)/3);if($expup < 1){$expup = 1;}$w_exp += $expup;
		#リミット-自分
			if($pref['clb'][$club]=='攀岩部'){$div=2.5;}else{$div=3;}
			$limitup = (int)(($w_level - $level)/$div);if($limitup < 1){$limitup = 1;}$limit += $limitup;
		}else{
			if($kiri){$w_log = ($w_log . "<font color=\"yellow\"><b>{$hour}:{$min}:{$sec} 战斗：不明 受{$hissatsu1}:{$result} {$kega3}</b></font><br>");}
			else{$w_log = ($w_log . "<font color=\"yellow\"><b>{$hour}:{$min}:{$sec} 战斗：{$f_name} {$l_name}（{$cl} {$sexx}{$no}号） 受{$hissatsu1}:{$result} {$kega3}</b></font><br>");}
			$log = ($log . "　　但是,千钧一发之际躲开了！<br>");
		}
		#武器消耗
		if((preg_match("/G|A/",$w_kind2))&&($w_wtai > 0)){$w_wtai--;if($w_wtai <= 0){$w_wtai = 0;}}#銃・射？
		elseif(preg_match("/C|D/",$w_kind2)){$w_wtai--;if($w_wtai <= 0){$w_wep ="空手<>WP";$w_watt=0;$w_wtai="∞";}}
		elseif((preg_match("/K/",$w_kind2))&&(rand(0,5) == 0)){$w_watt -= rand(1,3);if($w_watt <= 0){$w_wep ="空手<>WP";$w_watt=0;$w_wtai="∞";}}

	}else{
		if($kiri){
			$log = ($log . "对手不能反击！<br>　对手逃了…。<br>");
			$w_log = ($w_log . "<font color=\"yellow\"><b>{$hour}:{$min}:{$sec} 战斗：不明 受{$hissatsu1}:{$result} {$hakaiinf2} {$kega3}</b></font><br>");
		}else{
			$log = ($log . "{$w_l_name} 不能反击！<br>　{$w_l_name} 逃了…。<br>");
			$w_log = ($w_log . "<font color=\"yellow\"><b>{$hour}:{$min}:{$sec} 战斗：{$f_name} {$l_name}（{$cl} {$sexx}{$no}号） 受{$hissatsu1}:{$result} {$hakaiinf2} {$kega3}</b></font><br>");
		}
	}
}else{	#逃亡
	if($kiri){
		$log = ($log . "对手逃了…。<br>");
		$w_log = ($w_log . "<font color=\"yellow\"><b>{$hour}:{$min}:{$sec} 战斗：不明 受{$hissatsu1}:{$result} {$hakaiinf2} {$kega3}</b></font><br>");
	}else{
		$log = ($log . "$w_l_name 逃了…。<br>");
		$w_log = ($w_log . "<font color=\"yellow\"><b>{$hour}:{$min}:{$sec} 战斗：{$f_name} {$l_name}（{$cl} {$sexx}{$no}号） 受{$hissatsu1}:{$result} {$hakaiinf2} {$kega3}</b></font><br>");
	}
	if($w_ousen == '逃跑姿态'){#逃跑姿态自動回避
		$w_pls = RUN_AWAY($w_pls);
		$w_log = ($w_log . "<font color=\"lime\"><b>".$pref['place'][$w_pls]."移动了。</b></font><br>");
	}
}
#NPC自動移動
if(($w_sts == "NPC")&&(preg_match("/大叔/",$w_cl))){#逃跑姿态自動回避
	$w_pls = RUN_AWAY($w_pls);
	$w_log = ($w_log . "<font color=\"lime\"><b>".$pref['place'][$w_pls]."移动</b></font><br>");
}
#武器消耗
if	 ((preg_match("/G|A/",$w_kind))&&($wtai > 0)){$wtai--;if($wtai <= 0){$wtai = 0;}}#銃・射？
elseif(preg_match("/C|D/",$w_kind)){$wtai--;if($wtai <= 0){$wep ="空手<>WP";$watt=0;$wtai="∞";}}
elseif((preg_match("/K/",$w_kind))&&(rand(0,5) == 0)){$watt -= rand(1,3);if($watt <= 0){$wep ="空手<>WP";$watt=0;$wtai="∞";}}

LVUPCHK();

$w_bid = $id;
if(($teamID != '无')&&($teamID != '')){$w_bid .= '&'.$teamID;}
$bid = $w_id;
if(($w_teamID != '无')&&($w_teamID != '')){$bid .= '&'.$w_teamID;}

SAVE();
SAVE2();
}
?>