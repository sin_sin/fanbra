﻿<?
#□■□■□■□■□■□■□■□■□■□■□
#■ 	-    BR STATUS DISPLAY    - 	 ■
#□ 									 □
#■ 		  サブルーチン一覧  		 ■
#□ 									 □
#■ STS			-		ステータス部	 ■
#□■□■□■□■□■□■□■□■□■□■□
#=================#
# ■ ステータス部 #
#=================#
function STS(){
global $in,$pref;

global $endtime,$watt_2;
global $month,$mday,$week,$hour,$min,$levuprem,$up,$bar_exp,$icon_file,$full_name,$condi,$CONDITION,$cln,$kega,$bar_sta,$ball,$bar_hit,$w_name,$b_name,$bar_lim,$money_disp,$b_name_h,$b_name_a,$b_name_f,$b_name_i;
global $log,$MESSENGER,$m_mem;
global $id,$password,$f_name,$l_name,$sex,$cl,$no,$att,$def,$hit,$mhit,$level,$exp,$sta,$wep,$watt,$wtai,$bou,$bdef,$btai,$bou_h,$bdef_h,$btai_h,$bou_f,$bdef_f,$btai_f,$bou_a,$bdef_a,$btai_a,$tactics,$death,$msg,$sts,$pls,$kill,$icon,$item,$eff,$itai,$com,$dmes,$bid,$money,$wp,$wg,$wn,$wc,$wd,$comm,$limit,$bb,$inf,$ousen,$seikaku,$sinri,$item_get,$eff_get,$itai_get,$teamID,$teamPass,$expires;

if((preg_match('/睡觉|治疗|静养/',$sts))&&($in['Command'] == 'MAIN')&&($in['mode'] != 'main')) {
	$up = (int)(($pref['now'] - $endtime) / ($pref['kaifuku_time']));
	if(preg_match('/腹/',$inf)){$up = (int)($up / 2);}
	if($ousen == '专注治疗'){$up = (int)($up * 2);}
	if($sts == '睡觉'){
		$sta += $up;
		if($sta > $pref['maxsta']){$sta = $pref['maxsta'];}#最大値までいくつ
		$log .= '睡觉睡饱了，回复了 '.$up.' 体力。<BR>';
		$sts = '正常';$endtime = 0;SAVE();
	}elseif($sts == '治疗'){
		if($pref['kaifuku_rate'] == 0){$pref['kaifuku_rate'] = 1;}
		$up = (int)($up / $pref['kaifuku_rate']);
		$hit += $up;
		if($hit > $mhit){$hit = $mhit;}#最大値までいくつ
		$log .= '治疗结束，回复了'.$up.' 生命。<BR>';
		$sts = '正常';$endtime = 0;SAVE();
	}elseif($sts == '静养'){
		if($pref['kaifuku_rate'] == 0){$pref['kaifuku_rate'] = 1;}
		$ups = $up;
		$up = (int)($up / $pref['kaifuku_rate']);
		$hit += $up;
		if($hit > $mhit){$hit = $mhit;}#最大値までいくつ
		$sta += $ups;
		if($sta > $pref['maxsta']){$sta = $pref['maxsta'];}#最大値までいくつ
		$log .= '静养的结果，回复了生命 '.$up.'、体力 '.$ups.' 。<BR>';
		$sts = '正常';$endtime = 0;SAVE();
	}
}

#定義(LIB)
definition();
$b_limit = ($pref['battle_limit']) + 1;
$arealisttemp = rtrim($pref['arealist'][5]);
if($pref['MOBILE']){
?><big><font color="red"><b><?=$pref['place'][$pls]?> (<?=$pref['area'][$pls]?>)</b></font></big><br>
<?=$month?>月 <?=$mday?>日 <?=$week?>星期 <?=$hour?>:<?=$min?>分<br>
经验值(到下一等级还需<?=$levuprem?>)：<?=$exp?>/<?=$up?><br>
天气：<?=$pref['weather'][$arealisttemp]?><br>
姓名：<?=$full_name?><br>
状态：<?=preg_replace('/<BR>/','',$condi)?><br>
受伤部位：<?=$kega?><br>
攻击力：<?=$att?>+<?=$watt_2?><br>
体力：<?=$sta?>/<?=$pref['maxsta']?><br>
防御力：<?=$def?>+<?=$ball?><br>
生命：<?=$hit?>/<?=$mhit?><br>
武器：<?=$w_name?>/<?=$watt?>/<?=$wtai?><br>
防具：<?=$b_name?>/<?=$bdef?>/<?=$btai?><br>
基本方针：<?=$tactics?><br>
限界：<?=$limit?>/100<br>
<?
}else{
?>
<center><font color="#FF0000" face="ＭＳ Ｐ上午" size="6"><span id="BR" style="width:100%;filter:blur(add=1,direction=135,strength=9):glow(strength=5,color=gold);font-weight:700;text-decoration:underline"><?=$pref['place'][$pls]?> (<?=$pref['area'][$pls]?>)</span></font></center>
<TABLE align="center">
	<TR><TD colspan="2"><B><FONT color="#ff0000"><?=$pref['links']?></FONT></B></TD></TR>
	<TR>
		<TD valign="top" height="1">
		<TABLE border="1" width="550" height="292" cellspacing="0" cellpadding="0">
			<TR valign="middle"><TD width="70" colspan="1" class="b1"><B>LEV. <?=$level?></B></TD>
				<TD colspan="3" class="b1"><B><?=$month?>月 <?=$mday?>日 星期<?=$week?> <?=$hour?>时<?=$min?>分</B></TD>
				<TD class="b1" width="47"><B>经验值</B></TD>
				<TD class="b3" width="78"><a onmouseover="status='到下一等级还剩<?=$levuprem?>';return true;" onmouseout="status='';return true;" href="javascript:void(0)" title="到下一等级<?=$levuprem?>" style="text-decoration: none"><font color="white"><?=$exp?> / <?=$up?></font><BR><?=$bar_exp?></a></TD>
				<TD class="b1" width="47"><B>天　气</B></TD>
				<TD class="b3" width="79"><?=$pref['weather'][$arealisttemp]?></TD></TR>
			<TR><TD ROWSPAN="4" height="70" class="b3"><IMG src="<?=$pref['imgurl']?>/<?=$icon_file[$icon]?>" border="0" align="middle"></TD>
				<TD width="70" class="b2"><B>姓　名</B></TD>
				<TD width="145" colspan="2" class="b3"><?=$full_name?></TD>
				<TD ROWSPAN="4" height="70" class="b2"><B>状　态</B><?=$condi?></TD>
				<TD ROWSPAN="4" colspan="4" class="b3" height="70"><?=$CONDITION?></TD></TR>
			<TR><TD ROWSPAN="1" class="b2"><B>学号</B></TD>
				<TD ROWSPAN="1" colspan="2" class="b3"><?=$cln?></TD></TR>
			<TR><TD class="b2"><B>组队</B></TD>
				<TD colspan="2" class="b3"><?=$teamID?></TD></TR>
			<TR><TD class="b2"><B>受伤部位</B></TD>
				<TD colspan="2" class="b3"><?=$kega?></TD></TR>
			<TR><TD class="b2"><B>攻击力</B></TD>
				<TD class="b3"><?=$att?>+<?=$watt_2?></TD>
				<TD class="b2" width="70"><B>体　力</B></TD>
				<TD class="b3" width="74"><?=$sta?> / <?=$pref['maxsta']?></TD>
				<TD colspan="4" class="b3"><?=$bar_sta?></TD></TR>
			<TR><TD class="b2"><B>防御力</B></TD>
				<TD class="b3"><?=$def?>+<?=$ball?></TD>
				<TD class="b2"><B>生　命</B></TD>
				<TD class="b3"><?=$hit?> / <?=$mhit?></TD>
				<TD colspan="4" class="b3"><?=$bar_hit?></TD></TR>
			<TR><TD class="b2"><B>武　器</B></TD>
				<TD colspan="3" class="b3"><?=$w_name?></TD>
				<TD colspan="2" class="b3"><?=$watt?></TD>
				<TD colspan="2" class="b3"><?=$wtai?></TD></TR>
			<TR><TD class="b2"><B>防　具</B></TD>
				<TD colspan="3" class="b3"><?=$b_name?></TD>
				<TD colspan="2" class="b3"><?=$bdef?></TD>
				<TD colspan="2" class="b3"><?=$btai?></TD></TR>
			<TR><TD class="b2"><B>基本方针</B></TD>
				<TD class="b3"><?=$tactics?></TD>
				<TD class="b2"><B>限界</B></TD>
				<TD class="b3"><a onmouseover="status='限界 <?=$limit?>';return true;" onmouseout="status='';return true;" href="javascript:void(0)" title="限界 <?=$limit?>"><?=$bar_lim?></a></TD>
				<TD class="b2"><B>熟练度</B></TD>
				<TD colspan="3" class="b3">殴:<?=$wp?> 枪:<?=$wg?> 斩:<?=$wn?> 投:<?=$wc?> 爆:<?=$wd?></TD></TR>
			<TR><TD class="b2"><B>应战方针</B></TD>
				<TD class="b3"><?=$ousen?></TD>
				<TD class="b2"><B>社团</B></TD>
				<TD class="b3" colspan="2"><?=$pref['clb'][$in['club']]?></TD>
				<!--TD class="b2"><B>委员会</B></TD>
				<TD class="b3">准备中<?=$comm?></TD-->
				<TD class="b2"><B>资　金</B></TD>
				<TD class="b3" colspan="2"><?=number_format($money_disp)?> 元</TD></TR>
			<TR><TD class="b3" colspan="8" height="1"><div align="center">
<TABLE border="0" cellspacing="0" cellpadding="0"><TR><TD>
<TABLE border="1" cellspacing="0" cellpadding="0">
  <TR><TD class="b1" colspan="4">装备</TD></TR>
  <TR><TD class="b2">种</TD><TD class="b2">名</TD><TD class="b2">效</TD><TD class="b2">耐</TD></TR>
  <TR><TD class="b3">头</TD><TD class="b3"><?=$b_name_h?></TD><TD class="b3"><?=$bdef_h?></TD><TD class="b3"><?=$btai_h?></TD></TR>
  <TR><TD class="b3">腕</TD><TD class="b3"><?=$b_name_a?></TD><TD class="b3"><?=$bdef_a?></TD><TD class="b3"><?=$btai_a?></TD></TR>
  <TR><TD class="b3">足</TD><TD class="b3"><?=$b_name_f?></TD><TD class="b3"><?=$bdef_f?></TD><TD class="b3"><?=$btai_f?></TD></TR>
  <TR><TD class="b3">饰</TD><TD class="b3"><?=$b_name_i?></TD><TD class="b3"><?=$eff[5]?></TD><TD class="b3"><?=$itai[5]?></TD></TR>
</TABLE>
  </TD><TD width="20"></TD><TD>
<TABLE border="1" cellspacing="0" cellpadding="0">
  <TR><TD class="b1" colspan="4">所持物</TD></TR>
  <TR><TD class="b2">名</TD><TD class="b2">效</TD><TD class="b2">数</TD><TD class="b2">种</TD></TR>
<?
for($i=0;$i<5;$i++){
	$itemtype = '';
	@list($i_name,$i_kind) = explode('<>',$item[$i]);
	if(preg_match('/HH|HD/',$i_kind)){
		$itemtype = '【生命回复】';
	}elseif(preg_match("/SH|SD/",$i_kind)){
		$itemtype = '【体力回复】';
	}elseif($i_kind == 'TN'){
		$itemtype = '【陷阱】';
	}elseif(preg_match('/W/',$i_kind)){
		$itemtype = '【武器：';
		if(preg_match('/G/',$i_kind))	{
			$itemtype .= '枪';
			if(preg_match('/S/',$i_kind))	{$itemtype .= '-消音';}
		}
		if(preg_match('/K/',$i_kind)){$itemtype .= '斩';}
		if(preg_match('/C/',$i_kind)){$itemtype .= '投';}
		if(preg_match('/B/',$i_kind)){$itemtype .= '殴';}
		if(preg_match('/D/',$i_kind)){$itemtype .= '爆';}
		$itemtype .= '】';
	}elseif(preg_match('/D/',$i_kind)){
		$itemtype = '【防具：';
		if(preg_match('/B/',$i_kind)){$itemtype .= '体';}
		if(preg_match('/H/',$i_kind)){$itemtype .= '头';}
		if(preg_match('/F/',$i_kind)){$itemtype .= '足';}
		if(preg_match('/A/',$i_kind)){$itemtype .= '腕';}
		if(preg_match('/K/',$i_kind)){$itemtype .= '-对：斩';}
		$itemtype .= '】';
	}elseif($i_kind == 'R1' || $i_kind == 'R2'){
		$itemtype = '【雷达】';
	}elseif($i_kind == 'Y'){
		if($i_name == '腐烂的滋味酱' || $i_name == '滋味酱') {
			$itemtype = '【滋味酱】';
		}elseif($i_name == '子弹'){
			$itemtype = '【弹】';
		}else {
			$itemtype = '【道具】';
		}
	}elseif($i_kind == 'A'){
		$itemtype = '【装饰品】';
	}elseif($item[$i] == '无'){
		$itemtype = '【无】';
	}else{
		$itemtype = '【不明】';
	}
	if($pref['MOBILE']){
		if($item[$i]!='无'){
			echo $i_name.'/'.$eff[$i].'/'.$itai[$i].'/'.$itemtype.'<br>';
		}
	}else{
		echo '<TR><TD class="b3">'.$i_name.'</TD><TD class="b3">'.$eff[$i].'</TD><TD class="b3">'.$itai[$i].'</TD><TD class="b3"><font color="00ffff">'.$itemtype.'</font></TD></TR>';
	}
}
?>
</TABLE>
</TD></TR></TABLE></div>
			</TD>
		  </TR>
	  </TABLE>
	  </TD>
	  <TD valign="top">
	  <TABLE border="1" width="200" height="100%" cellspacing="0" cellpadding="0">
		<TR height="1"><TD height="20" width="200" class="b1"><B>指令</B></TD></TR>
		<TR>
		<TD align="left" valign="top" width="200" class="b3">
		<FORM METHOD="POST" name="BR" style="MARGIN: 0px;text-align: left;" action="BR.php">
		<INPUT TYPE="HIDDEN" NAME="mode" VALUE="command">
		<INPUT TYPE="HIDDEN" NAME="Id" VALUE="<?=$in['Id']?>">
		<INPUT TYPE="HIDDEN" NAME="Password" VALUE="<?=$in['Password']?>">
<?
		COMMAND();
?>
		</FORM>
		</TD>
		</TR>
	  </TABLE>
	  </TD>
	</TR>
<?
}
if($sts=='优胜;'){
}elseif($pref['MOBILE']){
	echo '<FORM METHOD="POST" name="BR" action="BR.php"><INPUT TYPE="HIDDEN" NAME="mode" VALUE="command"><INPUT TYPE="HIDDEN" NAME="Id" VALUE="'.$in['Id'].'"><INPUT TYPE="HIDDEN" NAME="Password" VALUE="'.$in['Password'].'">';
	COMMAND();
	if($log){
		echo '<br><br>日志<br>'.$log.'<br>';
	}
	echo '</FORM>';
}else{
?>
	<TR>
	  <TD valign="top">
		<TABLE border="1" width="550" height="201" cellspacing="0" cellpadding="0">
		  <TR height="1">
			<TD height="1" width="287" class="b1"><B>日志</B></TD>
			<TD height="1" width="257" class="b1"><B>消息</B></TD></TR>
		  <TR>
			<TD valign="top" class="b3" style="text-align:left;"><?=$log?>&nbsp;</TD>
			<TD valign="top" class="b3" style="text-align:left;"><div style="MARGIN: 0px; OVERFLOW: auto; WORD-BREAK: break-all; WIDTH: 256px; HEIGHT: 180px">
<?
		BR_MES();
?>
			</div></TD>
		  </TR>
		</TABLE>
	  </TD><TD valign="top" height="201">
		<TABLE border="1" cellspacing="0" width="200" height="100%" cellpadding="0">
		  <TR height="1"><TD height="1" class="b1"><B>MSN</B></TD></TR>
		  <TR><TD valign="top" class="b3">
			<form method="POST" name="MSG" style="MARGIN: 0px" action="BR.php">
<?
if($MESSENGER != 1){echo '<INPUT type="hidden" name="Mess">';}else{
?>
			<INPUT size="36" type="text" name="Mess" maxlength="36" ><BR>
			<input type="hidden" name="mode" value="command">
			<input type="hidden" name="Command" value="MESS">
			<INPUT TYPE="HIDDEN" NAME="Id" VALUE="<?=$in['Id']?>">
			<INPUT TYPE="HIDDEN" NAME="Password" VALUE="<?=$in['Password']?>">
			<input type="submit" value="发送消息(最长36个字)"><input type="reset" value="清空"><BR>
<?
	if(!preg_match('/无/',$teamID)){
		if(isset($in['M_Id']) && $in['M_Id'] != 'ALL'){$checked = '';}else{$checked = ' checked';}
		echo '对象：<A onclick="sl_msg(0);" href="javascript:void(0);"><INPUT type="radio" name="M_Id" value="ALL"'.$checked.'>全员</A>';
		if($checked == ''){$checked = ' checked';}else{$checked = '';}
		echo '<A onclick="sl_msg(1);" href="javascript:void(0);"><INPUT type="radio" name="M_Id" value="'.$teamID.'"'.$checked.'>小组</A><BR>';
	}else{
		echo '<INPUT type="hidden" name="M_Id" value="ALL">';
	}
}
?>
			在线：<?=$m_mem?>人<BR>
			</TD></form>
		  </TR>
<?/*<!--		  <TR height="1"><TD height="1" width="200" class="b1"><B>メッセンジャー</B></TD></TR>
		  <TR><TD valign="top" class="b3">　</TD></TR>-->*/?>
		</TABLE>
		</TD>
	</TR>
</TABLE>
<?
}
}

?>