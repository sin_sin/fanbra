<?php
#□■□■□■□■□■□■□■□■□■□■□■□■□■□
#■ 		   -  Winner & Rader Display  -   		 ■
#□ 												 □
#■ 				サブルーチン一覧				 ■
#□ 												 □
#■ ENDING		-	優勝処理						 ■
#□ END_FOOT	-	エンディング画面フッタ			 □
#■ ENDING2		-	通常エンディング				 ■
#□ EX_ENDING	-	プログラム解除エンディング		 □
#■ EX_ENDING2	-	解除キー使用者専用エンディング	 ■
#□ READER		-	レーダー表示部					 □
#■□■□■□■□■□■□■□■□■□■□■□■□■□■
#=============#
# ■ 優勝処理 #
#=============#
function ENDING(){
global $pref;

global $inf,$mem;
$fl = @file($pref['end_flag_file']) or ERROR("unable to open file end_flag_file","","DSP",__FUNCTION__,__LINE__);

if		(preg_match("/胜/",$inf))					{ENDING2();}	#優勝者
elseif	(preg_match("/解/",$inf))					{EX_ENDING2();}	#解除者
elseif	(preg_match("/解除/",$fl[0]))					{EX_ENDING();}	#解除時
elseif	(($mem == 1)&&(preg_match("/结束/",$fl[0])))	{ENDING2();}	#優勝者
else{ERROR("非法访问","no hit","DSP",__FUNCTION__,__LINE__);}

}
#===========================#
# ■ エンディング画面フッタ #
#===========================#
function END_FOOT(){
?>
<CENTER>
<p>
<U><B>CGI-PHP版 製作総指揮</B></U>
</p><p>
2Y -Yuta Yamashita-（<a href="http://www.b-r-u.net/">B-R-U.NET</A>）
</p><p>
<U><B>CGI-PERL版 製作総指揮</B></U>
</p><p>
原作者:tanatos
</p><p>
Special Thanks
</p><p>
神谷栄和氏（<a href="http://www.geocities.co.jp/Bookend/5696/index.html">読む前のバトロワ</A>）
</p><p>
箕輪氏（<a href="http://www01.u-page.so-net.ne.jp/zb3/t-c/TC.html">心の扉</A>）
</p><p>
くらげ＆けるぷ氏（<a href="http://homepage1.nifty.com/kurage-ya/">くらげ屋</A>）
</p><p>
<U><B>汉化版 制作总指挥</B></U>
</p><p>
Negimaster sin_sin
<U><B>汉化版 全员</B></U>
</p>Patch.S 诚君MK-2 滋味酱 LCK 蓝蓝路 iTEM Riatre 和饭否基合体全体群众<p>
</p>
<p><A href="index.php"><B><FONT color="#ff0000" size="+2">回到主页面</FONT></B></A></P>
</CENTER>
<?
FOOTER();
UNLOCK();
exit;
}

#=====================#
# ■ 通常エンディング #
#=====================#
function ENDING2(){
global $sex,$no,$f_name,$l_name;
HEAD();

?>
<font color="#FF0000" face="ＭＳ Ｐ明朝" size="6"><span id="BR" style="width:100%;filter:blur(add=1,direction=135,strength=9):glow(strength=5,color=gold); font-weight:700; text-decoration:underline">優勝者決定</span></font>
<p>
	突然警报响起，随后绿坝国歌开始演奏。<BR>
	然后，听到了本不该听到的声音。
</p><p>
「恭喜你。你获得了优胜。老师真的为你感到菊花痒痒的哟。<BR>
马上就去献给你所以洗干净扑床等着吧！」
</p><p>
到现在、到最后也没被菊爆的只剩下一个了么？<BR>
脑海中回响的新班主任的娇蛮声音。<BR>
扭曲的不是世界，而是我么？<BR>
难道其实还有生还者，现在的播送是新班主任的阴谋么？<BR>
确实，这个程序是给政府要人遴选好男人的计划。<BR>
然而当我听到这段广播之时脑中紧张的弦已然断裂。<BR>
意识开始回归深沉的海洋。。。<BR>
这几天，我都完全没有睡呢。。。<BR>
…<BR>
…<BR>
…这是梦中的根源么，我学着旁边的两仪式脱光了漂浮在虚空当中<BR>
眼前是菊花一片片凋落的同学。<BR>
「不是叫你轻一点了嘛！一来劲跟头熊似的！」<BR>
身后是失意体前屈的朋友。<BR>
「以后……洗白白了再来！」<BR>
右手侧的黑暗中传来嘶嚎。<BR>
「我诅咒你菊爆三世 鬼畜万年！」
</p><p>
我颤抖着、抽搐着让空气通过声带嘶吼而出。<BR>
「我不是故意的！我们都是碰友不是吗！只是、只是…我怕疼啊」<BR>
一切再次回归虚无…<BR>
…<BR>
…<BR>
身体感觉到晃动，我半梦半醒地睁开了双眼。<BR>
右手侧的座椅上坐着阿部派遣的好男人。现在好像是在护送军车内。<BR>
膝盖上有袋豆浆。<BR>
旁边的纸条上写着『祝贺你成为好男人！By 阿部』。
</p><p>
</p><p>
<DIV align="right">
Now，"1 student called spring brother remaining".<BR>
Surely 1 is pure man.<BR>
So OGC is never end.
</DIV>
</p><p>
<HR>
</p>
<?
END_FOOT();

}

#===============================#
# ■ プログラム解除エンディング #
#===============================#
function EX_ENDING(){

HEAD();

?>
<font color="#FF0000" face="ＭＳ Ｐ明朝" size="6"><span id="BR" style="width:100%;filter:blur(add=1,direction=135,strength=9):glow(strength=5,color=gold); font-weight:700; text-decoration:underline">プログラム緊急停止</span></font>
<p>
突然警报声穿贯耳膜。<br>
已经决定优胜者了么…？不对,明明还有不少同学的菊花很安全的…。<br>
正这么想着,听到了熟识的同学的呐喊。<br>
「程序已经被绿坝和谐了！再也不用爆菊了！！」
</p><p>
无法相信。<br>
能从这个恶魔般的程序中脱逃。但是我们现在该做些什么呢？<br>
和同学们一起向好男人努力么？<br>
还是现在回老家结婚呢？<br>
不管怎样先回家看看久违的小绿坝？<br>
没有思考的空暇了。闭锁空间的解除在今晚0:00就会恢复原状。<br>
除此之外,在这里磨磨蹭蹭的话会被程序执行本部的滋味发现然后菊爆的吧。<br>
「………」<br>
不管怎样先逃出去。<br>
把武器检查完毕,深呼吸……
</p><p>
逃出初音岛之后。<br>
菊花里的金属便在不断的强调着他的存在感,我终归在逃出来之后也没想到取出的办法。<br>
这个游戏毁灭了我的『希望』。但是他它给了我面对新生活的『希望』!<br>
这时,栓状物突然发出了刺耳的蜂鸣声,随着脊髓感到一阵恶寒袭来！<br>
不是……已经解除程序了么！！
</p><p>
<br>
……那里开始喷出白色气体,什么情况!?程序又启动了么?!<br>
一瞬间菊门一紧……<br>
我在意识的最后一刻，<br>
听到了栓住落地的声音，便沉沉睡去。
</p><p>
</p><HR><p>
<?

END_FOOT();
}

#===================================#
# ■ 解除キー使用者専用エンディング #
#===================================#
function EX_ENDING2(){
HEAD();
?>
<font color="#FF0000" face="ＭＳ Ｐ明朝" size="6"><span id="BR" style="width:100%;filter:blur(add=1,direction=135,strength=9):glow(strength=5,color=gold); font-weight:700; text-decoration:underline">プログラム緊急停止</span></font>

<CENTER>
<p>
「呼…哈……」<br>
我吐了口烟圈，看了看旁边咬着床单低泣垂泪的滋味…披上了挂在衣架上的风衣。<br>
他…他只是被宫小路瑞穗这个伪娘拖进了名为纯爱实为鬼畜的漩涡，迷失了自己的下半身而已。<br>
同样是身不由己，同样是以暴力作决断。<br>
在这无尽的轮回中，阿部渐渐的话渐渐感化了我。<br>
「滋味，做一个好阿部，一要温柔体贴的心，二要足够强壮力量去守护他人…你…」。<br>
「………」<br>
滋味只是忍着泪水，攥紧了我了衣角。<br>
我松开滋味的手，静静的启动了绿坝娘系统<br>
绿坝娘吮吸了些滋味酱之后…强忍着红潮结束了身份认证，解除了爆炸程序<br>
终于，大家的菊花可以自由了…。
</p><p>
但是，栓住发出了刺耳的蜂鸣，我惊异的看向滋味，只得到这就是解除程序详细也不太清楚的回答。<br>
然后，强大的冲击差点夺走了我的意识，我向前飞出趴到在地，只见栓柱向阿部的热狗般插入地板，居然爆炸解除了还这么乱来。<br>
「我也是第一次亲眼看见，是瑞穗骗我说这样大家就都能得到快乐的！！」
滋味红着脸冲着我如此喊道。<br>
</p><p>
虽然我早有预想，不过我只能揉揉菊花趴在那里尴尬的回以微笑。<br>
还真是不得了的办法呢。<br>
本以为爆炸解除了就万事大吉没想到还有喷射程序…。<br>
而我在意地另一件事情就是那个自从第一次见面就再也不曾得见的人…。
</p><p>
「宫小路瑞穗，你，你的菊花在哪里……」<br>
<br>
……故事远没有结束。<br>
</p><p>
</p><HR><p>
<?
END_FOOT();
}
#===================#
# ■ レーダー表示部 #
#===================#
function READER(){
global $in,$pref;
global $item,$wk,$pls,$log;

if(!preg_match("/R/",$item[$wk])){ERROR("不正なアクセスです。","","DSP",__FUNCTION__,__LINE__);}

$userlist = @file($pref['user_file']) or ERROR("unable to open file user_file","","DSP",__FUNCTION__,__LINE__);
$pref['arealist'] = @file($pref['area_file']) or ERROR("unable to open file area_file","","DSP",__FUNCTION__,__LINE__);

list($ar,$hackflg,$a) = explode(",",$pref['arealist'][1]);
$ara = explode(",",$pref['arealist'][4]);

$mem = array();
for ($i=0;$i<count($userlist);$i++){
	list($w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP,) = explode(",",$userlist[$i]);
	for ($j=0;$j<count($pref['area']);$j++){
		if(($w_pls == $j)&&($w_hit > 0)){
			if(!isset($mem[$j])){$mem[$j] = 1;}
			else{$mem[$j] += 1;}
		}
	}
}

if(preg_match("/<>R2/",$item[$wk])){
	for ($j=0;$j<count($pref['area']);$j++){
		if($j == $pls){$mem[$j] = "<FONT color=\"#ff0000\"><b>" . $mem[$j] . "<b></FONT>";}
		elseif(!isset($mem[$j])){$mem[$j] = "0";}
	}
}else{
	for($j=0;$j<count($pref['area']);$j++){
		if($j == $pls){$wk = $mem[$j];$mem[$j] = "<FONT color=\"#ff0000\"><b>$wk<b></FONT>";}
		else{$mem[$j] = "　";}
	}
}

if($hackflg == 0){for ($j=0;$j<$ar;$j++){$mem[$ara[$j]] = "<FONT color=\"#ff0000\"><b>×<b></FONT>";}}


print <<<_HERE_
<!--/TD></TR><TR><TD height="20%"-->
	<center><font color="#FF0000" face="ＭＳ Ｐ明朝" size="6"><span id="BR" style="width:100%;filter:blur(add=1,direction=135,strength=9):glow(strength=5,color=gold);font-weight:700;text-decoration:underline">{$pref['place'][$pls]} ({$pref['area'][$pls]})</span></font></center>
<!--/TD></TR><TR><TD height="0%"-->
<TABLE align="center">
 <TR><TD colspan="2"><B><FONT color="#ff0000">{$pref['links']}</FONT></B></TD></TR>
 <TR>
	<TD valign="top">
	  <TABLE border="1" width="550" height="293" cellspacing="0" cellpadding="0">
		<TR><TD class="b1">　</TD>
			<TD class="b1">01</TD>
			<TD class="b1">02</TD>
			<TD class="b1">03</TD>
			<TD class="b1">04</TD>
			<TD class="b1">05</TD>
			<TD class="b1">06</TD>
			<TD class="b1">07</TD>
			<TD class="b1">08</TD>
			<TD class="b1">09</TD>
			<TD class="b1">10</TD></TR>
		<TR><TD class="b1">A</TD>
			<TD class="b2">　</TD>
			<TD class="b3">$mem[1]</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD></TR>
		<TR><TD class="b1">B</TD>
			<TD class="b2">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[2]</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD></TR>
		<TR><TD class="b1">C</TD>
			<TD class="b2">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[3]</TD>
			<TD class="b3">$mem[4]</TD>
			<TD class="b3">$mem[5]</TD>
			<TD class="b3">$mem[6]</TD>
			<TD class="b3">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD></TR>
		<TR><TD class="b1">D</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[7]</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[0]</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD></TR>
		<TR><TD class="b1">E</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[8]</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[9]</TD>
			<TD class="b3">$mem[10]</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[11]</TD>
			<TD class="b3">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD></TR>
		<TR><TD class="b1">F</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[12]</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[13]</TD>
			<TD class="b2">　</TD></TR>
		<TR><TD class="b1">G</TD>
			<TD class="b2">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[14]</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[15]</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b2">　</TD></TR>
		<TR><TD class="b1">H</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[16]</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[17]</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b2">　</TD></TR>
		<TR><TD class="b1">I</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[18]</TD>
			<TD class="b3">$mem[19]</TD>
			<TD class="b2">　</TD>
			<TD class="b3">　</TD>
			<TD class="b3">$mem[20]</TD></TR>
		<TR><TD class="b1">J</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b3">$mem[21]</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD>
			<TD class="b2">　</TD></TR>
	  </TABLE>
	  </TD>
	  <TD valign="top">
	  <TABLE border="1" width="200" height="293" cellspacing="0" cellpadding="0">
		<TR height="1"><TD height="20" width="200" class="b1"><B>コマンド</B></TD></TR>
		<TR>
		<TD align="left" valign="top" width="200" class="b3">
		<FORM METHOD="POST" name="BR" style="MARGIN: 0px">
		<INPUT TYPE="HIDDEN" NAME="mode" VALUE="command">
		<INPUT TYPE="HIDDEN" NAME="Id" VALUE="$in[Id]">
		<INPUT TYPE="HIDDEN" NAME="Password" VALUE="$in[Password]">
		<p style="text-align: left">
_HERE_;
require $pref['LIB_DIR']."/dsp_cmd.php";
		COMMAND();
print <<<_HERE_
		</p>
		</FORM>
		</TD>
		</TR>
	  </TABLE>
	  </TD>
	</TR>
	
	<TR>
	  <TD valign="top">
		<TABLE border="1" width="550" height="201" cellspacing="0" cellpadding="0">
		  <TR height="1">
			<TD height="1" width="287" class="b1"><B>日志</B></TD>
			<TD height="1" width="257" class="b1"><B>信息</B></TD></TR>
		  <TR>
			<TD valign="top" class="b3"><p style="text-align: left">使用雷达探测。<BR><BR>数字：区域里人数<BR>赤数字：自己所在区域里人数<br>{$log}</p></TD>
			<TD valign="top" class="b3"><p style="text-align: left">
_HERE_;
		BR_MES();
print <<<_HERE_
			</p>
			</TD>
		  </TR>
		</TABLE>
	  </TD><TD valign="top" height="201">
		<TABLE border="1" cellspacing="0" width="200" height="201" cellpadding="0">
		  <TR height="1"><TD height="1" class="b1"><B>メッセンジャー</B></TD></TR>
		  <TR>
		  	<TD valign="top" class="b3">　</TD>
		  </TR>
		</TABLE>
</TD></TR></TABLE>
_HERE_;
$mflg="ON";#ステータス非表示

}
?>