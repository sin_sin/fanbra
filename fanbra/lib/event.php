<?php
#□■□■□■□■□■□■□■□■□■□■□
#■ 	 -    BR EVENT DISPLAY    - 	 ■
#□ 									 □
#■ 		  サブルーチン一覧  		 ■
#□ 									 □
#■ EVENT		-		イベント処理	 ■
#■□■□■□■□■□■□■□■□■□■□■
#=================#
# ■ イベント処理 #
#=================#
function EVENT(){
global $pref,$in;

global $hit,$sta,$sts,$pls,$log,$inf,$limit,$chksts,$money,$level;
$old_hit = $hit;
$dice = rand(1,5);//確立
$dice2 = rand(5,(8+round($level/5)));//ダメージ
$in['Command'] = 'MAIN';
if(rand(1,5) > 1){return;}

if($pls == 0){#分校

}elseif($pls == 1){#北の岬
	$log .= '忽然间仰望蓝天,居然有天上人袭来！<BR>';
	if($dice == 2){
		$log .= '　被天上人袭击,头部受伤了！<BR>';
		$inf = str_replace('头','',$inf);
		$inf .= '头';
	}elseif($dice == 3){
		$log .= '　被天上人击中,造成了<font color="red"><b>'.$dice2.'的</b></font> 伤害！<BR>';
		$hit-=$dice2;
	}else{
		$log .= '　呼,总算是击退他们了…<BR>';
	}
	$chksts='OK';
}elseif($pls == 2){#北村住宅街
}elseif($pls == 3){#北村役場
}elseif($pls == 4){#郵便局
}elseif($pls == 5){#消防署
}elseif($pls == 6){#観音堂
	$log .= '是喵喵的赛钱箱！甚好,里面放了什么呢？<BR>';
	if($dice == 2){
		$log .= '　好痛！被锈钉子刺到了,手腕受伤了！<BR>';
		$inf = str_replace('腕','',$inf);
		$inf .= '腕';
	}elseif($dice == 3){
		$log .= '　好痛！被锈钉子刺到手腕了,受到<font color="red"><b>'.$dice2.'</b></font> 的伤害！<BR>';
		$hit-=$dice2;
	}elseif($dice == 4 && $dice2 == 5){
		$money_found=round(1,20);//100円～2000円
		$log .= '　蓝～蓝～路～！'.($money_found*$pref['money_base']).'元就放在里面没人要。<br>';
		$money += $money_found;
	}else{
		$log .= '　什么也没有…。<BR>';
	}
	$chksts='OK';
}elseif($pls == 7){#清水池
	$log .= '不好，脚滑了一下！<BR>';
	if($dice <= 3){
		$dice2 += 10;
		if($sta <= $dice2){
			$dice2 = $sta;$dice2--;
		}
		$sta-=$dice2;
		$log .= '　虽然掉到池子里了，不过总算还是爬了上来！<BR>体力损失 <font color="red"><b>'.$dice2.'</b></font> 了！<BR>';
	}else{
		$log .= '　呼，总算小心了点没掉下去…。<BR>';
	}
	$chksts='OK';
}elseif($pls == 8){#西村神社
}elseif($pls == 9){#ホテル跡
}elseif($pls == 10){#山岳地帯
	$log .= '不好，有石块掉下来了！<BR>';
	if($dice == 2){
		$log .= '　虽然总算没什么大碍，不过足部被砸伤了！<BR>';
		$inf = str_replace('足','',$inf);
		$inf .= '足';
	}elseif($dice == 3){
		$log .= '　因为落石受到<font color="red"><b>'.$dice2.'</b></font> 伤害了！<BR>';
		$hit-=$dice2;
	}else{
		$log .= '　呼，总算是避开了…。<BR>';
	}
	$chksts='OK';
}elseif($pls == 11){#トンネル
	$log .= '忽然间看相脚下,地上居然有钉子！<BR>';
	if($dice == 2){
		$log .= '　被锈钉子刺到了,足部受伤了！<BR>';
		$inf = str_replace('足','',$inf);
		$inf .= '足';
	}elseif($dice == 3){
		$log .= '　被锈钉子刺到了,受到<font color="red"><b>'.$dice2.'</b></font> 伤害！<BR>';
		$hit-=$dice2;
	}else{
		$log .= '　好险，不小心走路的话…。<BR>';
	}
	$chksts='OK';
}elseif($pls == 12){#西村住宅街
	$log .= '忽然间仰望蓝天,居然有天上人袭来！<BR>';
	if($dice == 2){
		$log .= '　被天上人袭击,头部受伤了！<BR>';
		$inf = str_replace('头','',$inf);
		$inf .= '头';
	}elseif($dice == 3){
		$log .= '　被天上人击中,造成了<font color="red"><b>'.$dice2.'</b></font> 伤害！<BR>';
		$hit-=$dice2;
	}else{
		$log .= '　呼,总算是击退他们了…。<BR>';
	}
	$chksts='OK';
}elseif($pls == 13){#寺
	$log .= '是喵喵的赛钱箱！甚好,里面放了什么呢？<BR>';
	if($dice == 2){
		$log .= '　好痛！被锈钉子刺到了,手腕受伤了！<BR>';
		$inf = str_replace('腕','',$inf);
		$inf .= '腕';
	}elseif($dice == 3){
		$log .= '　好痛！被锈钉子刺到了,受到<font color="red"><b>'.$dice2.'</b></font> 伤害！<BR>';
		$hit-=$dice2;
	}elseif($dice == 4 && $dice2 == 5){
		$money_found=round(1,20);//100円～2000円
		$log .= '　蓝～蓝～路～！'.($money_found*$pref['money_base']).'元就放在里面没人要。<br>';
		$money += $money_found;
	}else{
		$log .= '　什么也没有…。<BR>';
	}
	$chksts='OK';
}elseif($pls == 14){#廃校
}elseif($pls == 15){#南村神社
}elseif($pls == 16){#森林地帯
	$log .= '突然野生的触手向这里袭来！<BR>';
	if($dice == 2){
		$log .= '　手腕被割到，负伤了！<BR>';
		$inf = str_replace('腕','',$inf);
		$inf .= '腕';
	}elseif($dice == 3){
		$log .= '　被触手袭击，受到<font color="red"><b>'.$dice2.'</b></font> 伤害！<BR>';
		$hit-=$dice2;
	}else{
		$log .= '　呼，总算是将其击退了…。<BR>';
	}
	$chksts='OK';
}elseif($pls == 17){#源二郎池
	$log .= '不好，脚滑了一下！<BR>';
	if($dice <= 3){
		$dice2 += 10;
		if($sta <= $dice2){
			$dice2 = $sta;$dice2--;
		}
		$sta-=$dice2;
		$log .= '　虽然掉到池子里了，不过总算还是爬了上来！<BR>体力损失 <font color="red"><b>'.$dice2.'</b></font> 了！<BR>';
	}else{
		$log .= '　呼，总算小心了点没掉下去…。<BR>';
	}
	$chksts='OK';
}elseif($pls == 18){#南村住宅街
	$log .= '忽然间仰望蓝天,居然有天上人袭来！<BR>';
	if($dice == 2){
		$log .= '　被天上人袭击,头部受伤了！<BR>';
		$inf = str_replace('头','',$inf);
		$inf .= '头';
	}elseif($dice == 3){
		$log .= '　被天上人击中,造成了<font color="red"><b>'.$dice2.'</b></font> 伤害！<BR>';
		$hit-=$dice2;
	}else{
		$log .= '　呼,总算是击退他们了…。<BR>';
	}
	$chksts='OK';
}elseif($pls == 19){#診療所
	$log .= '好痛！注射针啊…<BR>';
	if($dice == 2){
		$log .= '　腕负伤了！<BR>';
		$inf = str_replace('腕','',$inf);
		$inf .= '腕';
	}elseif($dice == 3){
		$log .= '　受到<font color="red"><b>'.$dice2.'</b></font> 伤害！<BR>';
		$hit-=$dice2;
	}else{
		$log .= '　没有受伤嘛…。<BR>';
	}
	$chksts='OK';
}elseif($pls == 20){#灯台
}elseif($pls == 21){#南の岬
}

#死亡処理
if($hit <= 0){
	global $f_name,$l_name,$cl,$sex,$no;
	$hit = $mhit = 0;
	$log .= '<font color="red"><b>'.$f_name.' '.$l_name.'（'.$cl.' '.$sex.$no.'号）已经被菊爆。</b></font><br>';

	#死亡ログ
	LOGSAVE('DEATH');
	$in['Command'] = 'EVENT';
}

if($old_hit != $hit){$limit++;}
}
?>