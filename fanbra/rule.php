<?php
require 'pref.php';
HEAD();
?>
<script type="text/javascript">
<!-- 
offY = 1;
posY = offY;
function floatMenu(){
	sy = document.body.scrollTop;
	document.menu.style.height = posY;
	movY = sy-posY+offY;
	cY = movY/1.5;
	posY += cY+10;
}
setInterval('floatMenu()',100);
// -->
</script>
<div style="position:absolute;z-index:2;">
<div style="float:left;height:100%;padding-left:24px;">
<img src="./img/spacer.gif" height="10" width="1" name="menu" id="menu" />
<center><b><font size="4">メニュー一覧</font></b></center><br>
<a href="#■勝利条件">勝利条件</a><br>
<a href="#■参加可能数">参加可能数</a><br>
<a href="#■コマンド説明">コマンド説明</a><br>
<a href="#■ステータス説明">ステータス説明</a><br>
<a href="#■アイテム説明">アイテム説明</a><br>
<a href="#■アイテム合成">アイテム合成</a><br>
<a href="#■クラブ説明">クラブ説明</a><br>
<a href="#■戦闘説明">戦闘説明</a><br>
<a href="#■怪我、状態">怪我、状態</a><br>
<a href="#■基本方針">基本方針</a><br>
<a href="#■応戦方針">応戦方針</a><br>
<a href="#■熟練度・熟練レベル">熟練度・熟練レベル</a><br>
<a href="#■リミット・必殺技">リミット・必殺技</a><br>
<a href="#■禁止エリア">禁止エリア</a><br>
<a href="#■天気">天気</a><br>
<a href="#■武器">武器</a><br>
<a href="#■罠・毒">罠・毒</a><br>
<a href="#■注意事項">注意事項・Special Thanx</a><br>
<center><B><a href="index.php">HOME</A></B></center>
</div>
</div>

<div align="left" style="padding-left:192px;position:relative;z-index:1;">
<FONT color="#ff0000"><B><a NAME="■勝利条件">■勝利条件</a></B></FONT><BR><BR>
　　一週間の期間で他の参加者全員を倒し、最後まで生き残る。<BR><BR>
<FONT color="#ff0000"><B><a name="■参加可能数">■参加可能数</a></B></FONT><BR><BR>
　　男子、女子共に<?=$pref['manmax']?>名を1クラスとし、最大<?=count($pref['clas'])?>クラスまで登録可能。<BR>
　　死亡者もクラスメイトとして計算されるので注意する事。<BR>
<BR>
<FONT color="#ff0000"><B><a name="■コマンド説明">■コマンド説明</a></B></FONT><BR>
<BR>
<table border="0" cellpadding="0" cellspacing="0"><TR><TD width="20">　</TD>
  <TD width="596">
<table border="1" cellpadding="0" cellspacing="0" width="624">
  <tr>
	<td width="169" align="center" colspan="2">『移動』</td>
	<td width="540">現在のエリアを離れ、別のエリアに移動する。<B><FONT color="#00ff00">（スタミナ10前後消費）</B></font></td>
  </tr><tr>
	<td width="169" align="center" colspan="2">『探索』</td>
	<td width="540">現在のエリアを探索する。<br>イベントや、他のプレイヤーとの戦闘の場合あり。<B><FONT color="#00ff00">（スタミナ20前後消費）</FONT></B></td>
  </tr><tr>
	<td width="32" align="center" rowspan="4">ア<br>イ<br>テ<br>ム</td>
	<td width="137" align="center">『アイテム使用・装備』</td>
	<td width="540">所持アイテムの使用、装備を行う。尚、アイテムは<font color="#00FF00"><b>5つ</b></font>まで所持できる。</td>
  </tr><tr>
	<td width="137" align="center">『アイテム投棄』</td>
	<td width="540">所持アイテムを捨てる。捨てたアイテムはそのエリアに放置される。</td>
  </tr><tr>
	<td width="137" align="center">『アイテム整理』</td>
	<td width="540">同じアイテムを纏めることができる。</td>
  </tr><tr>
	<td width="137" align="center">『アイテム合成』</td>
	<td width="540">アイテムとアイテムを合成することができる。詳しくはアイテム合成の項目を参照。</td>
  </tr><tr>
	<td width="32" align="center" rowspan="3">回<br><br>復</td>
	<td width="137" align="center">『治療』</td>
	<td width="540">傷の治療をし、体力を回復する。<br>治療の取消し、コンティニュー時に、治療状態が解除される。<B><FONT color="#00ff00">（<?=($pref['kaifuku_time']*$pref['kaifuku_rate'])?>秒に1ポイント回復）</FONT></B></td>
  </tr><tr>
	<td width="137" align="center">『睡眠』</td>
	<td width="540">睡眠をとり、スタミナを回復する。<br>睡眠の取消し、コンティニュー時に、睡眠状態が解除される。<B><FONT color="#00ff00">（<?=$pref['kaifuku_time']?>秒に1ポイント回復）</FONT></B></td>
  </tr><tr>
	<td width="137" align="center">『静養』</td>
	<td width="540">静養をとり、体力とスタミナ両方を回復する。<br>睡眠の取消し、コンティニュー時に、静養状態が解除される。<B><FONT color="#00ff00"><br>（体力-<?=($pref['kaifuku_time']*$pref['kaifuku_rate'])?>秒に1ポイント回復　スタミナ-<?=$pref['kaifuku_time']?>秒に1ポイント回復）</FONT></B></td>
  </tr><tr>
	<td width="32" align="center" rowspan="8">特<br><br><br>殊</td>
	<td width="137" align="center">『基本方針』</td>
	<td width="540">自分が行動する方針を決める。詳細は基本方針の項目を参照。</td>
  </tr><tr>
	<td width="137" align="center">『応戦方針』</td>
	<td width="540">応戦時の行動方針を決める。詳細は応戦方針の項目を参照。</td>
  </tr><tr>
	<td width="137" align="center">『口癖変更』</td>
	<td width="540">殺害時のコメント、遺言、一言コメントを変更できる。</td>
  </tr><tr>
	<td width="137" align="center">『応急処置』</td>
	<td width="540">負傷箇所を治療することができる。<B><FONT color="#00ff00">（スタミナ<?=$pref['okyu_sta']?>消費）</FONT></B></td>
  </tr><tr>
	<td width="137" align="center">『グループ』</td>
	<td width="540">グループに入ることによって、グループメンバーとの戦闘は行えないが、<br>アイテムを交換や、与えることが可能である。<br>グループの作成、加入、脱退の作業が行える。最高<?=$pref['team_max']?>人まで。</td>
  </tr><tr>
	<td width="137" align="center">『毒見』</td>
	<td width="540">料理部だけが可能なコマンドで、回復アイテムに毒物が混入されていないかを<br>調べることができる。<B><FONT color="#00ff00">（スタミナ<?=$pref['dokumi_sta']?>消費）</FONT></B></td>
  </tr><tr>
	<td width="137" align="center">『毒物混入』</td>
	<td width="540">回復アイテムに毒物を混入することができる。</td>
  </tr><!--tr>
	<td width="137" align="center">『スピーカ使用』</td>
	<td width="540">スピーカーを使うことによって、島全体の参加者にメッセージを伝えることができる。</td>
  </tr--><tr>
	<td width="137" align="center">『ハッキング』</td>
	<td width="540">政府のコンピュータにハッキングし、誤作動を起こし禁止エリアを全部解除することができる。</td>
  </tr><tr>
	<td width="169" align="center" colspan="2">『メッセンジャー』</td>
	<td width="540">現在ログイン中のメンバー数が表示されており、お互いに連絡の取り合いができる。</td>
  </tr>
</table>
</TD></TR></table>
<BR>
<FONT color="#ff0000"><B><a name="■ステータス説明">■ステータス説明</a></B></FONT><BR>
<BR>
<table border="0" cellpadding="0" cellspacing="0" width="574"><TR><TD width="20">　</TD>
  <TD width="554">
<table border="1" cellpadding="0" cellspacing="0" width="554">
  <tr>
	<td>『クラス』</td>
	<td>所属クラス</td>
  </tr><tr>
	<td>『氏名』</td>
	<td>プレイヤー名</td>
  </tr><tr>
	<td>『番号』</td>
	<td>性別と、出席番号</td>
  </tr><tr>
	<td>『クラブ』</td>
	<td>所属しているクラブ。転校時に自動的に配属される。</td>
  </tr><tr>
	<td>『レベル』</td>
	<td>キャラクターのレベル。上昇すると、最大体力、攻撃力、防御力が上昇する。<br>又、スタミナが<font color="#00FF00"><b><?=$pref['lvup_sta']?></b></font>回復する。</td>
  </tr><tr>
	<td>『経験値』</td>
	<td>キャラクターの経験値/次レベルに必要な経験値。一定値に達すると、レベルがアップする。<br>他プレイヤーへの攻撃成功時に獲得できる。<br>2日目以降は一定の経験値がある状態から始める事ができる。<br>これは、2日目以降の者と生存者をある程度、平等にするためである。</td>
  </tr><tr>
	<td>『体力』</td>
	<td>プレイヤーの体力/最大体力。0になるとキャラクターは死亡する。</td>
  </tr><tr>
	<td>『スタミナ』</td>
	<td>移動、探索で減少する。<br>開始時は最大値<font color="#00FF00"><b>100</font></b>で、2日目・3日目に<font color="#00FF00"><b>200</b></font>上がり最大値も最終的には<font color="#00FF00"><b>500</b></font>になる。</td>
  </tr><tr>
	<td>『攻撃力』</td>
	<td>プレイヤーの攻撃力と、武器の攻撃力。</td>
  </tr><tr>
	<td>『武器』</td>
	<td>現在装備している武器。</td>
  </tr><tr>
	<td>『防御力』</td>
	<td>プレイヤーの防御力と、防具の防御力。</td>
  </tr><tr>
	<td>『防具』</td>
	<td>現在装備している防具と、耐久度。<br>ダメージを受けるたびに、耐久度が減り、0になると、防具が壊れる。</font></td>
  </tr><tr>
	<td>『所持品』</td>
	<td>プレイヤーが所持している所持品リスト。最大5つまで所持できる。</td>
  </tr>
</table>
</TD></TR></table>
<br>
<FONT color="#ff0000"><B><a name="■アイテム説明">■アイテム説明</a></B></FONT><BR><BR>
<table border="0" cellpadding="0" cellspacing="0" width="609"><TR><TD width="20">　</TD>
  <TD width="589">
<table border="1" cellpadding="0" cellspacing="0" width="589">
  <tr>
	<td>パン</td>
	<td>食べる事により、体力をある程度回復できる。ゲーム開始時に、政府より4つ支給される。</td>
  </tr><tr>
	<td>水</td>
	<td>飲む事により、スタミナをある程度回復できる。ゲーム開始時に、政府より4つ支給される。</td>
  </tr><tr>
	<td>簡易レーダー</td>
	<td>使用すると、自分のエリアに存在する人数がわかる。但し、誰がいるのかまではわからない。</td>
  </tr><tr>
	<td>レーダー</td>
	<td>使用すると、各エリアに存在する人数がわかる。但し、誰がいるのかまではわからない。</td>
  </tr><tr>
	<td>武器</td>
	<td>ゲーム開始時に、ランダムで支給される。他プレイヤーを倒すと、奪う事が出来る。</td>
  </tr><tr>
	<td>防具</td>
	<td>装備する事により、身を守る事が出来る。種類によって、特定の武器のダメージを半減。<br>但し、苦手とする武器もあり、その際はダメージが倍増する。</td>
  </tr><tr>
	<td>その他</td>
	<td>このゲームには、他にも多数のアイテムが存在する。その効果などは不明である。</td>
  </tr>
</table>
</TD></TR></table>
　　 ※アイテムを最大数所持してる時に、アイテムを発見した場合、その場で持っているアイテムを捨てるか発見したアイテムを捨てるか選択できる。<BR>
<BR>
<FONT color="#ff0000"><B><a name="■アイテム合成">■アイテム合成</a></B></FONT><BR>
<BR>
　　アイテムを合成することによって、それぞれを別々に使うより強い効力のあるアイテムを作成することができる。<br>
　　合成できるアイテムの一覧は以下の通りである。<BR>
<BR>
<table border="0" cellpadding="0" cellspacing="0"><TR><TD width="20">　</TD><TD>
<table border="1" cellpadding="0" cellspacing="0" width="274" height="136">
  <tr>
	<td bgcolor="#FFFF00"><b><font color="#000000">合成素材</font></b></td>
	<td bgcolor="#FFFF00">　</td>
	<td bgcolor="#FFFF00"><b><font size="2" color="#000000">合成結果</font></b></td>
  </tr><tr>
	<td>軽油 + 肥料</td>
	<td>→</td>
	<td>火薬</td>
  </tr><tr>
	<td>ガソリン&nbsp; 空き瓶</td>
	<td>→</td>
	<td>☆火炎瓶☆</td>
  </tr><tr>
	<td>信管 + 火薬</td>
	<td>→</td>
	<td>☆爆弾☆</td>
  </tr><tr>
	<td>火薬 + 導火線</td>
	<td>→</td>
	<td>★ダイナマイト★</td>
  </tr><tr>
	<td>スプレー缶 + ライター</td>
	<td>→</td>
	<td>★簡易火炎放射器★</td>
  </tr><tr>
	<td>携帯電話 + パワーブック</td>
	<td>→</td>
	<td>モバイルPC</td>
  </tr><tr>
	<td>雑炊 + 松茸</td>
	<td>→</td>
	<td>松茸御飯</td>
  </tr><tr>
	<td>カレー + パン</td>
	<td>→</td>
	<td>カレーパン</td>
  </tr><tr>
	<td>あんこ + パン</td>
	<td>→</td>
	<td>あんぱん</td>
  </tr>
</table>
</TD></TR></table>
　　 ※ 毒は中和されます<br>
<BR>
<FONT color="#ff0000"><B><a name="■クラブ説明">■クラブ説明</a></B></FONT><BR>
<BR>
　　各プレイヤーは何かしらのクラブに入部しており、所属しているクラブによって、<BR>
　　ゲーム開始時にステータス変動が起きる。登録時に希望したい部活を選ぶ。<br>
　　ただし、必ずしも希望した部活に入部できるわけではないが、<BR>
　　特殊、シーズン、フル・シーズンと入部確率が上がっていく<BR>
　　確率は1割・3割・6割の順である。く<BR>
　　部活名、ステータス変化・特徴、種類は、以下の通りである。<br>
<BR>
<table border="0" cellpadding="0" cellspacing="0"><TR><TD width="20">　</TD><TD>
<table border="1" cellpadding="0" cellspacing="0">
  <tr>
	<td align="center" bgcolor="#CCCCCC"><font size="3" color="#000000">部活名</font></td>
	<td align="center" bgcolor="#CCCCCC"><font size="3" color="#000000">ステータス変化</font></td>
	<td align="center" bgcolor="#CCCCCC"><font size="3" color="#000000">特徴</font></td>
	<td align="center" bgcolor="#CCCCCC"><font size="3" color="#000000">種類</font></td>
  </tr><tr>
	<td bgcolor="#333333" align="center">空手部</td>
	<td bgcolor="#333333">殴の熟練度が20</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">剣道部</td>
	<td bgcolor="#333333">剣の熟練度が20</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">柔道部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">攻撃されたとき、反撃の確率が少し上がる</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">料理部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">毒見が可能 また、毒物混入時の効果が2倍になる</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">アート部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">発見率向上・先制攻撃向上</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">茶道部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">解毒剤不要 応急処理の毒より解毒できる</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">ロック部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">一定の確率で死亡せずに体力が1でとどまる・リミットが少しだけたまり易い</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">オーケストラ部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">発見率向上・防御率向上</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">ボランティア部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">譲渡時に体力・スタミナが10回復する</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">ブラスバンド部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">遭遇率・回避向上</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">釣り部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">（食料の発見率が向上）</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">コーラス部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">同一エリアのグループとの接触率向上</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">将棋部</td>
	<td bgcolor="#333333">投の熟練度が20</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">福沢諭吉研究会</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">分校・廃校に居る際、防御・攻撃力向上</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">書道部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">命中率・防御力向上</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">囲碁部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">先制攻撃時の防御・回避向上</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">西洋ボード部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">発見率向上</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">ZION</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">回避率向上</td>
	<td bgcolor="#333333" align="center">フル・シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">バスケ部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">グループに所属時攻撃力上昇</td>
	<td bgcolor="#333333" align="center">シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">サッカー部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">移動・探索スタミナ低下・回避率・命中力UP</td>
	<td bgcolor="#333333" align="center">シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">野球部</td>
	<td bgcolor="#333333">投・殴の熟練度が10</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333" align="center">シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">テニス部</td>
	<td bgcolor="#333333">銃の熟練度が20</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333" align="center">シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">水泳部</td>
	<td bgcolor="#333333">投の熟練度が20</td>
	<td bgcolor="#333333">北の岬・南の岬-静養可能 分校・廃校時-防御力低下</td>
	<td bgcolor="#333333" align="center">シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">ラグビー部</td>
	<td bgcolor="#333333">殴の熟練度が20</td>
	<td bgcolor="#333333">攻撃力向上</td>
	<td bgcolor="#333333" align="center">シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">ラクロス部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">（すばやさ)・回避率向上</td>
	<td bgcolor="#333333" align="center">シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">バレー部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">診療所での遭遇率低下</td>
	<td bgcolor="#333333" align="center">シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">クロスカウントリー部</td>
	<td bgcolor="#333333">　</td>
	<td bgcolor="#333333">移動スタミナ低下・防御力低下</td>
	<td bgcolor="#333333" align="center">シーズン</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">放送部</td>
	<td bgcolor="#333333">全熟練度10</td>
	<td bgcolor="#333333">攻撃時に相手にしゃべると攻撃率倍向上(最高で20%増し)</td>
	<td bgcolor="#333333" align="center">特殊</td>
  </tr><tr>
	<td bgcolor="#333333" align="center">Computer Help Desk</td>
	<td bgcolor="#333333">全熟練度20</td>
	<td bgcolor="#333333">ハッキングの確率向上</td>
	<td bgcolor="#333333" align="center">特殊</td>
  </tr>
</table>
</TD></TR></table>
<br>
<FONT color="#ff0000"><B><a name="■戦闘説明">■戦闘説明</a></B></FONT><BR>
<BR>
　　エリア移動時、又は、エリア探索時に他のプレイヤーに遭遇した場合、戦闘となる。<BR>
　　こちらが先に発見した場合、先制攻撃が可能。<BR>
　　逆に、相手に見つかった場合は、相手の先制攻撃となる。<BR>
<BR>
　　戦闘で相手を殺した場合、相手が持っていた所持品、武器、防具のいずれかの一つを奪う事ができる。<BR>
　　但し、休憩中などに襲われて、返り討ちにした場合は、所持品を奪うことは出来ない。<br>
<br>
　　戦闘は、相手を発見した者の先制攻撃となる。自分が行動して、相手を発見した際、『攻撃』『逃げる』『メッセージ』を選択可能。<BR>
　　又、『戦闘』は、装備している武器により、殴る、刺すなど、武器で選択可能なコマンドになる。<BR>
<BR>
<table border="0" cellpadding="0" cellspacing="0"><TR><TD width="20">　</TD><TD>
<table border="1" cellpadding="0" cellspacing="0" width="504">
  <tr>
	<td align="center">『戦闘』</td>
	<td>相手を攻撃する。反撃を受ける可能性もある。<br>もたもたしているうちに、相手が逃げてしまったら、攻撃は不可能となる。</td>
  </tr><tr>
	<td align="center">『逃亡』</td>
	<td>攻撃せず、逃亡する。経験値、熟練度を取得する事は出来ない。</td>
  </tr><tr>
	<td align="center">『メッセージ』</td>
	<td>攻撃時に、相手にメッセージを送る。逃亡時はメッセージを送ることは出来ない。</td>
  </tr>
</table>
</TD></TR></table>
<br>
　　エリア移動時、又は、エリア探索時に他のプレイヤーに遭遇した場合、譲渡することができる。<br>
　　譲渡には自分のスタミナが<?=$pref['jyouto_sta']?>ポイント消費し、相手のスタミナも<?=$pref['jyouto_w_sta']?>ポイント消費する。
<BR>
<br>
<FONT color="#ff0000"><B><a name="■怪我、状態">■怪我、状態</a></B></FONT><BR>
<BR>
　　戦闘中に、一定の確率で負傷をする事がある。負傷箇所により、以下の制限を受ける。<BR>
　　又、武器によって負傷させられる確率、場所が異なる。<BR>
　　特定の防具によっては、怪我を防ぐことも可能である。<BR>
　　怪我を防ぐ都度、防具の耐久度が低下し、破損する。<BR>
<BR>
<table border="0" cellpadding="0" cellspacing="0"><TR><TD width="20">　</TD><TD>
<table border="1" cellpadding="0" cellspacing="0">
  <tr>
	<td>『頭』</td>
	<td>攻撃の命中率が低下。</td>
  </tr><tr>
	<td>『腕』</td>
	<td>攻撃力が低下。</td>
  </tr><tr>
	<td>『腹』</td>
	<td>睡眠、治療時の回復力が低下。</td>
  </tr><tr>
	<td>『足』</td>
	<td>移動、探索時のスタミナ消費増加。</td>
  </tr><tr>
	<td>『毒』</td>
	<td>移動、検索時の体力低下。</td>
  </tr>
</table>
</TD></TR></table>
<br>
　　怪我は、特殊コマンドの「応急処置」で治療する事が出来る。（一箇所のみ）<BR>
　　応急処置には、スタミナ<?=$pref['okyu_sta']?>を必要とする。<br>
　　但し、毒は中和剤が必要となる。（毒の詳細については以下を参照）<BR>
<BR>
<FONT color="#ff0000"><B><a name="■基本方針">■基本方針</a></B></FONT><BR>
<BR>
<table border="0" cellpadding="0" cellspacing="0" width="663"><TR><TD width="20">　</TD>
  <TD width="643">
<table border="1" cellpadding="0" cellspacing="0" style="border: .75pt solid black; background-color: white; border-collapse:collapse" width="643" height="279">
  <tr>
	<td class="t1" width="56" height="28">方針</td>
	<td class="t2" width="269" height="28"><font size="2" face="ＭＳ Ｐゴシック">説明</td>
	<td class="t2" width="72" height="28">&nbsp;&nbsp;&nbsp;攻撃力&nbsp;&nbsp;&nbsp;</td>
	<td class="t2" width="72" height="28">&nbsp;&nbsp;&nbsp;防御力&nbsp;&nbsp;&nbsp;</td>
	<td class="t2" width="72" height="28">&nbsp;&nbsp;&nbsp;発見率&nbsp;&nbsp;&nbsp;</td>
	<td class="t2" width="98" height="28">&nbsp;先制攻撃率&nbsp;</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" width="56" height="40">通常</td>
	<td class="t4" width="269" height="40" style="text-align:left"><br><font color="#FFFFFF">可も無く、不可も無い。通常に行動する。</font><br>　</td>
	<td class="t4" width="72" height="40"><font color="#FFFFFF"><b>　</b></font></td>
	<td class="t4" width="72" height="40"><font color="#FFFFFF"><b>　</b></font></td>
	<td class="t4" width="72" height="40"><font color="#FFFFFF"><b>　</b></font></td>
	<td class="t4" width="98" height="40"><font color="#FFFFFF"><b>　</b></font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" width="56" height="40">攻撃重視</td>
	<td class="t5" width="269" height="40" style="text-align:left"><br><font color="#FFFFFF">攻撃に重点を置き行動する。</font><br>　</td>
	<td class="t5" width="72" height="40"><font color="#00ffff"><b>↑↑↑</b></font></td>
	<td class="t5" width="72" height="40"><font color="#ffff00"><b>↓↓↓</b></font></td>
	<td class="t5" width="72" height="40"><font color="#FFFFFF"><b>　</b></font></td>
	<td class="t5" width="98" height="40"><font color="#FFFFFF"><b>　</b></font></td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" width="56" height="40">防御重視</td>
	<td class="t4" width="269" height="40" style="text-align:left"><br><font color="#FFFFFF">防御に重点を置き行動する。</font><br>　</td>
	<td class="t4" width="72" height="40"><font color="#ffff00"><b>↓↓↓</b></font></td>
	<td class="t4" width="72" height="40"><font color="#00ffff"><b>↑↑↑</b></font></td>
	<td class="t4" width="72" height="40"><font color="#FFFFFF"><b>　</b></font></td>
	<td class="t4" width="98" height="40"><font color="#ffff00"><b>↓↓</b></font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" width="56" height="40">隠密行動</td>
	<td class="t5" width="269" height="40" style="text-align:left"><br><font color="#FFFFFF">敵に見つからないように隠ながら行動する。</font><br>　</td>
	<td class="t5" width="72" height="40"><font color="#ffff00"><b>↓↓↓</b></font></td>
	<td class="t5" width="72" height="40"><font color="#ffff00"><b>↓↓</b></font></td>
	<td class="t5" width="72" height="40"><font color="#ffff00"><b>↓↓</b></font></td>
	<td class="t5" width="98" height="40"><font color="#00ffff"><b>↑↑↑</b></font></td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" width="56" height="40">探索行動</td>
	<td class="t4" width="269" height="40" style="text-align:left"><font color="#FFFFFF"><br>探索しながら行動する。</font><br>　</td>
	<td class="t4" width="72" height="40"><font color="#ffff00"><b>↓</b></font></td>
	<td class="t4" width="72" height="40"><font color="#ffff00"><b>↓</b></font></td>
	<td class="t4" width="72" height="40"><font color="#00ffff"><b>↑↑↑</b></font></td>
	<td class="t4" width="98" height="40"><font color="#00ffff"><b>↑↑</b></font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" width="56" height="37">連闘行動</td>
	<td class="t5" width="269" height="37" style="text-align:left"><font color="#FFFFFF">三日目以降可能な行動であり、<br>同じ相手に1/2の確率で連続して攻撃できる。<br>但し、相手も連闘行動の場合は75%となる。</td>
	<td class="t5" width="72" height="37"><font color="#ffff00"><b>↓↓</b></font></td>
	<td class="t5" width="72" height="37"><font color="#ffff00"><b>↓↓</b></font></td>
	<td class="t5" width="72" height="37"><font color="#ffff00"><b>↓</b></font></td>
	<td class="t5" width="98" height="37"><font color="#ffff00"><b>↓</b></font></td>
  </tr>
</table>
</TD></TR></table>
<br>
<B><a name="■応戦方針"><font size="2" face="ＭＳ Ｐゴシック" color="#ff0000">■応戦方針</FONT></a></B><BR>
<BR>
<table border="0" cellpadding="0" cellspacing="0"><TR><TD width="20">　</TD><TD>
<table border="1" cellpadding="0" cellspacing="0" id="AutoNumber8" style="border: .75pt solid black; background-color: white" width="643" height="229">
  <tr>
	<td class="t1" width="56" height="28">方針</td>
	<td class="t2" width="269" height="28">説明</td>
	<td class="t2" width="72" height="28">&nbsp;&nbsp;&nbsp;攻撃力&nbsp;&nbsp;&nbsp;</td>
	<td class="t2" width="72" height="28">&nbsp;&nbsp;&nbsp;防御力&nbsp;&nbsp;&nbsp;</td>
	<td class="t2" width="80" height="28">&nbsp;&nbsp;&nbsp;遭遇率&nbsp;&nbsp;&nbsp;</td>
	<td class="t2" width="90" height="28">&nbsp;&nbsp;&nbsp;回避率&nbsp;&nbsp;&nbsp;</td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" width="56" height="27">通常</td>
	<td class="t4" width="269" height="27" style="text-align:left"><font color="#FFFFFF"><br>可も無く、不可も無い。通常に戦闘。<br>　</font></td>
	<td class="t4" width="72" height="27"><font color="#FFFFFF"><b>　</b></font></td>
	<td class="t4" width="72" height="27"><font color="#FFFFFF"><b>　</b></font></td>
	<td class="t4" width="90" height="27"><font color="#FFFFFF"><b>　</b></font></td>
	<td class="t4" width="80" height="27"><font color="#FFFFFF"><b>　</b></font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" width="56" height="27">攻撃体勢</td>
	<td class="t5" width="269" height="27" style="text-align:left"><font color="#FFFFFF"><br>相手を返り討ちにする。<br>　</font></td>
	<td class="t5" width="72" height="27"><font color="#00ffff"><b>↑↑↑</b></font></td>
	<td class="t5" width="72" height="27"><font color="#ffff00"><b>↓↓↓</b></font></td>
	<td class="t5" width="80" height="27"><font color="#ffff00"><b>↓</b></font></td>
	<td class="t5" width="90" height="27"><font color="#FFFFFF"><b>　</b></font></td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" width="56" height="27">防御体勢</td>
	<td class="t4" width="269" height="27" style="text-align:left"><font color="#FFFFFF"><br>防御に徹する。<br>　</font></td>
	<td class="t4" width="72" height="27"><font color="#ffff00"><b>↓↓↓</b></font></td>
	<td class="t4" width="72" height="27"><font color="#00ffff"><b>↑↑↑</b></font></td>
	<td class="t4" width="80" height="27"><font color="#00ffff"><b>↑</b></font></td>
	<td class="t4" width="90" height="27"><font color="#FFFFFF"><b>　</b></font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" width="56" height="27">隠密体勢</td>
	<td class="t5" width="269" height="27" style="text-align:left"><font color="#FFFFFF"><br>敵に見つからないように隠ながら行動する。<br>　</font></td>
	<td class="t5" width="72" height="27"><font color="#ffff00"><b>↓↓</b></font></td>
	<td class="t5" width="72" height="27"><font color="#FFFFFF"><b>　</b></font></td>
	<td class="t5" width="80" height="27"><font color="#ffff00"><b>↓↓</b></font></td>
	<td class="t5" width="90" height="27"><font color="#FFFFFF"><b>　</b></font></td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" width="56" height="42">治療専念</font></td>
	<td class="t4" width="269" height="42" style="text-align:left"><font color="#FFFFFF">とにかく傷を癒すことに専念する。<br>奇襲、反撃不可能。治療に要する時間が半分に。</font></td>
	<td class="t4" width="72" height="42"><font color="#FFFFFF"><b>　</b></font></td>
	<td class="t4" width="72" height="42"><font color="#ffff00"><b>↓↓↓</b></font></td>
	<td class="t4" width="80" height="42"><font color="#00ffff"><b>↑</b></font></td>
	<td class="t4" width="90" height="42"><font color="#ffff00"><b>↓↓↓</b></font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" width="56" height="37">逃亡体勢</td>
	<td class="t5" width="269" height="37" style="text-align:left"><font color="#FFFFFF">攻撃されたら直ちにその場から去る。<br>又、その場が禁止エリアになっても立ち去ります。<br>奇襲、反撃不可能ですのでご注意！<br>（連闘行動 及び 禁止エリア死亡対処です。）</font></td>
	<td class="t5" width="72" height="37"><font color="#FFFFFF"><b>　</b></font></td>
	<td class="t5" width="72" height="37"><font color="#ffff00"><b>↓↓</b></font></td>
	<td class="t5" width="80" height="37"><font color="#FFFFFF"><b>　</b></font></td>
	<td class="t5" width="90" height="37"><font color="#ffff00"><b>↓</b></font></td>
  </tr>
</table>
</TD></TR></table>
<font size="2" color="#FF0000">　　※発見率とは、他のPlayerに発見される意味の事です</font><B><font size="2"><br>
<br><FONT color="#ff0000"><a name="■熟練度・熟練レベル">■熟練度・熟練レベル</a></font></B><BR>
<BR>
　　使用する武器種別毎に熟練度があり、その武器で敵を攻撃する度に熟練度があがっていく。<BR>
　　熟練度は、攻撃が外れても、上昇する。（攻撃をせずに逃走した時は熟練値を得ることはできない）<BR>
　　熟練度が一定値に達すると、熟練レベルが上がる。
<br>
　　熟練していない種類の武器で戦闘をすると、その武器の力を100%発揮する事が出来ない。<BR>
　　熟練したものが、武器を使えば、武器の持つ力を上回る効果を発揮するだろう。<BR>
<BR>
<B><FONT color="#ff0000"><a name="■リミット・必殺技">■リミット</a></font></B><BR>
<BR>
　　攻撃を受ける度にリミット値が上がっていく<BR>
　　100になった次の戦闘にてリミットブレイクが発動する<BR>
　　詳細は下の必殺技を参照<BR>
<BR>
<FONT color="#ff0000"><B>■必殺技</B></FONT><BR>
<BR>
　　発動方法は、戦闘時の相手に残すメッセージにそれぞれ「必殺技」又は、「超必殺技」と入れる事で可能<BR>
　　攻撃力が高い方が優先される<BR>
<BR>
<table border="0" cellpadding="0" cellspacing="0"><TR><TD width="20">　</TD><TD>
<table border="1" cellpadding="0" cellspacing="0" style="border: .75pt solid black; background-color: white" height="158">
  <tr>
	<td class="t1" height="28">必殺技の種類</td>
	<td class="t2" height="28">発動形式</td>
	<td class="t2" height="28">条件</td>
	<td class="t2" height="28">効果</td>
	<td class="t2" height="28">攻撃力</td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" height="1"><br>クリティカルヒット<br>　</td>
	<td class="t4" height="1"><font color="#FFFF00">自動</font></td>
	<td class="t4" height="1"><font color="#FFFFFF">レベル<B><font color="#00ff00">3</font></B>以上<br>熟練度が<B><font color="#00ff00">20</font></B>以上</font></td>
	<td class="t4" height="1"><font color="#FFFFFF">先攻・後攻の時に一定の確率で自動的に発動</font></td>
	<td class="t4" height="1"><font color="#0000FF">★☆☆☆☆</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" height="1"><br>必殺技<br>　</td>
	<td class="t5" height="1"><font color="#00FFFF">任意</font></td>
	<td class="t5" height="1"><font color="#FFFFFF">熟練度が<B><font color="#00ff00">40</font></B>以上</font></td>
	<td class="t5" height="1"><font color="#FFFFFF">リミット<B><font color="#00ff00">15</font></b>使用<br>及び、最大体力の<B><font color="#00ff00">10%</font></B>程度の体力を消費し最大体力の6%程度を攻撃力に追加</font></td>
	<td class="t5" height="1"><font color="#0000FF">★★☆☆☆</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" height="1"><br>超必殺技<br>　</td>
	<td class="t4" height="1"><font color="#00FFFF">任意</font></td>
	<td class="t4" height="1"><font color="#FFFFFF">熟練度が<B><font color="#00ff00">100</font></B>以上<br>体力が<B><font color="#00ff00">100</font></B>以下</font></td>
	<td class="t4" height="1"><font color="#FFFFFF">リミット<B><font color="#00ff00">30</font></b>使用<br>及び、最大体力を<B><font color="#00ff00">10%</font></B>程度消費し最大体力の10%程度を攻撃力に追加</font></td>
	<td class="t4" height="1"><font color="#0000FF">★★★☆☆</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" height="1"><br>秘技<br>　</td>
	<td class="t5" height="1"><font color="#FFFF00">自動</font></td>
	<td class="t5" height="1"><font color="#FFFFFF">レベル<B><font color="#00ff00">10</font></B>以上<br>熟練度が<B><font color="#00ff00">200</font></B>以上</font></td>
	<td class="t5" height="1"><font color="#FFFFFF">先攻・後攻の時に一定の確率で自動的に発動</font></td>
	<td class="t5" height="1"><font color="#0000FF">★★★★☆</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" height="1"><br>リミットブレイク<br>　</td>
	<td class="t4" height="1"><font color="#FFFF00">自動</font></td>
	<td class="t4" height="1"><font color="#FFFFFF">リミットが<B><font color="#00ff00">100</font></B>以上のみ</font></td>
	<td class="t4" height="1"><font color="#FFFFFF">リミットが<B><font color="#00ff00">100</font></B>以上の場合のみ発動<BR>先攻・後攻の時に自動的に発動</font></td>
	<td class="t4" height="1"><font color="#0000FF">★★★★★</font></td>
  </tr>
  </table>
</TD></TR></table>
<br>
<FONT color="#ff0000"><B><a name="■禁止エリア">■禁止エリア</a></B></FONT>
<BR><BR>
　　毎日8時間おき<i>（0時、8時、16時）</i>に担任より、禁止エリアが発表される。<br>
　　時間になっても、禁止エリアにいた場合、政府により首輪が爆発され死亡する。<BR>
<BR>
<FONT color="#ff0000"><B><a name="■天気">■天気</a></B></FONT><BR>
<BR>
　　禁止エリアの発表と同時に天候が変化する。<br>
　　天候によって、さまざまな要素が変化されるが、変化は以下の通りである。<br>
　　基本的に微々たる変化であるため最大二割程度の変化が天候によって影響される。<br>
　　天候が霧の時、一定の確率で相手が確認できない場合がある。<br>
　　この場合、同じグループメンバーでも攻撃ができてしまうので、気をつける必要がある。<br>
<br>
</font>
<table border="0" cellpadding="0" cellspacing="0"><TR><TD width="20">　</TD><TD>
<table border="1" cellpadding="0" cellspacing="0" style="border: .75pt solid black; background-color: white; border-collapse:collapse">
  <tr>
	<td class="t1" height="22" width="70">天気</td>
	<td class="t2">&nbsp;攻撃力&nbsp;</td>
	<td class="t2">&nbsp;防御力&nbsp;</td>
	<td class="t2">&nbsp;回避率&nbsp;</td>
	<td class="t2">&nbsp;発見率&nbsp;</td>
	<td class="t2">&nbsp;視野&nbsp;</td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" height="22" width="70">『快晴』</td>
	<td class="t4"><font color="#0000FF">激強</font></td>
	<td class="t4"><font color="#0000FF">激強</font></td>
	<td class="t4"><font color="#0000FF">激強</font></td>
	<td class="t4"><font color="#0000FF">激強</font></td>
	<td class="t4"><font color="#0000FF">高</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" height="22" width="70">『晴れ』</td>
	<td class="t5"> <font color="#00FFFF">強</font></td>
	<td class="t5"> <font color="#00FFFF">強</font></td>
	<td class="t5"><font color="#00FFFF">強</font></td>
	<td class="t5"><font color="#00FFFF">強</font></td>
	<td class="t5"><font color="#0000FF" >高</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" height="22" width="70">『曇り』</td>
	<td class="t4"><font color="#FFFFFF">無</font></td>
	<td class="t4"><font color="#FFFFFF">無</font></td>
	<td class="t4"><font color="#FFFFFF">無</font></td>
	<td class="t4"><font color="#FFFFFF">無</font></td>
	<td class="t4"><font color="#0000FF" >高</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" height="23" width="70">『雨』</td>
	<td class="t5"> <font color="#00FF00">微弱</font></td>
	<td class="t5"> <font color="#00FF00">微弱</font></td>
	<td class="t5"><font color="#00FF00">微弱</font></td>
	<td class="t5"><font color="#00FF00">微弱</font></td>
	<td class="t5"><font color="#00FF00">微低</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" height="23" width="70">『豪雨』</td>
	<td class="t4"><font color="#00FF00">微弱</font></td>
	<td class="t4"><font color="#00FF00">微弱</font></td>
	<td class="t4"><font color="#00FF00">微弱</font></td>
	<td class="t4"><font color="#00FF00">微弱</font></td>
	<td class="t4"><font color="#FFFF00" >低</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" height="23" width="70">『台風』</td>
	<td class="t5"><font color="#00FF00">微弱</font></td>
	<td class="t5"><font color="#00FF00">微弱</font></td>
	<td class="t5"><font color="#00FF00">微弱</font></td>
	<td class="t5"><font color="#00FF00">微弱</font></td>
	<td class="t5"><font color="#FFFF00" >低</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" height="23" width="70">『雷雨』</td>
	<td class="t4"><font color="#00FF00">微弱</font></td>
	<td class="t4"><font color="#FFFF00">弱</font></td>
	<td class="t4"><font color="#FFFF00">弱</font></td>
	<td class="t4"><font color="#00FF00">微弱</font></td>
	<td class="t4"><font color="#FFFF00" >低</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" height="23" width="70">『雪』</td>
	<td class="t5"><font color="#FFFF00">弱</font></td>
	<td class="t5"><font color="#FFFF00">弱</font></td>
	<td class="t5"><font color="#00FF00">微弱</font></td>
	<td class="t5"><font color="#FFFF00">弱</font></td>
	<td class="t5"><font color="#00FF00" >微低</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#CCCCCC" height="23" width="70">『霧』</td>
	<td class="t4"><font color="#FFFFFF">無</font></td>
	<td class="t4"><font color="#00FF00">激弱</font></td>
	<td class="t4"><font color="#FF00FF">微強</font></td>
	<td class="t4"><font color="#FFFF00">弱</font></td>
	<td class="t4"><font color="#FF00FF" >激低</font></td>
  </tr><tr>
	<td class="t3" bgcolor="#999999" height="23" width="70">『濃霧』</td>
	<td class="t5"><font color="#FF00FF">微強</font></td>
	<td class="t5"><font color="#00FF00">激弱</font></td>
	<td class="t5"><font color="#FFFFFF">無</font></td>
	<td class="t5"><font color="#FFFF00">弱</font></td>
	<td class="t5"><font color="#FF0000">超低</font></td>
  </tr>
  </table>
</TD></TR></table>
<BR>
<FONT color="#ff0000"><B><a name="■武器">■武器</a></B></FONT><BR>
<BR>
　　銃系の武器は、弾丸を装填しなければ、鈍器としてしか利用出来ない。<BR>
　　弾丸は、6発まで装填可能。<BR>
　　武器は、一定の確率で壊れる事がある。<BR>
　　壊れた武器は二度と使い物にならず、素手で戦わなければならない。<BR>
　　投げて攻撃するタイプの武器は、投げつけた後、壊れてしまうので、消耗品となる。<BR>
<BR>
　　「剣」系武器は、一定の確率で刃こぼれをおこし、攻撃力が低下していく。<BR>
　　特定のアイテムで手入れをする事により攻撃力は増加する。<BR>
<BR>
<FONT color="#ff0000"><B><a name="■罠・毒">■罠・毒</a></B></FONT><BR>
<BR>
　　あるアイテムを使用することで、エリアに罠を仕掛けることが出来る。<BR>
　　また、回復系のアイテムには毒が含まれている場合がある。<BR>
　　罠・毒によりダメージは可変で、自分で仕掛けた罠・毒に、自分が引っかかる可能性もあるので、<BR>
　　罠・毒の設置には、注意が必要である。<br>
<BR>
　　尚、毒は応急処置では回復できないので『解毒剤』というアイテムを見つける必要性がある。<br>
<BR>
<FONT color="#ff0000"><B><a name="■注意事項">■注意事項</a></B></FONT><BR>
<BR>
　　開始後、分校に待機していると、禁止エリア追加時に首輪が爆発し、死亡するので、分校での待機は命取りとなる。<BR>
<BR>
　　一度攻撃した相手へは、その相手がコマンドを実行するまで、もしくは、<BR>
　　相手が他のプレイヤーに攻撃を受けるまでは、再度攻撃する事が出来ない。<BR>
<br>
　　二日目以降は一定の経験値がある状態から始める事ができる。<br>
　　これは、二日目以降の者と生存者をある程度、平等にするためである。<br>
<BR>
<FONT color="#ff0000"><B><a name="■Special Thanx">■Special Thanx</a></B></FONT><BR>
<BR>
　　このBRUを作るにあったて ご協力いただいた作者及びWebサイト<BR>
<br>
<table border="0" cellpadding="0" cellspacing="0"><TR><TD width="20">　</TD><TD>
<TABLE border="0" cellspacing="0" cellpadding="0">
<TR>
  <TD><B><U>Site Name</U></B></TD>
  <TD align="center">　</TD>
  <TD>　<B><U>Name</U></B></TD>
  <TD>　<B><U>Thanx for</U></B></TD>
</TR><TR>
  <TD><b><font color="#FFFF00">Battle Royale 本家</font></b></TD>
  <TD align="center">―</TD>
  <TD><font color="#00FF00">　バッカス 様</font></TD>
  <TD><font color="#00FFFF">　BRのスクリプトの配布をいただきました。<BR>　以前はBRの本家を運営されていました。</font></TD>
</TR><TR>
  <TD><b><a href="http://www.hoops.ne.jp/~kurisutof/"><font color="#FFFF00">透明な王国</font></a></b></TD>
  <TD align="center"><font size="2" face="ＭＳ Ｐゴシック">―</font></TD>
  <TD><font color="#00FF00">　クリストフ 様</font></TD>
  <TD><font color="#00FFFF">　改造の際参考になったサイトです。<br>　また、メッセンジャーの基本機能もここを参考にしました。</font></TD>
</TR>
<TR>
  <TD>　</TD>
  <TD width="47" align="center">　</TD>
  <TD>　</TD>
  <TD>　</TD></TR>
</TABLE>
</TD></TR></table>
</div>
<?
FOOTER();
?>