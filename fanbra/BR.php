<?php
require 'pref.php';
#□■□■□■□■□■□■□■□■□■□■□
#■ 	-     BR MAIN PROGRAM     - 			■
#□																		□
#■ 		  サブルーチン一覧								■
#□																		□
#■ MAIN	-	メイン処理										■
#□ COM		-	コマンド処理								□
#■ HEAL	-	治療処理											■
#□ INN		-	睡眠処理										□
#■ INNHEAL	-	静養処理									■
#□ BB_CK	-	ブラウザバック不正防止			□
#■□■□■□■□■□■□■□■□■□■□■
CREAD();
IDCHK();

if($in['mode']=='main'){MAIN();}
elseif($in['mode']=='command'){COMM();}
else{ERROR('不正常登录','No Command selected','BATTLE',__FUNCTION__,__LINE__);}
UNLOCK();
exit;
#===============#
# ■ メイン処理 #
#===============#
function MAIN(){
global $pref;

HEAD();
require $pref['LIB_DIR'].'/dsp_sts.php';
require $pref['LIB_DIR'].'/dsp_cmd.php';
STS();
FOOTER();
}
#=================#
# ■ コマンド処理 #
#=================#
function COMM(){
global $in,$pref;

global $mflg;
#★ Messener
if($in['Command']=='MESS')											{require $pref['LIB_DIR'].'/lib4.php';MESS();}	#送信
elseif($in['Command']=='MSG_DEL')									{require $pref['LIB_DIR'].'/lib4.php';MSG_DEL();}#削除
#★ 移動・検索・勝利時
elseif($in['Command']=='MOVE' && strpos($in['Command2'],'MV')===0)	{require $pref['LIB_DIR'].'/lib2.php';MOVE();}	#移動
elseif($in['Command']=='SEARCH')									{require $pref['LIB_DIR'].'/lib2.php';SEARCH();}#探索
#★ 回復処理部
elseif($in['Command']=='kaifuku'){
	if($in['Command5']=='HEAL')			{HEAL();}	#治療
	elseif($in['Command5']=='INN')		{INN();}	#睡眠
	elseif($in['Command5']=='INNHEAL')	{INNHEAL();}#静養
}
elseif($in['Command']=='HEAL')		{HEAL();}		#治療
elseif($in['Command']=='INN')		{INN();}		#睡眠
elseif($in['Command']=='INNHEAL')	{INNHEAL();}	#静養
#★ ITEM1
elseif(strpos($in['Command'],'ITEM_')===0)		{require $pref['LIB_DIR'].'/item1.php';ITEM();}			#アイテム使用
elseif(strpos($in['Command'],'DEL_')===0)			{require $pref['LIB_DIR'].'/item1.php';ITEMDEL();}		#アイテム投棄
elseif($in['Command']=='ITEMDELNEW')				{require $pref['LIB_DIR'].'/item1.php';ITEMDELNEW();}	#アイテム捨てる
elseif($in['Command']=='ITEMGETNEW')				{require $pref['LIB_DIR'].'/item1.php';ITEMGETNEW();}	#アイテム拾う
elseif(strpos($in['Command'],'ITEMNEWXCG_')===0)	{require $pref['LIB_DIR'].'/item1.php';ITEMNEWXCG();}	#アイテム交換
elseif(strpos($in['Command'],'ITEMSEINEW_')===0)	{require $pref['LIB_DIR'].'/item1.php';ITEMSEINEW();}	#アイテム整理
elseif($in['Command']=='ITMAIN'){
	require $pref['LIB_DIR'].'/item1.php';
	if($in['Command3']=='WEPDEL')		{WEPDEL();}	#武器外す
	elseif($in['Command3']=='WEPDEL2')	{WEPDEL2();}#武器捨てる
	elseif($in['Command3']=='BOUDELH')	{BOUDELH();}#頭防具を外す
	elseif($in['Command3']=='BOUDELB')	{BOUDELB();}#体防具を外す
	elseif($in['Command3']=='BOUDELA')	{BOUDELA();}#腕防具を外す
	elseif($in['Command3']=='BOUDELF')	{BOUDELF();}#足防具を外す
	elseif($in['Command3']=='BOUDEL')	{BOUDEL();}	#装飾品を外す
}
#★ ATTACK
elseif(strpos($in['Command'],'ATK')===0)	{require $pref['LIB_DIR'].'/attack.php';require $pref['LIB_DIR'].'/att_sen.php';ATTACK1();}	#攻撃
elseif($in['Command']=='RUNAWAY')			{require $pref['LIB_DIR'].'/attack.php';RUNAWAY();}									#逃亡
elseif(strpos($in['Command'],'GET_')===0)	{require $pref['LIB_DIR'].'/att_etc.php';WINGET();}									#戦利品
#★ LIB3
elseif(strpos($in['Command'],'OUK_')===0)								{require $pref['LIB_DIR'].'/lib3.php';OUKYU();}	#応急処置
elseif((isset($in['Message'])&&$in['Message']!='')||(isset($in['Message2']) && $in['Message2'] != '')||(isset($in['Comment']) && $in['Comment'] != ''))
																		{require $pref['LIB_DIR'].'/lib3.php';WINCHG();}	#口癖変更
elseif((isset($in['teamID2']) && $in['teamID2'] != '')||(isset($in['teamPass2']) && $in['teamPass2'] != ''))
																		{require $pref['LIB_DIR'].'/lib3.php';TEAM();}	#グループ操作
elseif($in['Command']=='SPEAKER')										{require $pref['LIB_DIR'].'/lib3.php';SPEAKER();}#携帯スピーカ使用
elseif($in['Command']=='SPECIAL'&&$in['Command4']=='HACK')			{require $pref['LIB_DIR'].'/lib3.php';HACKING();}#ハッキング
elseif(strpos($in['Command'],'KOU_')===0)								{require $pref['LIB_DIR'].'/lib3.php';KOUDOU();}	#基本方針
elseif(strpos($in['Command'],'OUS_')===0)								{require $pref['LIB_DIR'].'/lib3.php';OUSEN();}	#応戦方針
elseif(strpos($in['Command'],'POI_')===0)								{require $pref['LIB_DIR'].'/lib3.php';POISON();}	#毒物混入
elseif(strpos($in['Command'],'PSC_')===0)								{require $pref['LIB_DIR'].'/lib3.php';PSCHECK();}#毒見
elseif(strpos($in['Command'],'BAITEN2_')===0)							{require $pref['LIB_DIR'].'/lib3.php';BAITEN2();}#売店２
#★ ITEM2
elseif(strpos($in['Command'],'SEIRI_')===0 && strpos($in['Command2'],'SEIRI2_')===0)	{require $pref['LIB_DIR'].'/item2.php';ITEMSEIRI();}	#アイテム整理
elseif(strpos($in['Command'],'GOUSEI1_')===0 && strpos($in['Command2'],'GOUSEI2_')===0 && strpos($in['Command3'],'GOUSEI3_')===0)		#アイテム合成
																						{require $pref['LIB_DIR'].'/item2.php';ITEMGOUSEI();}
elseif(strpos($in['Command'],'SEITO_')===0 && strpos($in['Command2'],'JO_')===0)		{require $pref['LIB_DIR'].'/item2.php';ITEMJOUTO();}		#アイテム譲渡

#★ 戦闘
if(strpos($in['Command'],'BATTLE')===0 || strpos($in['Command'],'ATK')===0)
		{HEAD();require $pref['LIB_DIR'].'/dsp_att.php';require$pref['LIB_DIR'].'/dsp_cmd.php';BATTLE();FOOTER();}#戦闘結果
elseif($in['Command']=='ITEMJOUTO')
		{HEAD();require $pref['LIB_DIR'].'/dsp_att.php';require$pref['LIB_DIR'].'/dsp_cmd.php';BATTLE();FOOTER();}#アイテム譲渡
#★ ADMIN
elseif($in['Command']=='DEATHGET')	{HEAD();require $pref['LIB_DIR'].'/dsp_att.php';require$pref['LIB_DIR'].'/dsp_cmd.php';BATTLE();FOOTER();}#死体発見
elseif($mflg!='ON'){MAIN();}
else{ERROR('没有选择命令','Command not found','BR',__FUNCTION__,__LINE__);}
}
#===================#
# ■ IDチェック処理 #
#===================#
function IDCHK(){
global $in,$pref;

global $mem,$chksts,$Index,$host,$fl,$ar,$mem;
global $id,$password,$f_name,$l_name,$sex,$cl,$no,$endtime,$att,$def,$hit,$mhit,$level,$exp,$sta,$wep,$watt,$wtai,$bou,$bdef,$btai,$bou_h,$bdef_h,$btai_h,$bou_f,$bdef_f,$btai_f,$bou_a,$bdef_a,$btai_a,$tactics,$death,$msg,$sts,$pls,$kill,$icon,$item,$eff,$itai,$com,$log,$dmes,$bid,$money,$wp,$wg,$wn,$wc,$wd,$comm,$limit,$bb,$inf,$ousen,$seikaku,$sinri,$item_get,$eff_get,$itai_get,$teamID,$teamPass;
$userlist = @file($pref['user_file']) or ERROR('unable to open file user_file user_file','用户文件读取失败','BR',__FUNCTION__,__LINE__);
$jyulog = $jyulog2 = $jyulog3 = '';#銃声ログ
$mem=0;
$chksts = 'NG';

list($ka,$hf) = explode(',',trim($pref['arealist'][1]));
if(!$ka){
	ERROR('在游戏开始前请稍候');
}
for($i=0;$i<count($userlist);$i++){
	list($w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP,) = explode(",", $userlist[$i]);
	if($w_id == $in['Id']){	#ID一致？
		if($w_password == $in['Password']){	#パスワード正常？
			if($w_hit > 0){	#生存?
				$chksts = 'OK';$Index=$i;$mem++;
				list($id,$password,$f_name,$l_name,$sex,$cl,$no,$endtime,$att,$def,$hit,$mhit,$level,$exp,$sta,$wep,$watt,$wtai,$bou,$bdef,$btai,$bou_h,$bdef_h,$btai_h,$bou_f,$bdef_f,$btai_f,$bou_a,$bdef_a,$btai_a,$tactics,$death,$msg,$sts,$pls,$kill,$icon,$item[0],$eff[0],$itai[0],$item[1],$eff[1],$itai[1],$item[2],$eff[2],$itai[2],$item[3],$eff[3],$itai[3],$item[4],$eff[4],$itai[4],$item[5],$eff[5],$itai[5],$log,$com,$dmes,$bid,$in['club'],$money,$wp,$wg,$wn,$wc,$wd,$comm,$limit,$bb,$inf,$ousen,$seikaku,$sinri,$item_get,$eff_get,$itai_get,$teamID,$teamPass,$in['IP'],) = explode(",", $userlist[$i]);
				if(($item_get!='无')&&(!preg_match('/ITEM|GET/',$in['Command']))){require $pref['LIB_DIR'].'/item1.php';ITEMDELNEW();}/*新規アイテム所持？*/
				if($host != $in['IP']){
					if(!$pref['MOBILE']){
						debug($pref['now'].','.$id.','.$host."\n");
					}else{
						//debug($pref['now'].','.$id.','.$pref['MOBILE']."\n");
					}
				}
				if($host == $pref['SubS']){
					if($in['IPAdd'] != ''){$in['IP'] = $in['IPAdd'];}
				}elseif(!$pref['MOBILE']){
					$in['IP'] = $host;
				}
				CSAVE();
				$gunlog = @file($pref['gun_log_file']) or ERROR('unable to open file gun_log_file','','BR',__FUNCTION__,__LINE__);
				global $jyulog,$jyulog2,$jyulog3;
				list($guntime,$gunpls,$wid,$wid2,$a) = explode(',',$gunlog[0]);
				if(($pref['now'] < ($guntime+(30)))&& ($wid != $id)&&($wid2 != $id)){$jyulog = '<font color="yellow"><b>'.$gunpls.' 的附近，听到了打手枪的声音…。</b></font><br>';}/*銃使用から30秒以内？*/
				list($guntime,$gunpls,$wid,$wid2,$a) = explode(',',$gunlog[1]);
				if(($pref['now'] < ($guntime+(30)))&& ($wid != $id)&&($wid2 != $id)&&($pref['place'][$pls] == $gunpls)){$jyulog2 = '<font color="yellow"><b>附近有人发出了悲鸣吗？是谁，是谁被爆菊了…？</b></font><br>';}/*殺害から30秒以内？*/
				list($guntime,$gunpls,$wid,$wid2,$a) = explode(',',$gunlog[2]);
				if(($pref['now'] < ($guntime+(60)))){$jyulog3 = '<font color="yellow"><b>'.$gunpls.'的附近'.$wid.'的声音确实可以被听到…</b></font><br><font color="lime"><b>『'.$wid2.'』</b></font><br>';}/*スピーカ使用から1分以内？*/
			}else{#死亡
				global $c_id;
				if($c_id != '2YBRU'){CDELETE();}
				ERROR('你已经被爆菊至死了。<BR><BR>爆因：'.$w_death.'<BR><BR><font color="lime"><b>'.$w_msg.'</b></font><br><br><script language="JavaScript">SWF_DSP("./'.$pref['imgurl'].'/bar.swf?color=5",200,70);</script>','You died','BR',__FUNCTION__,__LINE__);}
		}else{
			if(preg_match('/2YBRU/',$w_password)){ERROR('已经被馆里猿锁定。请尽快联络馆里猿','Your userdata is been locked. Please contact with Administrator','BR',__FUNCTION__,__LINE__);}
			else{ERROR('密码不吻合','Password does not match','BATTLE-IDCHK','BR',__FUNCTION__,__LINE__);}}
	}else{#他PC生存者？
		if($w_hit>0 && $w_sts!='NPC'){$mem++;}
	}
}
if($chksts=='NG'){ERROR('ID不存在','ID Not Found','BR',__FUNCTION__,__LINE__);}
$b_limit = ($pref['battle_limit']) + 1;

if((($mem == 1)&&(preg_match('/胜/',$inf))&&($ar > $b_limit))||(($mem == 1)&&($ar > $b_limit))){
	if(!preg_match('/结束/',$fl)){/*優勝？*/
		$handle = @fopen($pref['end_flag_file'], 'w');
		if(!@fwrite($handle,"结束\n")){ERROR('unable to write end_flag_file','','BR',__FUNCTION__,__LINE__);}
		fclose($handle);
		LOGSAVE('END');
	}
	if(!preg_match('/胜/',$inf)){/*優勝？*/
		$inf .= '胜';
		LOGSAVE('WINNER');
	}
	require $pref['LIB_DIR'].'/dsp.php';ENDING();
}elseif(preg_match('/解/',$inf)){
	require $pref['LIB_DIR'].'/dsp.php';ENDING();
}elseif(@preg_match('/解除/',$fl)){
	require $pref['LIB_DIR'].'/dsp.php';ENDING();
}else{
	if($log != ''){$wlog = $log;$log='';$log=$wlog;}
	$bid = '';
}

SAVE();
}
#=========================#
# ■ 治療・睡眠・静養処理 #
#=========================#
function HEAL()		{
global $pref;
global $sts,$endtime;
$sts = '治疗';
$endtime = $pref['now'];
SAVE();
}
function INN(){
global $pref;
global $sts,$endtime;
$sts = '睡觉';
$endtime = $pref['now'];
SAVE();
}
function INNHEAL(){
global $pref;
global $sts,$endtime,$pls,$id,$hour,$min,$sec,$in;
if(!(($pref['place'][$pls] == "永琳医保定点单位")||((preg_match('/游泳部/',$pref['clb'][$in['club']]))&&(preg_match('/〇感沙滩|〇感海茶/',$pref['place'][$pls]))))){
	debug($hour.':'.$min.':'.$sec.','.$id.',INNHEAL'."\n");
	ERROR('不正当登入','INNHEAL bug fix','BR',__FUNCTION__,__LINE__);
}
$sts = '静养';
$endtime = $pref['now'];
SAVE();
}
#===========================#
# ■ ブラウザバック不正防止 #
#===========================#
function BB_CK(){
global $bb,$w_id;
if($bb == $w_id){$bb = '';}else{ERROR('不正常登录','Used Browser Back Command','BR',__FUNCTION__,__LINE__);}
}
?>