<?php
/*
#□■□■□■□■□■□■□■□■□■□■□
#■ 	-     BR USER REGISTER    -		 		■
#□																		□
#■ 		サブルーチン一覧			 						■
#□ 									 									□
#■ checker		-	確認				 						■
#□ MAIN		-	メイン				 							□
#■ REGIST		-	登録処理			 						■
#□ INFO		-	説明処理			 							□
#■ CLUBMAKE	-	社团作成				 					■
#□■□■□■□■□■□■□■□■□■□■□
*/
$regist = 1;
require "pref.php";

CREAD();

if		($in['mode'] == 'regist')	{REGIST();}	#登録処理
elseif	($in['mode'] == 'info')		{INFO();}	#説明処理
elseif	($in['mode'] == 'icon')		{ICON();}	#アイコン一覧
else								{MAIN();}	#メイン
//UNLOCK();
exit;

#===========#
# ■ 確認	#
#===========#
function checker(){
global $pref;

global $fl,$ar,$c_endtime,$c_id,$host;

if(!isset($pref['plimit']) || $pref['plimit']==0){$pref['plimit'] = 4;}
$t_limit = $pref['plimit'];

if((isset($fl)&& preg_match('/结束/',$fl))||($ar >= $t_limit)){
	ERROR('程序登录终止<br><br>　请等待下回程序开始吧。','no more registration','REGIST',__FUNCTION__,__LINE__);
}

list($ar,$hackflg,$a) = explode(',',$pref['arealist'][1]);

list($pre_reg,$res_y,$res_m,$res_d,$res_j,$res_f,$res_b) = explode(',',trim($pref['arealist'][7]));
list($y,$m,$d,$hh,$mm) = explode(',',trim($pref['arealist'][0]));

if(!$ar && (mktime($hh,$mm,0,$m,$d,$y) != mktime($res_j,$res_f,0,$res_m,$res_d,$res_y))){
	ERROR('不接收提前登录。<br><br>　请等待下回程序开始。','no more registration','REGIST',__FUNCTION__,__LINE__);
}

// 取消登录时间限定……
/*
$chktim = $c_endtime + (1*60*60*2);	#死亡時間取得
list($sec,$min,$hour,$mday,$month,$year,$wday,$yday,$isdst) = localtime($chktim);
$year+=1900; $month++;


if($chktim > $pref['now']){#登録時間エラー？
	ERROR('你被菊爆后，需要两小时养伤。<br><br>　次回登录可能时间：'.$year.'/'.$month.'/'.$mday.' '.$hour.':'.$min.':'.$sec,'Cannot Register for 2 hours after death','REGIST',__FUNCTION__,__LINE__);
}*/

#ユーザーファイル取得
$userlist = @file($pref['user_file']) or ERROR('unable to open file user_file','','REGIST',__FUNCTION__,__LINE__);

if((count($userlist) - $pref['npc_num']) >= $pref['maxmem']){#最大人数超過？
	ERROR('对不起，定员('.$pref['maxmem'].'人)而已','Too much player','REGIST',__FUNCTION__,__LINE__);
}

#重複チェック
foreach($userlist as $userlist2){
	list($w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP,) = explode(",",$userlist2);
	if(($c_id != '')&&($c_id == $w_id)&&($c_password == '')&&($c_password == $w_password)&&($w_sts != '菊爆')){#同一ID or 同姓同名?
		ERROR('角色的多线程登录禁止。菊花痒的话请咨询馆里猿。HOST ID:'.$in['IPAdd'].'-'.$w_f_name.'-'.$w_l_name,'Lock Matched','REGIST-checker');
	}
	if($w_sts != '菊爆' && ($w_IP != '' && (strpos($host,$w_IP)!==FALSE || strpos($w_IP,$host)!==FALSE)) && $host != $pref['SubS']){#同一ID or 同姓同名?
		ERROR('角色的多线程登录禁止。菊花痒的话请咨询馆里猿。COMMENT:USED SAME PC HOST:'.$host,'Host Matched','REGIST-checker');
	}
}
}
#===============#
# ■ メイン		#
#===============#
function MAIN($REG_ERR=''){
global $in,$pref;
$hostchk = 0;

global $host,$icon_name,$icon_check1,$icon_check2,$icon_check3,$icon_check4;

#if(($pref['SubServer'])&&($host == "209.137.141.2")){$hostchk=1;$host = $in['IPAdd'];if($in['IPAdd'] == ""){ERROR("慶応生はTOP PAGEからアクセスしてください","","REGIST",__FUNCTION__,__LINE__);}}
#if($host == "209.137.141.2"){$hostchk=1;}
checker();
$REGIST=1;
HEAD();

if($pref['MOBILE']){

	if($pref['mobile_regist']){//携帯禁止
		ERROR('禁止从手机登录中。','registration from mobile phone is forbidden','REGIST',__FUNCTION__,__LINE__);
	}else{
?>
<big>转入手续</big><BR>
<BR>
你就是传说中的炎之转校生吗？我是班主任宫小路瑞穗。<BR>学生们都称呼我「姐姐大人」呢。<BR>啊，先不要管这些事情了。<BR><BR>总之在这里填上姓名和性别，<BR>然后交给我好吗？
<?=$REG_ERR?>
<FORM METHOD="get"  ACTION="regist.php" name="Regist">
<INPUT TYPE="HIDDEN" NAME="mode" VALUE="regist">
<?/*<!--INPUT TYPE="HIDDEN" NAME="IP" VALUE="$host"-->*/?>
姓：<INPUT size="10" type="text" name="F_Name" maxlength="5" value="<?=$in['F_Name']?>"><BR>
名：<INPUT size="10" type="text" name="L_Name" maxlength="5" value="<?=$in['L_Name']?>"><BR>
（姓,名最多全角5个字）<BR>
<BR>
アイコン：<SELECT name="Icon">
<?
	}
}else{
	REG_HEAD();
?>
<center><font color="#FF0000" face="ＭＳ Ｐ明朝" size="6"><span id="BR" style="width:100%;filter:blur(add=1,direction=135,strength=9):glow(strength=5,color=gold); font-weight:700; text-decoration:underline">转学手续</span></font></center>
<BR><BR><img border="0" src="<?=$pref['imgurl']?>/i_mizuho.jpg" width="70" height="70"><BR><BR>
你就是传说中的炎之转校生吗？我是班主任宫小路瑞穗。<BR>学生们都称呼我「姐姐大人」呢。<BR>啊，先不要管这些事情了。<BR><BR>总之在这里填上姓名和性别，<BR>然后交给我好吗？
<?=$REG_ERR?>
<form method="post" action="regist.php" name="Regist">
<INPUT TYPE="HIDDEN" NAME="mode" VALUE="regist">
<?/*<!--INPUT TYPE="HIDDEN" NAME="IP" VALUE="$host"-->*/?>
姓：<INPUT size="10" type="text" name="F_Name" maxlength="8" value="<?=$in['F_Name']?>"><BR>
名：<INPUT size="10" type="text" name="L_Name" maxlength="8" value="<?=$in['L_Name']?>"><BR>
（姓,名分别最多8个字）<BR>
<BR>
<div id="SubMenu" class="SubMenu" height="70"><img src="<?=$pref['imgurl']?>/question.gif" ALIGN=middle alt="Question"></div>
头像：<SELECT name="Icon" onchange="Mover()">
<?
}
?>
<OPTION value="NOICON">　-　头像　-<BR><OPTION value="NOICON">　　男子<BR>
<?for($i=1;$i<$icon_check1;$i++){echo '<OPTION value="'.$i.'"';if($i==$in['Icon']){echo ' selected';}echo '>'.$icon_name[$i].'<BR>';}?>
<OPTION value="NOICON">　　女子<BR>
<?for($i;$i<$icon_check2;$i++){echo '<OPTION value="'.$i.'"';if($i==$in['Icon']){echo ' selected';}echo '>'.$icon_name[$i].'<BR>';}?>
<OPTION value="NOICON">　　NPC<BR>
<?for($i;$i<$icon_check3;$i++){echo '<OPTION value="'.$i.'"';if($i==$in['Icon']){echo ' selected';}echo '>'.$icon_name[$i].'<BR>';}?>
<OPTION value="NOICON">　　特殊<BR>
<?for($i;$i<$icon_check4;$i++){echo '<OPTION value="'.$i.'"';if($i==$in['Icon']){echo ' selected';}echo '>'.$icon_name[$i].'<BR>';}?>
</SELECT><BR>[<a href="regist.php?mode=icon" target="_blank">头像一览</a>]<BR><BR>
社团：<SELECT name="club1">
<OPTION value="NOCLB" selected>　　　【第一志愿】<BR>
<?for($i=0;$i<count($pref['clb'])-2;$i++){echo '<OPTION value="'.$i.'"';if(isset($in['club1']) && $in['club1']!='NOCLB' && $i==$in['club1']){echo ' selected';}echo '>'.$pref['clb'][$i].'<BR>';}?>
</SELECT><BR>
社团：<SELECT name="club2">
<OPTION value="NOCLB" selected>　　　【第二志愿】<BR>
<?for($i=0;$i<count($pref['clb'])-2;$i++){echo '<OPTION value="'.$i.'"';if(isset($in['club2']) && $in['club2']!='NOCLB' && $i == $in['club2']){echo ' selected';}echo '>'.$pref['clb'][$i].'<BR>';}?>
</SELECT><BR>
社团：<SELECT name="club3">
<OPTION value="NOCLB" selected>　　　【第三志愿】<BR>
<?for($i=0;$i<count($pref['clb'])-2;$i++){echo '<OPTION value="'.$i.'"';if(isset($in['club3']) && $in['club3']!='NOCLB' && $i == $in['club3']){echo ' selected';}echo '>'.$pref['clb'][$i].'<BR>';}?>
</SELECT><BR><BR>
ID：<INPUT size="8" type="text" name="Id" maxlength="8" value="<?=$in['Id']?>">　Password：<INPUT size="8" type="text" name="Password" maxlength="8" value="<?=$in['Password']?>"><BR>
（ID,密码半角数字最多8个）<BR><BR>
<Table border="0" cellspacing="0" cellpadding="0">
<tr><td align="right">口癖：</td><td><INPUT size="32" type="text" name="Message" maxlength="32" value="<?=$in['Message']?>"></td></tr>
<tr><td align="center" colspan="2">（菊爆对方时的台词）</td></tr>
<tr><td align="right">遗言：</td><td><INPUT size="32" type="text" name="Message2" maxlength="32" value="<?=$in['Message2']?>"></td></tr>
<tr><td align="center" colspan="2">（自己爆菊时的台词）</td>
<tr><td align="right">签名：</td><td><INPUT size="32" type="text" name="Comment" maxlength="32" value="<?=$in['Comment']?>"></td></tr>
<tr><td align="center" colspan="2">（在生还者一览里记述。）<BR>（分别最多32个字）</td></tr>
</table><BR>
<FONT color="yellow" size="2"><B><U>
请不要登录多个用户。<BR>
一旦发现等着被塞抹布吧。<BR>
如果名字取得不和谐照样拖出去菊爆<br>
只有真心杀伐的人才能在此生存。<BR>
</U></B></FONT><BR>

<INPUT type="submit" name="Enter" value="开始"> 　<INPUT type="reset" name="Reset" value="重置"><BR></FORM></CENTER>
<P align="center"><B><a href="index.php">HOME</A></B></P>
<?
FOOTER();
}
#===============#
# ■ 登録処理	#
#===============#
function REGIST(){
#$host = $in['IP'];
checker();
global $in,$pref;

global $icon_check1,$icon_check2,$icon_check3,$icon_check4,$ar;
global $s_icon_pass;
$REG_ERR = '';
#入力情報チェック
if($in['F_Name'] == '')			{$REG_ERR .= '没姓<BR>';}
#if(preg_match("/\x8E[\xA6-\xDF]|\xA5[\xA1-\xF6]|\xA1[\xA6\xBC\xB3\xB4]/",$in['F_Name'])){$REG_ERR .= '姓にカタカナが含まれてます<BR>';}
#if(preg_match("/ア|イ|ウ|エ|オ|カ|キ|ク|ケ|コ|サ|シ|ス|セ|ソ|タ|チ|ツ|テ|ト|ナ|ニ|ヌ|ネ|ノ|ハ|ヒ|フ|ヘ|ホ|マ|ミ|ム|メ|モ|ヤ|ユ|ヨ|ラ|リ|ル|レ|ロ|ワ|オ|ン/",$in['F_Name']))
#								{$REG_ERR .= '姓にカタカナが含まれてます<BR>';}
if(strlen($in['F_Name']) > 24)	{$REG_ERR .= '姓太长（全角8个字以下）<BR>';}
if(preg_match("/ |\_|\,|\;|\<|\>|\(|\)|&|\/|\.|\~/",$in['F_Name']))
								{$REG_ERR .= '姓里不能有空格和特殊符号<BR>';}
if($in['L_Name'] == '')			{$REG_ERR .= '没名<BR>';}
#if(preg_match("/\x8E[\xA6-\xDF]|\xA5[\xA1-\xF6]|\xA1[\xA6\xBC\xB3\xB4]/",$in['L_Name'])){$REG_ERR .= '名にカタカナが含まれてます<BR>';}
#if(preg_match("/ア|イ|ウ|エ|オ|カ|キ|ク|ケ|コ|サ|シ|ス|セ|ソ|タ|チ|ツ|テ|ト|ナ|ニ|ヌ|ネ|ノ|ハ|ヒ|フ|ヘ|ホ|マ|ミ|ム|メ|モ|ヤ|ユ|ヨ|ラ|リ|ル|レ|ロ|ワ|オ|ン/",$in['L_Name']))
#								{$REG_ERR .= '名にカタカナが含まれてます<BR>';}
if(strlen($in['L_Name']) > 24)	{$REG_ERR .= '名太长（全角8个字以下）<BR>';}
if(preg_match("/ |\_|\,|\;|\<|\>|\(|\)|&|\/|\.|\~/",$in['L_Name']))
								{$REG_ERR .= '姓里不能有空格和特殊符号<BR>';}
if($in['Sex'] == "NOSEX")		{$REG_ERR .= '没选性別<BR>';}
if(preg_match("/NOICON/",$in['Icon']))		{$REG_ERR .= '没选头像<BR>';}
if($in['Icon'] == 0)			{$REG_ERR .= '没选头像<BR>';}
elseif($in['Icon'] < $icon_check1){$in['Sex'] = '男子';}#アイコン自動選択
elseif($in['Icon'] < $icon_check2){$in['Sex'] = '女子';}#アイコン自動選択
elseif($in['Icon'] < $icon_check3){
	if($in['SPass'] != $pref['a_pass_npc']){$REG_ERR .= '选了馆里猿用头像<BR>';}#管理用
}elseif($in['Icon'] < $icon_check4){#特殊アイコン
	if(!preg_match("/男子|女子/",$in['Sex'])){$REG_ERR .= '特殊头像要选性别<BR>';}
	if(($in['SPass'] != $s_icon_pass[$in['Icon'] - $icon_check3])||($s_icon_pass[$in['Icon'] - $icon_check3] == '')){$REG_ERR .= '特殊头像密码不吻合<BR>';}
}else{ERROR('Internal Server Error','Abnormal Icon ID','REGIST',__FUNCTION__,__LINE__);}
if($in['club'] == "NOCLB")		{$REG_ERR .= '没选部<BR>';}
if($in['Id'] == '')				{$REG_ERR .= '没输入ID<BR>';}
if(strlen($in['Id']) < 4)		{$REG_ERR .= 'ID字不够（半角4个字以上）<BR>';}
if(strlen($in['Id']) > 16)		{$REG_ERR .= 'ID字太多（半角16个字以下）<BR>';}
if(preg_match("/[^a-zA-Z0-9]+/",$in['Id'])){$REG_ERR .= 'ID只能输入半角英文数字（半角英数8个字以内）<BR>';}
if(preg_match("/2YBRU/",$in['Id']))	{$REG_ERR .= 'ID中有禁用文字<BR>';}
if($in['Password'] == '')		{$REG_ERR .= '没输入密码<BR>';}
if(strlen($in['Password']) < 4)	{$REG_ERR .= '密码不够长（半角4个字以上）<BR>';}
if(strlen($in['Password']) > 16)	{$REG_ERR .= '密码太长（半角16个字以下）<BR>';}
if(preg_match("/[^a-zA-Z0-9]+/",$in['Password'])){$REG_ERR .= '密码只能输入半角英文数字（半角英数8个字以内）<BR>';}
if(preg_match("/2YBRU/",$in['Password'])){$REG_ERR .= '密码中有禁用文字<BR>';}
if(preg_match("/$in[Id]/",$in['Password'])||preg_match("/$in[Password]/",$in['Id'])){$REG_ERR .= 'ID和密码不能相同<BR>';}
if(strlen($in['Message']) > 96)	{$REG_ERR .= '口癖字数超了（全角32个字以内）<BR>';}
if(strlen($in['Message2']) > 96){$REG_ERR .= '遗言字数超了（全角32个字以内）<BR>';}
if(strlen($in['Comment']) > 96)	{$REG_ERR .= '签名字数超了（全角32个字以内）<BR>';}
if($in['Message'] == '')		{$REG_ERR .= '请输入口癖（全角32个字以内）<BR>';}
if($in['Message2'] == '')		{$REG_ERR .= '请输入遗言（全角32个字以内）<BR>';}
if($in['Comment'] == '')		{$REG_ERR .= '请输入签名（全角32个字以内）<BR>';}
#エラー出力
if($REG_ERR != ''){
	MAIN('<br><br><font color="red">'.$REG_ERR.'</font>');
	exit;
}
#ユーザーファイル取得
$userlist = @file($pref['user_file']) or ERROR('unable to open file user_file','','REGIST',__FUNCTION__,__LINE__);
#同姓同名同一ID？
foreach ($userlist as $userlist2){
	list($w_id,$w_password,$w_f_name,$w_l_name,$w_sex,$w_cl,$w_no,$w_endtime,$w_att,$w_def,$w_hit,$w_mhit,$w_level,$w_exp,$w_sta,$w_wep,$w_watt,$w_wtai,$w_bou,$w_bdef,$w_btai,$w_bou_h,$w_bdef_h,$w_btai_h,$w_bou_f,$w_bdef_f,$w_btai_f,$w_bou_a,$w_bdef_a,$w_btai_a,$w_tactics,$w_death,$w_msg,$w_sts,$w_pls,$w_kill,$w_icon,$w_item[0],$w_eff[0],$w_itai[0],$w_item[1],$w_eff[1],$w_itai[1],$w_item[2],$w_eff[2],$w_itai[2],$w_item[3],$w_eff[3],$w_itai[3],$w_item[4],$w_eff[4],$w_itai[4],$w_item[5],$w_eff[5],$w_itai[5],$w_log,$w_com,$w_dmes,$w_bid,$w_club,$w_money,$w_wp,$w_wg,$w_wn,$w_wc,$w_wd,$w_comm,$w_limit,$w_bb,$w_inf,$w_ousen,$w_seikaku,$w_sinri,$w_item_get,$w_eff_get,$w_itai_get,$w_teamID,$w_teamPass,$w_IP,) = explode(",", $userlist2);
	if(($in['Id'] == $w_id)||(($in['F_Name'] == $w_f_name)&&($in['L_Name'] == $w_l_name))){ERROR("已经存在相同ID或相同姓名的角色。","","REGIST",__FUNCTION__,__LINE__);}#同一ID or 同姓同名?
}

#支給武器ファイル
$weplist = @file($pref['wep_file']) or ERROR('unable to open file wep_file','','REGIST',__FUNCTION__,__LINE__);

#私物ファイル
$stitemlist = @file($pref['stitem_file']) or ERROR('unable to open file stitem_file','','REGIST',__FUNCTION__,__LINE__);

#生徒番号ファイル
$memberlist = @file($pref['class_file']) or ERROR('unable to open file class_file','','REGIST',__FUNCTION__,__LINE__);

list($m,$f,$mc,$fc) = explode(",",$memberlist[0]);

#性別人数チェック
global $cl,$no;
if($in['Sex'] == '男子'){
	if($mc >= count($pref['clas'])){ERROR('男生太多了！','Too much boys','REGIST',__FUNCTION__,__LINE__);}#登録不可能？
	$m+=1;$no=$m;$cl=$pref['clas'][$mc];
	if($m >= $pref['manmax']){$m=0;$mc+=1;}#クラス更新？
}else{
	if($fc >= count($pref['clas'])){ERROR('女生太多了！','Too much girls','REGIST',__FUNCTION__,__LINE__);}#登録不可能？
	$f+=1;$no=$f;$cl=$pref['clas'][$fc];
	if($f >= $pref['manmax']){$f=0;$fc+=1;}#クラス更新？
}

#生徒番号ファイル更新
$memberlist=$m.','.$f.','.$mc.','.$fc.",\n";
$handle = @fopen($pref['class_file'],'w') or ERROR('unable to open file class_file','','REGIST',__FUNCTION__,__LINE__);
if(!@fwrite($handle,$memberlist)){ERROR('cannot write to class_file','','REGIST',__FUNCTION__,__LINE__);}
fclose($handle);

#初期配布武器リスト取得
$index = rand(0,count($weplist)-1);
list($w_wep,$w_att,$w_tai) = explode(',',$weplist[$index]);

#私物アイテムリスト取得
$index = rand(0,count($stitemlist)-1);
list($st_item,$st_eff,$st_tai) = explode(',',$stitemlist[$index]);
$index = rand(0,count($stitemlist)-1);
list($st_item2,$st_eff2,$st_tai2) = explode(',',$stitemlist[$index]);

#所持品初期化
for ($i=0; $i<6; $i++){$item[$i] = '无'; $eff[$i]=$itai[$i]=0;}

#初期能力
$rand = rand(0,15);
$att = 95 + $rand;
$def = 105 - $rand;#155
$hit = 500;
$mhit = $hit;$kill=0;$sta = $pref['maxsta'];$level = 1;
#経験値
$exp=0;
$reg_exp = $ar;
if ($reg_exp >= 4){$reg_exp -= 4;$exp = ($ar * 10);}

$death=$msg = '';
$sts = '正常';
$pls = 0;
$tactics = '通常';
$endtime = 0;
$log=$dmes=$bid=$inf = '';

#初期アイテム＆初期配布武器
$item[0] = '面包<>HH'; $eff[0] = 25; $itai[0] = 4;
$item[1] = '水<>SH'; $eff[1] = 25; $itai[1] = 4;
$item[2] = $w_wep; $eff[2] = $w_att; $itai[2] = $w_tai;

$wep = '空手<>WP';
$watt = 0;
$wtai = '∞';

if ($in['Sex'] == '男子'){$bou = '阿部做的小衣服<>DBN';}
else {$bou = '瑞穗的连衣裙<>DBN';}

list($b_name,$b_namea) = explode('<>',$bou);

list($w_name,$w_kind) = explode('<>',$w_wep);


$bdef = 20;
$btai = 30;

$bou_f = '皮靴<>DF';
$bdef_f = 10;
$btai_f = 15;

$bou_h = $bou_a = '无';
$bdef_h = $bdef_a = 0;
$btai_h = $btai_a = 0;

#弾又は矢支給
if(preg_match('/<>WG/',$w_wep)){$item[3] = '子弹<>Y'; $eff[3] = 10; $itai[3] = 1;$item[4] = $st_item; $eff[4] = $st_eff; $itai[4] = $st_tai;}#弾
elseif(preg_match('/<>WA/',$w_wep)){$item[3] = '箭<>Y'; $eff[3] = 12; $itai[3] = 1;$item[4] = $st_item; $eff[4] = $st_eff; $itai[4] = $st_tai;}#矢
else{$item[3] = $st_item; $eff[3] = $st_eff; $itai[3] = $st_tai;$item[4] = $st_item2; $eff[4] = $st_eff2; $itai[4] = $st_tai2;}

CLUBMAKE();	#クラブ作成
global $wp,$wg,$wn,$wc,$wd,$comm,$limit,$bb,$host;

#User File
$money = 10;//mt_rand(10,20);//10
$seikaku = '无';#seikaku
$sinri = '无';#sinri
$teamID = '无';#TeamID
$teamPass = '无';#TeamPass
$ousen = '通常';#Ousen
$item_get = '无';
$eff_get = 0;
$itai_get = 0;

$in['IP'] = $host;

$newuser = $in['Id'].','.$in['Password'].','.$in['F_Name'].','.$in['L_Name'].','.$in['Sex'].",$cl,$no,$endtime,$att,$def,$hit,$mhit,$level,$exp,$sta,$wep,$watt,$wtai,$bou,$bdef,$btai,$bou_h,$bdef_h,$btai_h,$bou_f,$bdef_f,$btai_f,$bou_a,$bdef_a,$btai_a,$tactics,$death,".$in['Message'].",$sts,$pls,$kill,".$in['Icon'].",$item[0],$eff[0],$itai[0],$item[1],$eff[1],$itai[1],$item[2],$eff[2],$itai[2],$item[3],$eff[3],$itai[3],$item[4],$eff[4],$itai[4],$item[5],$eff[5],$itai[5],$log,".$in['Comment'].",".$in['Message2'].",$bid,".$in['club'].",$money,$wp,$wg,$wn,$wc,$wd,$comm,$limit,$bb,$inf,$ousen,$seikaku,$sinri,$item_get,$eff_get,$itai_get,$teamID,$teamPass,".$in['IP'].",\n";

#New User Save
$handle = @fopen($pref['user_file'], 'a') or ERROR('unable to open file user_file','','REGIST',__FUNCTION__,__LINE__);
if(!@fwrite($handle,$newuser)){ERROR('cannot write to user_file','','REGIST',__FUNCTION__,__LINE__);}
fclose($handle);


# ホスト判別
$host1 = $_SERVER['REMOTE_ADDR'];
if($pref['host_view'] == 1){
	$host = $host1;
}elseif($pref['host_view'] == 2){
	$host2 = gethostbyaddr($host1);
	if(($host1 == $host2)||($host1 == '')){$host = $host2;}
	else{$host = $host1.'-'.$host2;}
}else{
	$host = '';
}

#New User Log
LOGSAVE('NEWENT');

$id=$in['Id'];$password=$in['Password'];

CSAVE();	#Cokie Save

HEAD();

?>
<center><B><FONT color="#ff0000" size="+3" face="ＭＳ 明朝">转学手续结束</FONT></B><BR><BR></center>
<TABLE border="1" cellspacing="0">
  <TBODY>
	<TR><TD class="b1">班级</TD><TD colspan="3" class="b3"><?=$cl?></TD></TR>
	<TR><TD class="b1">姓名</TD><TD colspan="3" class="b3"><?=$in['F_Name'].' '.$in['L_Name']?></TD></TR>
	<TR><TD class="b1">学号</TD><TD colspan="3" class="b3"><?=$in['Sex'].$no?>号</TD></TR>
	<TR><TD class="b1">社团</TD><TD colspan="3" class="b3"><?=$pref['clb'][$in['club']]?></TD></TR>
	<TR><TD class="b1">生命</TD><TD class="b3"><?=$hit.'/'.$mhit?></TD><TD width="39" class="b1">体力</TD><TD class="b3"><?=$sta?></TD></TR>
	<TR><TD class="b1">攻击力</TD><TD class="b3"><?=$att?></TD><TD class="b1">武器</TD><TD class="b3"><?=$w_name?></TD></TR>
	<TR><TD class="b1">防御力</TD><TD class="b3"><?=$def?></TD><TD class="b1">防具</TD><TD class="b3"><?=$b_name?></TD></TR>
  </TBODY>
</TABLE>
<P align="center">
<?=$in['F_Name'].' '.$in['L_Name']?>
<?

if ($in['Sex'] == '男子') {echo '君';}
else {echo '小姐';}

?>怎样？<BR>
虽然刚刚转校，不过明天就是修学旅行了。<BR><BR>
绝对不能迟到哦！<BR><BR>

<FORM METHOD="POST"  ACTION="regist.php" style="MARGIN: 0px">
<INPUT TYPE="HIDDEN" NAME="mode" VALUE="info">
<INPUT TYPE="HIDDEN" NAME="Id" VALUE="<?=$in['Id']?>">
<INPUT TYPE="HIDDEN" NAME="Password" VALUE="<?=$in['Password']?>">
<center>
<INPUT type="submit" name="Enter" value="出发！所罗门我回来了！！">
</center>
</FORM>

</P>
<?
FOOTER();
}

#===============#
# ■ 説明処理	#
#===============#
function INFO(){
global $in;

HEAD();

?>
<center><font color="#FF0000" face="ＭＳ Ｐ明朝" size="6"><span id="BR" style="width:100%;filter:blur(add=1,direction=135,strength=9):glow(strength=5,color=gold); font-weight:700; text-decoration:underline">注册完成</span></font></center><BR>
<P align="center">
当我醒来的时候，已经发觉自己身在像教室的地方。明明应该去修学旅行的…。<BR>
「对了，今天在去修学旅行的巴士上突然睡着了…」<BR>
环顾周围，其他的同学也在。转动身子，突然感觉菊花一紧<BR>
自己用手摸了摸，感觉到那冰冷金属的恶寒，却根本拔不出来。<BR>
大家和我一样吧，菊花里都被固定上了栓状物体。<BR><BR>
突然，从前门走进一个人，身后有拿着军用步枪的士兵随从…。<BR><BR>
<img border="0" src="img/i_ziwei.jpg" width="70" height="70"><BR><BR>
「大家好，我是一年级时候的班主任四大皆控滋味菌。这回又和B班的同学们见面了，请多指教哦<BR>
怎样，要不是宫小路瑞穗那个丑男人菊爆我的照片和我和风见学园校长啪啪啪的照片被他拿来要挟我，当初我才不辞职呢。<BR>
所以，和伟大的国家领导人阿部商量过后制定了这个法律。<BR><BR>
<p align="center"><font color="#FF0000" face="ＭＳ Ｐ明朝" size="6">
<span id="BR" style="width:100%;filter:blur(add=1,direction=135,strength=9):glow(strength=5,color=gold); font-weight:700; text-decoration:underline">
■ 饭否大杀基 ■</span></font></p><BR>
那么大家，一起来夺取对方的血肉吧。<BR>
如果抵抗、拔出菊栓、尝试逃跑的话菊栓可是会炸的哟。<BR><BR>
直到剩下最后一人。<BR>
不允许异议存在。<BR><BR>
啊啊，老师忘了说了，这里是初音岛。<BR><BR>
听到了没，这是我曾经就职的分校。<BR>
我就在附近的某处看着你们努力吧。<BR><BR>
那么，从这里出去到哪里都没问题。<BR>
毎天8点起床(0時和8時和16時)，将会进行全岛放送。一天三回啊。<BR><BR>
大家按照地图就可以知道哪里危险了。<BR>
以最快的速度离开这里<BR><BR>
要说为什么，那个菊栓可是会爆炸的。<BR><BR>
对了还有时间显示。<B><font color="yellow">一周</font></B>足够了吧。<BR><BR>
程序如果发现24小时内没有任何人菊爆的话、<BR>
只要时间到，不管多少人我们都会启动电脑，<BR>
将剩余所有人的菊栓引爆<u>优胜者自然不存在</u>。<BR><BR>
为什么会这样？<BR>
是你们的原因哦　你们，<B>看不起大人吧</B>　那就看不起好了<BR>
不过给我记住孩子们　<BR><BR>
<Font size="+1" color="red"><B><i>『人生是一场游戏。　　　　　　　　　　　　　　　　<font color="black">.</font><BR>大家都在拚死得战斗求得生存。<BR>　　　　　　　　　　成为有价值的大人吧！』</i></B></Font><BR><BR>
那么，每个人拿着背包离开教室吧。<BR><BR>
对了，已经和父母说过了所以安心菊爆吧」<BR><BR>
<FORM METHOD="POST"  ACTION="BR.php" style="MARGIN: 0px">
<INPUT TYPE="HIDDEN" NAME="mode" VALUE="main">
<INPUT TYPE="HIDDEN" NAME="Id" VALUE="<?=$in['Id']?>">
<INPUT TYPE="HIDDEN" NAME="Password" VALUE="<?=$in['Password']?>">
<INPUT type="submit" name="Enter" value="离开教室">
</center>
</FORM>
<?
FOOTER();
}
#===================#
# ■ アイコン一覧	#
#===================#
function ICON(){
global $pref;
global $icon_file,$icon_name;
HEAD();
?>
<center><hr width="75%">
<b><big>头像</big></b>
<P><small>- 当前登录头像如下 -</small>
<hr width="75%">
<?
if(!$pref['MOBILE']){
	echo '<P><table border="1" cellpadding="0" cellspacing="0"><tr>';
}

foreach($icon_file as $j => $icon_list){
	if($j != 0){
		if($pref['MOBILE']){
			print '<img src="'.$pref['imgurl'].'/'.$icon_list.'"><BR>'.$icon_name[$j].'<br>';
		}else{
			print '<th class="b3" width="70"><img src="'.$pref['imgurl'].'/'.$icon_list.'" ALIGN="middle" alt="'.$icon_name[$j].'" height="70" width="70"><BR>'.preg_replace('/ /','<br>',$icon_name[$j],1).'</th>';
		}
	}
	if($j%10==0 && !$pref['MOBILE']){print '</tr><tr>';}
}

if(!$pref['MOBILE']){
	while($j%10!=0){print '<th class="b3" width="70">&nbsp;</th>';$j++;}
	echo '</tr></table><p><FORM><INPUT TYPE="button" VALUE="  CLOSE  " onClick="top.close();"></FORM>';
}

?>
</center>
</body>
</html>
<?
FOOTER();
exit;
}
#===================#
# ■ クラブ作成		#
#===================#
function CLUBMAKE(){
global $pref,$in;

global $wp,$wg,$wn,$wc,$wd;
$wp=$wg=$wn=$wc=$wd=0;
#殴  銃  剣  投  爆
$dice = rand(0,100);
#フルシーズン	25 50 75%
#シーズン		15 30 45%
if($in['club1']==$in['club2']){
	$in['club2']='';
}
if($in['club1']==$in['club3']){
	$in['club3']='';
}
if($in['club2']==$in['club3']){
	$in['club3']='';
}

if($in['club1']==''){
	if($in['club2']!=''){$in['club1'] = $in['club2'];$in['club2']='';}
}
if($in['club2']==''){
	if($in['club3']!=''){$in['club2'] = $in['club3'];$in['club3']='';}
}
if($in['club1']==''){$in['club1'] = rand(0,29);}
if($in['club2']==''){$in['club2'] = rand(0,29);}
if($in['club3']==''){$in['club3'] = rand(0,29);}

if($in['club'] <= 17 && $dice < 25){#フルシーズン
	$in['club']=$in['club1'];
}elseif($in['club'] > 17 && $dice < 15){#シーズン
	$in['club']=$in['club1'];
}elseif($in['club'] <= 17 && $dice < 50){
	$in['club']=$in['club2'];
}elseif($in['club'] > 17 && $dice < 30){
	$in['club']=$in['club2'];
}elseif($in['club'] <= 17 && $dice < 75){
	$in['club']=$in['club3'];
}elseif($in['club'] > 17 && $dice < 45){
	$in['club']=$in['club3'];
}else{
	$in['club'] = rand(0,29);
}

if($in['club'] >= 28){$in['club'] = rand(0,29);}

if		($in['club'] == 0)	{$wp = 20;}	#空手部
elseif	($in['club'] == 1)	{$wn = 20;}	#剣道部
elseif	($in['club'] == 2)	{;}	#柔道部
elseif	($in['club'] == 3)	{;}	#料理部
elseif	($in['club'] == 4)	{;}	#アート部
elseif	($in['club'] == 5)	{;}	#茶道部
elseif	($in['club'] == 6)	{;}	#ロック部
elseif	($in['club'] == 7)	{;}	#オーケストラ部
elseif	($in['club'] == 8)	{;}	#ボランティア部
elseif	($in['club'] == 9)	{;}	#ブラスバンド部
elseif	($in['club'] == 10)	{;}	#釣り部
elseif	($in['club'] == 11)	{;}	#コーラス部
elseif	($in['club'] == 12)	{$wc = 20;}	#将棋部
elseif	($in['club'] == 13)	{;}	#福沢諭吉研究会
elseif	($in['club'] == 14)	{;}	#書道部
elseif	($in['club'] == 15)	{;}	#囲碁部
elseif	($in['club'] == 16)	{;}	#西洋ボード部
elseif	($in['club'] == 17)	{;}	#ZION
elseif	($in['club'] == 18)	{;}	#バスケ部
elseif	($in['club'] == 19)	{;}	#サッカー部
elseif	($in['club'] == 20)	{$wc=$wp=10;}	#野球部
elseif	($in['club'] == 21)	{$wg = 20;}	#テニス部
elseif	($in['club'] == 22)	{$wc = 20;}	#水泳部
elseif	($in['club'] == 23)	{$wp = 20;}	#ラグビー部
elseif	($in['club'] == 24)	{;}	#ラクロス部
elseif	($in['club'] == 25)	{;}	#バレー部
elseif	($in['club'] == 26)	{;}	#クロスカウントリー部
elseif	($in['club'] == 27)	{$wp=$wg=$wn=$wc=$wd=10;}	#放送部
elseif	($in['club'] == 28)	{$wp=$wg=$wn=$wc=$wd=20;}	#Computer Help Desk
elseif	($in['club'] == 29)	{$wp=$wg=$wn=$wc=$wd=30;}	#Computer Task Force
else	{CLUBMAKE();}
}
#===================#
# ■ 登録ヘッダー部	#
#===================#
function REG_HEAD(){
global $pref;
global $icon_check1,$icon_check2,$icon_check3,$icon_check4,$icon_file,$icon_name;
?>
<script language="javascript" type="text/javascript">
<!--
var brw_v = navigator.appVersion.charAt(0);
var brw_n = navigator.appName.charAt(0);
var iIE4 = false;
var iNN4 = false;
if((brw_v >= 4)&&(brw_n == "M"))iIE4 = true;
if((brw_v >= 4)&&(brw_n == "N"))iNN4 = true;

var comments = new Array();
comments[0] = '<img src="<?=$pref['imgurl']?>/question.gif" ALIGN="middle" alt="Question">';
comments[1] = '<img src="<?=$pref['imgurl']?>/question.gif" ALIGN="middle" alt="Question">';
<?

for ($i=0;$i<$icon_check1;$i++){
	$j=$i+1;
	print 'comments['.$j.'] = \'<img src="'.$pref['imgurl'].'/'.$icon_file[$i].'" ALIGN="middle" alt="'.$icon_name[$i].'">\';';
}
$j++;
print 'comments['.$j.'] = \'<img src="'.$pref['imgurl'].'/question.gif" ALIGN="middle" alt="Question">\';';
for ($i;$i<$icon_check2;$i++){
	$j=$i+2;
	print "comments[$j] = '<img src=\"".$pref['imgurl']."/$icon_file[$i]\" ALIGN=middle alt=\"$icon_name[$i]\">';\n";
}
$j++;print "comments[$j] = '<img src=\"".$pref['imgurl']."/question.gif\" ALIGN=middle alt=\"Question\">';\n";
for ($i;$i<$icon_check3;$i++){$j=$i+3;print "comments[$j] = '<img src=\"".$pref['imgurl']."/$icon_file[$i]\" ALIGN=middle alt=\"$icon_name[$i]\"><BR>性別：<SELECT name=\"Sex\"><OPTION value=\"NOSEX\" selected>- 性別 -<BR><OPTION value=\"男子\">男子<BR><OPTION value=\"女子\">女子<BR></SELECT><BR>特殊图标密码：<INPUT size=\"8\" type=\"text\" name=\"SPass\" maxlength=\"16\"> ';\n";}
$j++;print "comments[$j] = '<img src=\"".$pref['imgurl']."/question.gif\" ALIGN=middle alt=\"Question\">';\n";
for ($i;$i<$icon_check4;$i++){$j=$i+4;print "comments[$j] = '<img src=\"".$pref['imgurl']."/$icon_file[$i]\" ALIGN=middle alt=\"$icon_name[$i]\"><BR>性別：<SELECT name=\"Sex\"><OPTION value=\"NOSEX\" selected>- 性別 -<BR><OPTION value=\"男子\">男子<BR><OPTION value=\"女子\">女子<BR></SELECT><BR>特殊图标密码：<INPUT size=\"8\" type=\"text\" name=\"SPass\" maxlength=\"16\"> ';\n";}

?>
function Mover(){
	ChkNum(document.Regist.Icon.selectedIndex);
}
function ChkNum(n){
	//var lay = new Array();

	Com = "<div class=Style>" + comments[n] + "<\/div>";

	if(document.all){
		obj1=document.all["SubMenu"];
	}else if(document.layers){
		obj1=document.layers["SubMenu"];
	}else if(document.getElementById){
		obj1=document.getElementById("SubMenu");
	}
	obj1.innerHTML=Com;
}
// -->
</SCRIPT>
<?
}
?>