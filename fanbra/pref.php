<?php
########################################
## BATTLE ROYALE ULTIMATE PHP SCRIPT  ##
##  (C) 2008 by 2Y -Yuta Yamashita-   ##
##  E-MAIL: info@b-r-u.net            ##
##  MSN:    yuta@users.sourceforge.jp ##
##  Yahoo:  YutaYama2Y                ##
##  ICQ:    280839763                 ##
##  HP:     http://www.b-r-u.net/     ##
##■ ゲームバージョン※変更厳禁※     ##
$pref['ver'] = 'V01.19';              ##
$pref['ver2y'] = '4.33';              ##
#$pref['ver'] = 'V01.19';             ##
#$pref['ver2y'] = '2.51';             ##
##################################################
## BATTLE ROYALE CGI                            ##
##   (C) 2000 by Happy Ice.                     ##
##   E-MAIL: webmaster@happy-ice.com            ##
##   HomePage: http://www.happy-ice.com/battle/ ##
#######################################################################################################
##+----[注意事項]-----------------------------------------------------------------------------------+##
##| 1. このスクリプトを使用したいかなる損害に対して作者は一切の責任を負いません。                   |##
##|                                                ~~~~~~~~~~~~~~~~~~~~~~~~~~~~                     |##
##| 2. 表示の際のTABは4スペースです。TeraPadなどのテキストエディターで設定してやってください。      |##
##|              ~~~~~~~~~~~~~~                                                                     |##
##| 3. 設置に関する質問・改造に関する質問はサポート掲示板にお願いいたします。                       |##
##|      メールによる質問にはお答えできませんが、MSNによる質問にも答えられます。                    |##
##|                                              ~~~                                                |##
##| 4. 初歩的な質問につきましては、当ホームページにて簡単に説明してあります。                       |##
##|    ~~~~~~~~~~~~                                                                                 |##
##| 5. バグ報告、又は要望などがありましたら、ホームページの管理人掲示板に書き込んでください。       |##
##|                                                                                         - 2Y -  |##
##+-------------------------------------------------------------------------------------------------+##
#######################################################################################################

#------- 基本設定 ---------

#■ ゲームタイトル
$pref['game'] = '■ 饭否大杀基 ■ (Ver'.$pref['Ver0.5'].') ['.$pref['ver'].']';
#■ トップページ
#	連絡先（PC,携帯）
$pref['contact'] = array('<a href="http://fanfou.com/">http://fanfou.com/</a> MODDED by 饭否基合众','');
#	カウンター（カウンターをトップページの上部に表示する場合は入力）
$pref['counter'] = '';
#	連絡事項
$pref['information'] = '代理禁止，开多窗口的拖出去枪毙5分钟。';
#	フッター(</body>の直前に挿入)
$pref['footer'] = '';

#------- 管理人設定 ---------

#■ 管理者ID＆パスワード
# ユーザログ崩れの原因になるので、「,」（コンマ）は使用しないで下さい。
$pref['a_id'] = 'npc';# NPC使用時のIDとなりま;す。
$pref['a_pass'] = 'fanfou233';
$pref['a_pass_npc'] = 'npc233';#兵士のパスワード
$pref['a_group_id'] = 'npcgroup';#兵士のグループ名
$pref['a_group_pass'] = '233';#兵士のグループパスワード


#------- ファイル設定 ---------

#■ ディレクトリ(最後は/で閉じません)
#	データ覗き見防止のため必ず変更して下さい。
$pref['LOG_DIR'] = './log';
$pref['DAT_DIR'] = './dat';
$pref['LIB_DIR'] = './lib';
#■ リンク先(各CGIへのリンク・追加可能)
#	ステータス画面上部に表示されるリンクです。
$pref['links'] = '<A href="index.php">>>首页</A><A href="rule.php" target="_blank">>>说明</A><A href="rank.php" target="_blank">>>幸存者一览</A><A href="map.php" target="_blank">>>地图</A><A href="news.php" target="_blank">>>进行状况</A><A href="winner.php" target="_blank">>>历史优胜者</A><A href="admin.php" target="_blank">>>游戏管理</A><A href="http://fanfou.com" target="_blank">>>饭否</A>';
#■ 個別バックアップ（未使用）
#$pref['u_save_dir'] = $pref['LOG_DIR'].'/users/'; #ユーザ個別バックアップディレクトリ
#$pref['u_save_file'] = '_back.log'; #IDに付加する文字列
#■ ユーザファイル
#	データ覗き見防止のため $pref['LOG_DIR']/ 以下は必ず変更して下さい。
$pref['user_file'] = $pref['LOG_DIR'].'/userdatfile.log';
$pref['back_file'] = $pref['LOG_DIR'].'/userbackfile.log';
#■ 管理用ファイル
$pref['admin_file'] = $pref['LOG_DIR'].'/admin.log';
#■ 優勝者保存ファイル
$pref['win_file'] = $pref['LOG_DIR'].'/win.log';
#■ ログファイル
$pref['log_file'] = $pref['LOG_DIR'].'/newsfile.log';
#■ ロックファイル名
$pref['lockf'] = './lock/dummy.txt';
#■ ファイルロック形式（未使用）
#	→ 0=no 1=symlink関数 2=mkdir関数 3=Tripod用
#	ローカルテスト時にはmkdir関数を、サーバにアップするときには使用可能なら
#	symlink関数を使うようにします。
#$pref['lkey'] = 1;
#■ クラスファイル
$pref['class_file'] = $pref['LOG_DIR'].'/classfile.log';
#■ 禁止エリアファイル
$pref['area_file'] = $pref['LOG_DIR'].'/areafile.log';
#■ 支給武器ファイル
$pref['wep_file'] = $pref['DAT_DIR'].'/wepfile.dat';
#■ 私物アイテムファイル
$pref['stitem_file'] = $pref['DAT_DIR'].'/stitemfile.dat';
#■ 売店アイテムファイル
$pref['baiten_file'] = $pref['LOG_DIR'].'/baiten.log';
#■ 取得アイテムファイル
$pref['item_file'] = 'itemfile.log';
#■ 時間管理ファイル
$pref['time_file'] = $pref['LOG_DIR'].'/timefile.log';
#■ 銃声ログファイル
$pref['gun_log_file'] = $pref['LOG_DIR'].'/gunlog.log';
#■ 終了フラグ
$pref['end_flag_file'] = $pref['LOG_DIR'].'/e_flag.log';


#------- 基本配列設定 ---------

#■ 利用可能言語
$pref['language'] = array('ja'/*,'en'*/);
#■ 天気
$pref['weather'] = array('搅基快晴','头顶苍天','裸飘曇天','天气雨','春哥豪雨','斯巴达台风','永恒雾雨','忘却霭雪','浊白的雾','红魔馆的浓雾');
#■ クラブ
$pref['clb'] = array('空手道部','剑道部','柔道部','料理部','艺术部','茶道部','攀岩部','管弦乐部','公益福利部','军乐队部','钓鱼部','合唱部','将棋部','蓝蓝路研究会','书道部','围棋部','国际象棋部','基翁公国',
		'篮球部','足球部','棒球部','网球部','游泳部','橄榄球部','曲棍球部','排球部','定向越野部','放送部','Computer Help Desk','Computer Task Force','NPC');
#■ クラス名（ここに入力されるクラス名の数がクラス数になります。必要に応じて追加・削除してください。）
$pref['clas'] = array('3年A班','3年B班','3年C班');
#■ 性別毎最大数
$pref['manmax'] = 21;
#■ 最大登録数
$pref['maxmem'] = $pref['manmax'] * 2 * count($pref['clas']);
#■ 場所
$pref['place'] = array('绿坝娘中心','〇感沙滩','@HomeMate住宅区','CCAV','邮局','消防局','诚哥纪念堂','受寒温泉','喵喵神社','情侣宾馆废楼','绫女之丘','隧道','西村住宅街','好男人教廷','风见学园','博丽神社','悠久之森','YY混浴','南村住宅街','永琳医保定点单位','灯塔','〇感海茶');
#	SU=発見増 SD:発見減 DU:防御増 DD:防御減 AU:攻撃増 AD:攻撃減
$pref['arsts'] = array('SU','DD','DU','SU','SD','SU','AU','SU','SD','AD','SU','DD','DU','SD','AD','SD','SD','SD','AU','SU','DU','SU');
$pref['area'] = array('D-6','A-2','B-4','C-3','C-4','C-5','C-6','D-4','E-2','E-4','E-5','E-7','F-2','F-9','G-3','G-6','H-4','H-6','I-6','I-7','I-10','J-6');
$pref['arno'] = range(0,21);
$pref['arinfo'] = array(
	'绿坝娘中心，不过现在已经禁止入内。<BR>4000万大小姐居然在卖⊙…。',
	'大海上可以看到NiceBoat呢。<BR>不过还是不打扰船上的言叶和诚哥醉生梦死了…。（敬礼',
	'这条宁静的小街过去也有萝莉玩耍吧。<BR>只不过滋味这台推土机过后……',
	'这里是饭否基合体的中心。<BR>待久了会心神恍惚的……。',
	'这里，除了Loveletter就是邮寄好人卡的吧…。',
	'说到消防局，也就是变形消防车嘛。',
	'大大小小的诚哥造像矗立在这里。<BR>夜里可是死死团的集会中心呢。',
	'让受寒温泉带给你第一次的温暖。',
	'这里祭祀着喵喵神哟，传言确实能听到喵喵声。',
	'这里寄宿着数千万年前高端仿人智能XP的思考核心。<BR>该不会是幽灵吧。',
	'在这里能将这个岛一望无疑。<BR>当然，你想站在这里的话<BR>小心被中华大加农射成蜂窝煤。',
	'在这个地方。<BR>要是被夹击可就麻烦了。',
	'这里的人们也和其他住宅街一样，<BR>被滋味凌虐过了么…。',
	'好男人教廷。<BR>待太久的话菊花会爆炸的吧…。',
	'白天风见学园还是你的攻略场所<BR>夜晚的学校可就是化物语的摄影棚了哟。',
	'博丽神社的人都去飞上天了呢。',
	'这里寄宿着悠久的精灵<BR>听说永恒之剑在此现身…。',
	'这里的混浴真是充满了阴谋的味道。<BR>偷拍摄像机有很多吧…。',
	'这里比及其他地方受滋味的毒害不深。<BR>不过为什么墙壁像奶油一般被切开了呢。',
	'来到这里，<BR>毒药质量绝对让你满意…不开发票。',
	'这里是情侣们曾经告白过的灯塔。<BR>但为什么现在血染坡道了呢？',
	'这里不是NiceBoat…是优胜者离开的船么？<BR>啊，是海猫剧组的道具跑错棚了，切。',
);


#------- 基本プログラム設定 ---------

#■ プレイヤーのセーブデータ有効期限。日数で記入します。デフォルトで1週間。
$pref['save_limit'] = 7;
#■ レベルアップベース経験値
$pref['baseexp'] = 9;
#■ 熟練度ベース
$pref['BASE'] = 20;
#■ レベルアップ時のスタミナ回復値
$pref['lvup_sta'] = 100;
#■ 現金ベース
$pref['money_base'] = 100;
#■ プログラム最低開催日数
#	イベント・罠などで最後の一人が決定しても、この日数以下ならゲームが続行します。
#	0 にすると1日。// 单位已被改成小时
$pref['battle_limit'] = 1;
#■ プログラム受付締切日数（禁止エリア数）
#   ※ 仕組みが変わりました。（2は初期化してから8時間、3は初期化してから16時間、4は初期化してから24時間…）
$pref['plimit'] = 18;
#■ 携帯による登録を禁止 (1=禁止 0=許可)
$pref['mobile_regist']=0;
#■ スタミナ最大数
$pref['maxsta'] = 300;
#■ 応急処置コマンドの消費スタミナ
$pref['okyu_sta'] = 50;
#■ グループ脱退維持の消費スタミナ
$pref['team_sta'] = 100;
#■ 毒見コマンドの消費スタミナ
$pref['dokumi_sta'] = 30;
#■ アイテム譲渡時の消費スタミナ(譲渡する側される側)
$pref['jyouto_sta'] = 30;
$pref['jyouto_w_sta'] = 30;
#■ 回復量の設定
#	スタミナ回復時間(秒)：30秒で1ポイント回復
$pref['kaifuku_time'] = 2;
#	体力回復レート：スタミナの2分の1(0としない事)
$pref['kaifuku_rate'] = 2;
#■ チームの最大人数
$pref['team_max'] = 5;
#■ 過去優勝者表示 ON=1 OFF=0
$pref['oldwinner'] = 1;


#------- メッセンジャー設定 ---------

#■ メッセージの保存されるファイル名
$pref['mes_file'] = $pref['LOG_DIR'].'/mes.log';
#■ 保存されるメッセージの数
$pref['listmax'] = 1000;
#■ 表示されるメッセージの数
$pref['mesmax'] = 20;
#■ 表示文字数(bytes)
$pref['mes_break'] = 108;
#■ メンバー数保存ファイル
$pref['member_file'] = $pref['LOG_DIR'].'/member.log';
#■ メンバーがカウントされ続ける時間(sec)
$pref['mem_time'] = 60;
#■ メッセージの色
$pref['col_to'] = 'red';
#■ 送られたメッセージの色
$pref['col_from'] = 'blue';
#■ 全員に転送されたメッセージの色
$pref['col_all'] = 'green';


#------- セキュリティ関連設定 ---------

#■ 他サイトから投稿排除時(他サイトのリンク又はお気に入りから直接アクセスされたくない場合)に指定
$pref['base_url'] = 0;#排除機能を使用する場合1を設定
#				排除機能を有効かつ携帯電話からの排除機能をOFFにする場合は2を入力。
#				有効にする場合は$base_listにサイトのアドレスの入力が必須。
#■ 時刻（海外サーバの時は、$now=time - (13 * 60 * 60); 等してください。例：13時間差）
#	@base_listには入力を受け付けるアドレスを設定(http://から書く)・複数設定可
if		(!$pref['base_url'])							{$pref['now'] = time() + (8*60*60);}
elseif	($_SERVER['SERVER_NAME'] == 'anapple.org')		{$pref['now'] = time() + (8*60*60);$pref['base_list'] = 'anapple.orgt';}
else	{require $pref['LIB_DIR'].'/lib4.php';KICKOUT();}
#■ アクセス禁止について
#	ホスト名が取得出来ない場合は排除しています。
#	排除したくない場合は、アクセス了承ホストにIPアドレスを入れてください。
#	例：$pref['oklist'] = array("127.0.0");
#	こうすると、127.0.0.*** のIPアドレスは拒否されません。
#	アクセス禁止機能 (0=OFF 1=ON)
$pref['acc_for'] = 1;
#	アクセス禁止ホスト
$pref['kick'] = array('froute.jp','60.45.66.134',/*電光の γ*/'125.13.211.114',/*暗 鬼*/'221.82.75.16','jig.jp','xrea.com',/*mobaxy*/'210.131.5.205','coreserver.jp','sakura.ne.jp','124.97.123.183','yamaguchi.yamaguchi.ocn.ne.jp','arena.ne.jp','maido3.com','125.170.238.85','218.224.97.211','125.170.242.254','221.191.241.101','125.170.240.221','58.90.118.236','222.149.152.51','222.146.239.69','222.149.107.24','222.149.154.11','222.227.75.41','203.191.235.65','203.187.111.10','202.172.28.31','203.104.106.247','125.53.25.40','211.14.226.94','210.171.91.25','219.163.5.181','61.89.99.98','64.233.178.136','124.97.108.56','220.109.65.139','123.224.43.208','softbank220061037253.bbtec.net','220.20.206.161','219.179.126.37','221.47.148.10','221.186.126.115-gate.nafco.jp','router.hinet.net','dynamic.hinet.net','fks.ed.jp','221.189.59.128','ebix.net.tw','softbank219193012031.bbtec.net','58.156.158.60','219.162.114.79');
#	アクセス了承ホスト
$pref['oklist'] = array('2Y1');
#	method=POST 限定 (0=no 1=yes) → セキュリティ対策
$pref['Met_Post'] = 1;
#■ プロキシサーバーのチェック(0=no 1=yes(no registration) 2=yes(no access))
#	0では何も制限しません。
#	1にするとプロキシサーバーを使った登録ができません。
#	2にするとプロキシさーばーを使ったアクセスができません。
$pref['proxy_check'] = 0;
#■ ユーザーデータのIP情報を表示する(0=no 1=yes(IP) 2=yes(HOST-IP))
#	0では何も表示されません。
#	1にするとユーザーデータにIPアドレスとホスト名が表示され、2ではIPアドレスのみが表示されます。
#	2ではデータの破損が発生しやすくなりますのでお勧めしません。
$pref['host_save'] = 1;
#■ 進行状況にホスト情報を表示する(0=no 1=yes(IP) 2=yes(HOST-IP))
#	0では何も表示されません。
#	2にすると新規登録時にIPアドレスとホスト名が表示され、1ではIPアドレスのみが表示されます。
$pref['host_view'] = 2;
#■ Sub-Server（未使用）
$pref['SubServer'] = 0;
$pref['SubS'] = 'SUB-SERVER-IP';#IP


#------- セキュリティ関連設定・選択機能 ---------

#■ 同一ホストからの登録禁止((0=no 1=yes)
#	この機能を使用すると、ケーブルテレビ等の環境からの登録が出来なくなる場合が
#	ありますので、設定の際はご注意下さい。
#$IP_deny = 0;
#■ ホスト名を使用(0=no 1=yes IPからホスト名の逆引きが出来ない場合、0にする)
#	0にするとIPアドレスでの表示になります。
#$IP_host = 0;
#■ 同一ホストからの登録を許可するホストorIPアドレス
#	省略するとアドレスやホスト名の範囲指定が出来ます。
#	例１) hogehoge.ne.jp とすると *.hogehoge.ne.jp のホストが許可されます。
#	例２) 192.168.0 とすると 192.168.0.* のアドレスが許可されます。
#	注意：この設定を空にすると全てのホスト（アドレス）が許可対象になります。
#$IP_ok = array('2Y','dummy');


#------- NPC設定 ---------

#■ NPC設置有り無し (0=no 1=yes)
#	設置する場合はbase.datのNPCデータが使用されます。
$pref['npc_mode'] = 1;
$pref['npc_num'] = 9;	# Number of NPC
$pref['npc_file'] = $pref['DAT_DIR'].'/base.dat'; #NPC DATA FILE
$pref['npc_run'] = 4;
$pref['BOSS'] = '班主任';$pref['KANN'] = '路人';$pref['ZAKO'] = '大叔';


#------- ICON設定 ---------

#■ アイコン画像のある「ディレクトリ」
#	→ URLなら http:// から記述する
#	→ 最後は / で閉じない
$pref['imgurl'] = 'img';
#■ マップの画像のある「ディレクトリ」
#	→ URLなら http:// から記述する
#	→ 最後は / で閉じない
$map = 'map';#	~~~~~~~~~~
#■ メーターの画像ファイル
$blue = $pref['imgurl'].'/blue.PNG';$gold = $pref['imgurl'].'/gold.PNG';$green = $pref['imgurl'].'/green.PNG';$pink = $pref['imgurl'].'/pink.PNG';$red = $pref['imgurl'].'/red.PNG';$yellow = $pref['imgurl'].'/yellow.PNG';
#■ アイコンを定義（上下は必ずペアで。男子アイコンを先、女子アイコンを後にして下さい）
$icon_file = array('question.gif');
#	男子生徒アイコン1（ファイル名）
#array_push ($icon_file,'i_akamatsu.jpg','i_keita.jpg','i_ooki.jpg','i_oda.jpg','i_kawada.jpg','i_kiriyama.jpg','i_kuninobu.jpg','i_kuramoto.jpg','i_kuronaga.jpg','i_sasagawa.jpg','i_sugimura.jpg','i_yutaka.jpg','i_takiguchi.jpg','i_duki.jpg','i_nanahara.jpg','i_niida.jpg','i_numai.jpg','i_hatagami.jpg','i_mimura.jpg','i_motobuchi.jpg','i_yamamoto.jpg');
#	男子生徒アイコン2（ファイル名）
array_push ($icon_file,'m_02.jpg','m_03.jpg','m_04.jpg','m_05.jpg','m_06.jpg','m_07.jpg','m_08.jpg','m_09.jpg','m_10.jpg','m_11.jpg','m_12.jpg','m_13.jpg','m_14.jpg','m_15.jpg','m_16.jpg','m_17.jpg','m_18.jpg','m_19.jpg','m_20.jpg','m_21.jpg');
#	女子生徒アイコン1（ファイル名）
#array_push ($icon_file,'i_inada.jpg','i_utsumi.jpg','i_eto.jpg','i_sakura.jpg','i_kanai.jpg','i_yukiko.jpg','i_yumiko.jpg','i_kotohiki.jpg','i_sakaki.jpg','i_hirono.jpg','i_souma.jpg','i_haruka.jpg','i_takako.jpg','i_tendo.jpg','i_noriko.jpg','i_yuka.jpg','i_noda.jpg','i_fujiyoshi.jpg','i_matsui.jpg','i_minami.jpg','i_yahagi.jpg');
#	女子生徒アイコン2（ファイル名）
array_push ($icon_file,'f_01.jpg','f_02.jpg','f_03.jpg','f_04.jpg','f_05.jpg','f_06.jpg','f_07.jpg','f_08.jpg','f_09.jpg','f_10.jpg','f_11.jpg','f_12.jpg','f_13.jpg','f_14.jpg','f_15.jpg','f_16.jpg','f_17.jpg','f_18.jpg','f_19.jpg','f_20.jpg','f_21.jpg');
#	NPC用アイコン（ファイル名）
array_push ($icon_file,'i_ziwei.jpg','i_jun.jpg','i_leimu.jpg','i_miaomiao.jpg','i_shiki.jpg','i_abu.jpg','i_llu.jpg','i_pxr.jpg','i_shouhan.jpg');
#	特殊 アイコン（ファイル名）
array_push ($icon_file,'s_negi.jpg','s_xp.jpg','s_makoto.jpg','s_item.jpg','s_lck.jpg','s_bing.jpg','s_yyloli.jpg','s_ria.jpg','s_sin.jpg','s_kunagisa.jpg');
#if($regist){#登録時？
	$icon_name = array('不明');
	#	男子生徒アイコン1（名前）
	#array_push ($icon_name,'1号 赤松義生','2号 飯島敬太','3号 大木立道','4号 織田敏憲','5号 川田章吾','6号 桐山和雄','7号 国信慶時','8号 倉元洋二','9号 黒長博','10号 笹川竜平','11号 杉村弘樹','12号 瀬戸豊','13号 滝口優一朗','14号 月岡彰','15号 七原秋也','16号 新井田和志','17号 沼井充','18号 旗上忠勝','19号 三村信史','20号 元渕恭一','21号 山本和彦');
	#	男子生徒アイコン2（名前）
	array_push ($icon_name,'2号 飯島敬太','3号 大木立道','4号 織田敏憲','5号 川田章吾','6号 桐山和雄','7号 国信慶時','8号 倉元洋二','9号 黒長博','10号 笹川竜平','11号 杉村弘樹','12号 瀬戸豊','13号 滝口優一朗','14号 月岡彰','15号 七原秋也','16号 新井田和志','17号 沼井充','18号 旗上忠勝','19号 三村信史','20号 元渕恭一','21号 山本和彦');
	$icon_check1 = count($icon_name);
	#	女子生徒アイコン1（名前）
	#array_push ($icon_name,'1号 稲田瑞穂','2号 内海幸枝','3号 江藤恵','4号 小川さくら','5号 金井泉','6号 北野雪子','7号 日下友美子','8号 琴弾加代子','9号 榊祐子','10号 清水比呂乃','11号 相馬光子','12号 谷沢はるか','13号 千草貴子','14号 天堂真弓','15号 中川典子','16号 中川有香','17号 野田聡美','18号 藤吉文世','19号 松井知里','20号 南佳織','21号 矢作好美');
	#	女子生徒アイコン2（名前）
	array_push ($icon_name,'1号 稲田瑞穂','2号 内海幸枝','3号 江藤恵','4号 小川さくら','5号 金井泉','6号 北野雪子','7号 日下友美子','8号 琴弾加代子','9号 榊祐子','10号 清水比呂乃','11号 相馬光子','12号 谷沢はるか','13号 千草貴子','14号 天堂真弓','15号 中川典子','16号 中川有香','17号 野田聡美','18号 藤吉文世','19号 松井知里','20号 南佳織','21号 矢作好美');
	$icon_check2 = count($icon_name);
	#	NPC用アイコン（名前）
	array_push ($icon_name,'情兽 滋味酱','渡良濑 准','博丽 灵梦','小神 喵喵','shiki 七夜','高和 阿部','蓝蓝路 蓝叔','麻将 小R','教练 受寒');
	$icon_check3 = count($icon_name);
	#	特殊 アイコン（名前、パスワード）
	array_push ($icon_name,'葱娘 专用','XP娘 专用','诚哥 专用','iTEM 专用','LCK 专用','小冰 专用','YY 专用','Riatre 专用','正弦 专用','Kunagisa 专用');
	#パスワード
	$s_icon_pass = array('negi','gchxp','makoto','item','lck1','bing','yyloli','riatre','sin2','kuna');
	$icon_check4 = count($icon_name);
	#'アナウンサー','北川安奈','安野良子','三村郁美','剣崎順矢','新谷和美','大貫慶子','三村叔父',
	#'i_ana.jpg','i_anna.jpg','i_anno.jpg','i_ikumi.jpg','i_junya.jpg','i_kazumi.jpg','i_keiko.jpg','i_uncle.jpg',
	#	アイコン定義（名前）【変更・削除しないでください】
#	$icon_name = array('不明',$m_icon_name,$f_icon_name,$n_icon_name,$s_icon_name);
#}

#	アイコン定義（ファイル名）【変更・削除しないでください】
#$icon_file = array('question.gif',$m_icon_file,$f_icon_file,$n_icon_file,$s_icon_file);
#------- 設定終了 ---------


#------- データ初期化 ---------
require $pref['LIB_DIR'].'/lib1.php';#LIBファイルロード

#下のは消さない事！
?>